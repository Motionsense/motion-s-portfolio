﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {
	public int id;
	public Vector3 position;
	public List<Node> neighbours = new List<Node>();
	public bool leftEdge = false;
	public bool rightEdge = false;
	public bool topEdge = false;
	public bool bottomEdge = false;
}

public class AIController : MonoBehaviour {

	RoadSpawner roadSpawner;
	public List<Node> nodes = new List<Node>();
	[SerializeField]
	AICar[] cars = new AICar[5];

	public void SetupController() {
		roadSpawner = GameObject.Find("Roads").GetComponent<RoadSpawner>();
		SetupNodes();
		SpawnCars();
	}

	void SetupNodes() {
		for (int x = 0; x < 10; ++x) {
			for (int y = 0; y < 10; ++y) {
				if (roadSpawner.isRoad[x, y]) {
					Node newNode = new Node();
					newNode.id = y + (x * 10);
					newNode.position = new Vector3(x * roadSpawner.tileSize, 0, y * roadSpawner.tileSize);
					if (x == 0) {
						newNode.leftEdge = true;
					}
					if (x == 9) {
						newNode.rightEdge = true;
					}
					if (y == 0) {
						newNode.bottomEdge = true;
					}
					if (y == 9) {
						newNode.topEdge = true;
					}
					nodes.Add(newNode);
				}
			}
		}

		foreach (Node thisNode in nodes) {
			foreach (Node neighbourNode in nodes) {
				if (thisNode == neighbourNode) {
					continue;
				}

				// Check below
				if (!thisNode.bottomEdge) {
					if (neighbourNode.id == thisNode.id - 1) {
						thisNode.neighbours.Add(neighbourNode);
					}
				}
				// Check Above
				if (!thisNode.topEdge) {
					if (neighbourNode.id == thisNode.id + 1) {
						thisNode.neighbours.Add(neighbourNode);
					}
				}
				// Check Left
				if (!thisNode.leftEdge) {
					if (neighbourNode.id == thisNode.id - 10) {
						thisNode.neighbours.Add(neighbourNode);
					}
				}
				// Check Right
				if (!thisNode.rightEdge) {
					if (neighbourNode.id == thisNode.id + 10) {
						thisNode.neighbours.Add(neighbourNode);
					}
				}
			}
		}
	}

	void SpawnCars() {
		foreach (Node node in nodes) {
			if (!node.leftEdge) {
				GameObject.Instantiate(cars[Random.Range(0, cars.Length)].gameObject, node.position, Quaternion.identity).GetComponent<AICar>().currentNode = node;
			}
		}
	}
}
