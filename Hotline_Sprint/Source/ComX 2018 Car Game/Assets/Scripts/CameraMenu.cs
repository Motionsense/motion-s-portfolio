﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMenu : MonoBehaviour {

    // <summary>
    //
    // <summary>
    public float cameraSpeed = 0.05f;
    // <summary>
    //
    // <summary>
    public Transform currentMount;
    // <summary>
    //
    // <summary>
    public Camera cam;
    // <summary>
    //
    // <summary>
    public int cameraInitialView = 60;

    // <summary>
    //
    // <summary>
    void LateUpdate () {
        transform.position = Vector3.Lerp(transform.position, currentMount.position, Time.deltaTime * cameraSpeed);

        Vector3 currentAngle = new Vector3(
            Mathf.LerpAngle(transform.rotation.eulerAngles.x, currentMount.transform.rotation.eulerAngles.x, Time.deltaTime * cameraSpeed),
            Mathf.LerpAngle(transform.rotation.eulerAngles.y, currentMount.transform.rotation.eulerAngles.y, Time.deltaTime * cameraSpeed),
            Mathf.LerpAngle(transform.rotation.eulerAngles.z, currentMount.transform.rotation.eulerAngles.z, Time.deltaTime * cameraSpeed));

        transform.eulerAngles = currentAngle;
	}

    // <summary>
    //
    // <summary>
    public void setMount(Transform newMount){
        currentMount = newMount;
    }
}
