﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

public class ResolutionScreen : MonoBehaviour {

    // <summary>
    //This is accesible in the Unity inspector,
    //but not in code outside this class.
    // <summary>
    [SerializeField]
    Dropdown resolutionDropdown;

    // <summary>
    //Array where the resolution 
    //vales are being stored
    // <summary>
    Resolution[] resolutionValues;

    // <summary>
    //Index values to be used and track 
    //when the user makes changes in options
    //and the changes are being saves
    // <summary>
    int activeScreenResIndex;

    // <summary>
    //Initializing the screen resolution values and 
    //clears any other values that been stored in the previous game run
    // <summary>
    void Start () {
        resolutionValues = Screen.resolutions.Distinct().ToArray();

        resolutionDropdown.ClearOptions();

        List<string> resValues = new List<string>();
        int testindex = 0;

        for (int i = 0; i < resolutionValues.Length; i++) {
            string resValue = resolutionValues[i].width + " x " + resolutionValues[i].height;
            resValues.Add(resValue);

            if (resolutionValues[i].width == Screen.currentResolution.width &&
                resolutionValues[i].height == Screen.currentResolution.height) {
                testindex = i;
            }
        }

        resolutionDropdown.AddOptions(resValues);
        resolutionDropdown.value = activeScreenResIndex;
        resolutionDropdown.RefreshShownValue();

        activeScreenResIndex = PlayerPrefs.GetInt("screen res index");
        bool isFullscreen = (PlayerPrefs.GetInt("fullscreen") == 1) ? true : false;
    }

    // <summary>
    //Function that retrives the resolution settings
    //and sets the values of the dropbox
    // <summary>
    public void SetResolution(int resolutionIndex) {

        Screen.SetResolution(resolutionValues[resolutionDropdown.value].width, 
            resolutionValues[resolutionDropdown.value].height, Screen.fullScreen);

        PlayerPrefs.SetInt("screen res index", activeScreenResIndex);
        PlayerPrefs.Save();
    }

    // <summary>
    //Function that is accessing the fullscreen setting
    //from Unity's setting
    // <summary>
    public void SetFullScreen(bool isFullscreen) {
        Screen.fullScreen = isFullscreen;

        PlayerPrefs.SetInt("fullscreen", ((isFullscreen) ? 1 : 0));
        PlayerPrefs.Save();
    }
}
