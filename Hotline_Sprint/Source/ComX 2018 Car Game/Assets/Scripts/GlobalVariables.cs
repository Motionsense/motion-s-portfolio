﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalVariables : MonoBehaviour {

	public int playersAmount = 0;
	public bool player1Controller = false;
	public bool player2Controller = false;
	public bool player3Controller = false;
	public bool player4Controller = false;
	public int playerRotation = 1; // Which player is the leader
	public Color[] colourArray = new Color[4];
	public int[] carSelected = new int[4];
	public string[] playerNames = new string[4];
	public int[] playerScores = new int[4];
	public int roundsPlayed = 0;
	public int roundsToPlay = 4;
	[SerializeField]
	Animator blackCover;

	void Start () {
		GameObject.DontDestroyOnLoad(gameObject);
		blackCover.Play("CoverFadeOut");
	}

	public void LoadScene(int sceneID) {
		blackCover.Play("CoverFadeIn");
		StartCoroutine(CoverFadeDelay(sceneID));
	}

	IEnumerator CoverFadeDelay(int sceneID) {
		yield return new WaitForSeconds(1);
		SceneManager.LoadScene(sceneID); // NEXT SCENE
	}

	private void OnLevelWasLoaded(int level) {
		if(level == 0 || level == 5) { // level 5: ComX test scene is a temporary work around to skip the main menu
			Destroy(gameObject); // Returning to menu, destroy this object
		}
		blackCover.Play("CoverFadeOut");
	}

}
