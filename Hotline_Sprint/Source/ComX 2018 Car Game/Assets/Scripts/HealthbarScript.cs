﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthbarScript : MonoBehaviour {

	RectTransform rt;
    Vector3 startPos;

	private void Start() {
		rt = gameObject.GetComponent<Image>().rectTransform;
        startPos = rt.localPosition;
	}

	public void UpdateHealthbar(float health) {
		Vector3 newPos = startPos;
        float inverseHealth = -health + 100;
        newPos.x = 960 + (8.1f * inverseHealth);
		rt.localPosition = newPos;
	}
}
