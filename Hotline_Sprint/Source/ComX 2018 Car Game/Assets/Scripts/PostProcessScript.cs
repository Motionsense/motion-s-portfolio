﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class PostProcessScript : MonoBehaviour {

	[SerializeField]
	PostProcessingProfile postProfile;
	bool editDoF = true;
	public Transform target;
    bool shortenDOF = false;
    const int startDOF = 100; // Depth of field during start up sequence
    const int playDOF = 35; // Depth of field during play

    void Start()
    {
        var dof = postProfile.depthOfField.settings;
        dof.focalLength = startDOF;
        postProfile.depthOfField.settings = dof;
    }

    void Update () {
		if (target == null) {
			target = gameObject.GetComponent<CameraFollow>().target;
			return;
		}
		if (editDoF) {
			var dof = postProfile.depthOfField.settings;
			dof.focusDistance = Vector3.Distance(transform.position, target.position) - 1;

            if (!shortenDOF)
            {
                if (gameObject.GetComponent<CameraFollow>().enabled == true)
                {
                    shortenDOF = true;
                }
            }
            else
            {
                if (dof.focalLength > playDOF + 1)
                {
                    dof.focalLength = Mathf.Lerp(dof.focalLength, playDOF, 0.05f);
                }
            }
			postProfile.depthOfField.settings = dof;
		}
	}
}
