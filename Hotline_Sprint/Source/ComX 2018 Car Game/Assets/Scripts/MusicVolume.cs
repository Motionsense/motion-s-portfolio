﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicVolume : MonoBehaviour {

    // <summary>
    //This are accesible in the Unity inspector,
    //but not in code outside this class.
    // <summary>
    [SerializeField]
    AudioClip[] MusicClips;
    [SerializeField]
    Slider sliderMusic;
    [SerializeField]
    Toggle toggleMusicOn;
    [SerializeField]
    AudioSource MusicSource;

    // <summary>
    //Assigns the AudioSource component to the AudioSource
    // in the inspector in Unity
    // <summary>
    void Start() {
        MusicSource = MusicSource.GetComponent<AudioSource>();
    }

    // <summary>
    //Based on the value of the slider the music volume will change
    //If the music is not played then starts playing a random song
    // <summary>
    void Update() {
        MusicSource.volume = sliderMusic.value;

        if (!MusicSource.isPlaying) {
            MusicSource.clip = playRandomMusic();
            MusicSource.Play();
        }
    }
    // <summary>
    // Picking randomly an index that reprents the songs' index
    // <summary>
    private AudioClip playRandomMusic() {
        return MusicClips[Random.Range(0, MusicClips.Length)];
    }
}

