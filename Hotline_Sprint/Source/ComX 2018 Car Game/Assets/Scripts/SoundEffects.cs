﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundEffects : MonoBehaviour {

    // <summary>
    //
    // <summary>
    [SerializeField]
    AudioClip helicopterSound;
    [SerializeField]
    Slider sliderSFX;
    [SerializeField]
    AudioSource SoundEffect;

    // <summary>
    //
    // <summary>
    void Start() {
        SoundEffect = SoundEffect.GetComponent<AudioSource>();
    }

    // <summary>
    //
    // <summary>
    void Update() {
        SoundEffect.volume = sliderSFX.value;

    }
}
