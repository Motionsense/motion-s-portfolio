﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class COMX_TEMP : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(Delay());
	}
	
	IEnumerator Delay() {
		yield return new WaitForSeconds(0.5f);
		SceneManager.LoadScene(1);
	}
}
