﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour {

	GlobalVariables globalVariables;
	[SerializeField]
	Text scoreText;

	void Start () {
		if (!GameObject.Find("GlobalVariables")) {
			SceneManager.LoadScene(1);
			return;
		}
		globalVariables = GameObject.Find("GlobalVariables").GetComponent<GlobalVariables>();
		globalVariables.roundsPlayed++;
		// SET TEXT
		if(globalVariables.roundsPlayed >= globalVariables.roundsToPlay) {
			SetScoreText("FINAL SCORES", 3, 5);
		} else {
			SetScoreText("", 2, 2);
		}
	}

	void SetScoreText(string title, float delay, int sceneID) {
		scoreText.text = title + "\n";
		switch (globalVariables.playersAmount) {
			case 2: {
					scoreText.text +=	globalVariables.playerNames[0] + " : " + globalVariables.playerScores[0] + "\n" +
										globalVariables.playerNames[1] + " : " + globalVariables.playerScores[1] + "\n" +
										"\n";
					break;
				}
			case 3: {
					scoreText.text +=	globalVariables.playerNames[0] + " : " + globalVariables.playerScores[0] + "\n" +
										globalVariables.playerNames[1] + " : " + globalVariables.playerScores[1] + "\n" +
										globalVariables.playerNames[2] + " : " + globalVariables.playerScores[2] + "\n";
					break;
				}
			case 4: {
					scoreText.text +=	globalVariables.playerNames[0] + " : " + globalVariables.playerScores[0] + "\n" +
										globalVariables.playerNames[1] + " : " + globalVariables.playerScores[1] + "\n" +
										globalVariables.playerNames[2] + " : " + globalVariables.playerScores[2] + "\n" +
										globalVariables.playerNames[3] + " : " + globalVariables.playerScores[3];
					break;
				}
			default: {
					Debug.LogWarning("NOT ENOUGH PLAYERS");
					SceneManager.LoadScene(0);
					break;
				}
		}
		StartCoroutine(ScoreDelay(delay, sceneID));
	}

	IEnumerator ScoreDelay(float delay, int sceneID) {
		yield return new WaitForSeconds(delay);
		globalVariables.LoadScene(sceneID);
	}

}
