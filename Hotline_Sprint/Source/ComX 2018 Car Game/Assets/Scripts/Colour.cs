﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Colour : MonoBehaviour
{
    // <summary>
    //
    // <summary>
    public Color ObjectColor;
    private Color currentColor;

    // <summary>
    //
    // <summary>
    private Material materialColored;

    // <summary>
    //
    // <summary>
    void Update() {
        if (ObjectColor != currentColor) {
            materialColored = new Material(Shader.Find("Diffuse"));
            materialColored.color = currentColor = ObjectColor;

            this.GetComponent<Renderer>().material = materialColored;
        }
    }
}