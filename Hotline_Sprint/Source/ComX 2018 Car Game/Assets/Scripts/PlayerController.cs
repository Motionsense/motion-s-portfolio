﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	GlobalVariables globalVariables;
	[SerializeField]
	PostProcessScript postProcessScript;
	Camera mainCamera;

	[SerializeField]
	Transform spotlight;
	[SerializeField]
	CameraFollow cameraFollow;
	[SerializeField]
	RoadSpawner roadSpawner;
	[SerializeField]
	GameObject[] criminalCars = new GameObject[4];
	[SerializeField]
	GameObject policeCar;
	WinCondition winCondition;

	int players = 0;
	public CarController player1;
	public CarController player2;
	public CarController player3;
	public CarController player4;

	public bool carCrashing = false;
	Vector3 crashCameraPosition;
	Transform crashingCar;
	float crashCameraLerpSpeed = 0.1f;
	public bool checkPolicePositions = false;
	Vector3 playerStartPosition = new Vector3(0, 1, -175); // Start position of lead car, other cars are offset from this
	const float leadCarBrightness = 0.2f;

	void Start () {
		if (GameObject.Find("GlobalVariables")) {
			globalVariables = GameObject.Find("GlobalVariables").GetComponent<GlobalVariables>();
			players = globalVariables.playersAmount;
		} else {
			SceneManager.LoadScene(1);
			return;
		}
		if (globalVariables.player1Controller) {
			player1 = GameObject.Instantiate(criminalCars[globalVariables.carSelected[globalVariables.playerRotation - 1]], playerStartPosition, Quaternion.identity).GetComponent<CarController>();
			player1.gameObject.GetComponent<LeadCarScript>().isLeader = true; // OPTIMISE
		}
		if (globalVariables.player2Controller) {
			Vector3 spawnPos = playerStartPosition;
			spawnPos.x -= 15;
			spawnPos.z -= 25;
			player2 = GameObject.Instantiate(policeCar, spawnPos, Quaternion.identity).GetComponent<CarController>();
		}
		if (globalVariables.player3Controller) {
			Vector3 spawnPos = playerStartPosition;
			spawnPos.z -= 28;
			player3 = GameObject.Instantiate(policeCar, spawnPos, Quaternion.identity).GetComponent<CarController>();
		}
		if (globalVariables.player4Controller) {
			Vector3 spawnPos = playerStartPosition;
			spawnPos.x += 15;
			spawnPos.z -= 25;
			player4 = GameObject.Instantiate(policeCar, spawnPos, Quaternion.identity).GetComponent<CarController>();
		}

		SetupControls();

		mainCamera = Camera.main;
		cameraFollow.target = player1.transform;
		winCondition = gameObject.GetComponent<WinCondition>();
		winCondition.player1 = player1.transform;
	}

	void SetupControls() {
		// Controller (Xbox One For Windows) - XBOX
		// Controller (XBOX 360 For Windows) - XBOX
		// Wireless Controller - PS4
		int nextPlayer = globalVariables.playerRotation;
		if (player1 != null) { // Set lead car to the selected controller under current rotation
			if (Input.GetJoystickNames()[nextPlayer - 1] == "Controller (XBOX 360 For Windows)" || Input.GetJoystickNames()[nextPlayer - 1] == "Controller (Xbox One For Windows)" || Input.GetJoystickNames()[nextPlayer - 1] == "Controller (XBOX One For Windows)") {
				player1.Triggers = "Triggers[" + nextPlayer.ToString() + "]";
				player1.LeftStickX = "LeftStickX[" + nextPlayer.ToString() + "]";
				player1.ButtonX = "ButtonX[" + nextPlayer.ToString() + "]";
				player1.ButtonB = "ButtonB[" + nextPlayer.ToString() + "]";
			} else if(Input.GetJoystickNames()[nextPlayer - 1] == "Wireless Controller") {
				player1.isPS4 = true;
				player1.Triggers = "null";
				player1.TriggerL2 = "PS4_L2[" + nextPlayer.ToString() + "]";
				player1.TriggerR2 = "PS4_R2[" + nextPlayer.ToString() + "]";
				player1.LeftStickX = "LeftStickX[" + nextPlayer.ToString() + "]";
				player1.ButtonX = "ButtonA[" + nextPlayer.ToString() + "]";
				player1.ButtonB = "ButtonX[" + nextPlayer.ToString() + "]";
			}
			Color carColour = globalVariables.colourArray[nextPlayer - 1];
			carColour = carColour * leadCarBrightness;
			player1.transform.Find("body").Find("model").GetComponent<MeshRenderer>().material.color = carColour;
            player1.transform.Find("PlayerText").GetComponent<TextMesh>().text = globalVariables.playerNames[nextPlayer - 1];
            player1.transform.Find("PlayerText").GetComponent<TextMesh>().color = globalVariables.colourArray[nextPlayer - 1];
            StartCoroutine(NameTagDelay(player1.transform.Find("PlayerText").gameObject));
        }
		if (player2 != null) { // Set police cars to controllers after
			nextPlayer++;
			if (nextPlayer > globalVariables.playersAmount) {
				nextPlayer = 1;
			}
			if (Input.GetJoystickNames()[nextPlayer - 1] == "Controller (XBOX 360 For Windows)" || Input.GetJoystickNames()[nextPlayer - 1] == "Controller (Xbox One For Windows)" || Input.GetJoystickNames()[nextPlayer - 1] == "Controller (XBOX One For Windows)") {
				player2.Triggers = "Triggers[" + nextPlayer.ToString() + "]";
				player2.LeftStickX = "LeftStickX[" + nextPlayer.ToString() + "]";
				player2.ButtonX = "ButtonX[" + nextPlayer.ToString() + "]";
				player2.ButtonB = "ButtonB[" + nextPlayer.ToString() + "]";
			} else if (Input.GetJoystickNames()[nextPlayer - 1] == "Wireless Controller") {
				player2.isPS4 = true;
				player2.Triggers = "null";
				player2.TriggerL2 = "PS4_L2[" + nextPlayer.ToString() + "]";
				player2.TriggerR2 = "PS4_R2[" + nextPlayer.ToString() + "]";
				player2.LeftStickX = "LeftStickX[" + nextPlayer.ToString() + "]";
				player2.ButtonX = "ButtonA[" + nextPlayer.ToString() + "]";
				player2.ButtonB = "ButtonX[" + nextPlayer.ToString() + "]";
			}
			player2.transform.Find("Light").GetComponent<Light>().color = globalVariables.colourArray[nextPlayer - 1];
            player2.transform.Find("PlayerText").GetComponent<TextMesh>().text = globalVariables.playerNames[nextPlayer - 1];
            player2.transform.Find("PlayerText").GetComponent<TextMesh>().color = globalVariables.colourArray[nextPlayer - 1];
            StartCoroutine(NameTagDelay(player2.transform.Find("PlayerText").gameObject));
        }
		if (player3 != null) {
			nextPlayer++;
			if (nextPlayer > globalVariables.playersAmount) {
				nextPlayer = 1;
			}
			if (Input.GetJoystickNames()[nextPlayer - 1] == "Controller (XBOX 360 For Windows)" || Input.GetJoystickNames()[nextPlayer - 1] == "Controller (Xbox One For Windows)" || Input.GetJoystickNames()[nextPlayer - 1] == "Controller (XBOX One For Windows)") {
				player3.Triggers = "Triggers[" + nextPlayer.ToString() + "]";
				player3.LeftStickX = "LeftStickX[" + nextPlayer.ToString() + "]";
				player3.ButtonX = "ButtonX[" + nextPlayer.ToString() + "]";
				player3.ButtonB = "ButtonB[" + nextPlayer.ToString() + "]";
			} else if (Input.GetJoystickNames()[nextPlayer - 1] == "Wireless Controller") {
				player3.isPS4 = true;
				player3.Triggers = "null";
				player3.TriggerL2 = "PS4_L2[" + nextPlayer.ToString() + "]";
				player3.TriggerR2 = "PS4_R2[" + nextPlayer.ToString() + "]";
				player3.LeftStickX = "LeftStickX[" + nextPlayer.ToString() + "]";
				player3.ButtonX = "ButtonA[" + nextPlayer.ToString() + "]";
				player3.ButtonB = "ButtonX[" + nextPlayer.ToString() + "]";
			}
			player3.transform.Find("Light").GetComponent<Light>().color = globalVariables.colourArray[nextPlayer - 1];
            player3.transform.Find("PlayerText").GetComponent<TextMesh>().text = globalVariables.playerNames[nextPlayer - 1];
            player3.transform.Find("PlayerText").GetComponent<TextMesh>().color = globalVariables.colourArray[nextPlayer - 1];
            StartCoroutine(NameTagDelay(player3.transform.Find("PlayerText").gameObject));
        }
		if (player4 != null) {
			nextPlayer++;
			if (nextPlayer > globalVariables.playersAmount) {
				nextPlayer = 1;
			}
			if (Input.GetJoystickNames()[nextPlayer - 1] == "Controller (XBOX 360 For Windows)" || Input.GetJoystickNames()[nextPlayer - 1] == "Controller (Xbox One For Windows)" || Input.GetJoystickNames()[nextPlayer - 1] == "Controller (XBOX One For Windows)") {
				player4.Triggers = "Triggers[" + nextPlayer.ToString() + "]";
				player4.LeftStickX = "LeftStickX[" + nextPlayer.ToString() + "]";
				player4.ButtonX = "ButtonX[" + nextPlayer.ToString() + "]";
				player4.ButtonB = "ButtonB[" + nextPlayer.ToString() + "]";
			} else if (Input.GetJoystickNames()[nextPlayer - 1] == "Wireless Controller") {
				player4.isPS4 = true;
				player4.Triggers = "null";
				player4.TriggerL2 = "PS4_L2[" + nextPlayer.ToString() + "]";
				player4.TriggerR2 = "PS4_R2[" + nextPlayer.ToString() + "]";
				player4.LeftStickX = "LeftStickX[" + nextPlayer.ToString() + "]";
				player4.ButtonX = "ButtonA[" + nextPlayer.ToString() + "]";
				player4.ButtonB = "ButtonX[" + nextPlayer.ToString() + "]";
			}
			player4.transform.Find("Light").GetComponent<Light>().color = globalVariables.colourArray[nextPlayer - 1];
            player4.transform.Find("PlayerText").GetComponent<TextMesh>().text = globalVariables.playerNames[nextPlayer - 1];
            player4.transform.Find("PlayerText").GetComponent<TextMesh>().color = globalVariables.colourArray[nextPlayer - 1];
            StartCoroutine(NameTagDelay(player4.transform.Find("PlayerText").gameObject));
        }

		globalVariables.playerRotation++;
		if(globalVariables.playerRotation > globalVariables.playersAmount) {
			globalVariables.playerRotation = 1;
		}
	}

    IEnumerator NameTagDelay(GameObject toDisable)
    {
        yield return new WaitForSeconds(6);
        toDisable.SetActive(false);
    }

	void Update() {
		if (checkPolicePositions) {
			if (player2 != null) {
				CheckPolicePositions(player2.transform);
			}
			if (player3 != null) {
				CheckPolicePositions(player3.transform);
			}
			if (player4 != null) {
				CheckPolicePositions(player4.transform);
			}
		}
		if (carCrashing) {
			CarCrash();
		}
		if (players > 1) {
			SpotlightControl();
		}
	}

	void SpotlightControl() {
		if (!carCrashing && player1 != null) {
			Vector3 newPos = player1.transform.position;
			newPos.y += cameraFollow.cameraHeight;
			newPos.z += cameraFollow.cameraDepth;
			spotlight.position = Vector3.Lerp(spotlight.position, newPos, 0.05f);
			Vector3 lookDirection = player1.transform.position - spotlight.position;
			spotlight.rotation = Quaternion.Lerp(spotlight.rotation, Quaternion.LookRotation(lookDirection), 0.04f);
		} else {
			if (crashingCar != null) {
				Vector3 lookDirection = crashingCar.position - spotlight.position;
				spotlight.rotation = Quaternion.Lerp(spotlight.rotation, Quaternion.LookRotation(lookDirection), crashCameraLerpSpeed);
			}
		}
	}

	void CheckPolicePositions(Transform policeCar) {
		Vector3 carScreenPosition = mainCamera.WorldToViewportPoint(policeCar.position);
		bool onScreen = carScreenPosition.z > 0 && carScreenPosition.x > 0 && carScreenPosition.x < 1 && carScreenPosition.y > 0 && carScreenPosition.y < 1;
		if (!onScreen) {
			DestroyCar(policeCar);
		}
	}

	public void DestroyCar(Transform car) {
		if(player1 == null) {
			return;
		}
        checkPolicePositions = false;
        carCrashing = true;
        cameraFollow.enabled = false;
		crashingCar = car;
		crashCameraPosition = car.position;
		if (car != player1.transform) {
				crashCameraPosition -= (player1.transform.position - car.position) / 2;
		} else {
			crashCameraPosition.x -= 4;
			crashCameraPosition.z -= 4;
		}
		crashCameraPosition.y += 5;
		newCameraPos = crashCameraPosition;
		Time.timeScale = 0.2f;
		Time.fixedDeltaTime = 0.004f;
		Vector3 explosionPosition = car.position;
		explosionPosition.x += Random.Range(-3f, 3f);
		explosionPosition.z += Random.Range(-3f, 3f);
		explosionPosition.y += Random.Range(-1f, 0f);
		car.GetComponent<Rigidbody>().AddExplosionForce(10, explosionPosition, 10, 10, ForceMode.VelocityChange);
		postProcessScript.target = car;
		StartCoroutine(CrashDelay(car));
	}

	Vector3 newCameraPos;
	void CarCrash() {
		Vector3 lookDirection = player1.transform.position - mainCamera.transform.position;
		if (crashingCar == player1.transform) {
			RaycastHit hit;
			if (Physics.Raycast(mainCamera.transform.position, lookDirection, out hit)) {
				if (hit.transform.root != player1.transform) { // If we cant see the player, move the camera
					newCameraPos = crashCameraPosition;
					newCameraPos.z += 8;
				}
			}
		}
		mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, newCameraPos, crashCameraLerpSpeed);
		mainCamera.transform.rotation = Quaternion.Lerp(mainCamera.transform.rotation, Quaternion.LookRotation(lookDirection), crashCameraLerpSpeed);
	}

	IEnumerator CrashDelay(Transform car) {
		yield return new WaitForSeconds(0.6f); // 3 Seconds
		Time.timeScale = 1;
		Time.fixedDeltaTime = 0.02f;
		if (player1 != null) {
			postProcessScript.target = player1.transform;
			if (car == player1.transform) {
				player1 = null;
				winCondition.PoliceWin();
			}
		}
		if (player2 != null) {
			if (car == player2.transform) {
				player2 = null;
				cameraFollow.enabled = true;
			}
		}
		if(player3 != null) {
			if (car == player3.transform) {
				player3 = null;
				cameraFollow.enabled = true;
			}
		}
		if(player4 != null) {
			if (car == player4.transform) {
				player4 = null;
				cameraFollow.enabled = true;
			}
		}
		carCrashing = false;

		yield return new WaitForSeconds(2);
        checkPolicePositions = true;
		if (car != null) {
			Destroy(car.gameObject);
		}
		players--;
		if(players <= 1) {
			if (player1 != null) {
				winCondition.CriminalWins();
			}
		}
	}

}
