﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationScript : MonoBehaviour {

    WinCondition winConditon;

    void Start()
    {
        winConditon = GameObject.Find("GameMechanics").GetComponent<WinCondition>();
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.root.GetComponent<LeadCarScript>())
        {
            if (collider.transform.root.GetComponent<LeadCarScript>().isLeader)
            {
                DestinationReached();
            }
        }
    }

    void DestinationReached()
    {
        winConditon.CriminalWins();
    }
}
