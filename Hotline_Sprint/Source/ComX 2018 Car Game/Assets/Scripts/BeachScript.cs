﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeachScript : MonoBehaviour {

	Mesh mesh;
	Vector3[] vertices;
	int[] triangles;
	Vector3[] normals;
	int triangleIndex = 0;
	float[] randoms;
	float[] randomTargets;

	[SerializeField]
	int size = 2;
	[SerializeField]
	float dunesHeight = 1f;

	void Start() {
		mesh = GetComponent<MeshFilter>().mesh;
		mesh.Clear();
		vertices = new Vector3[(size + 1) * (size + 1)];
		triangles = new int[(size * size) * 6];
		normals = new Vector3[(size + 1) * (size + 1)];

		for (int i = 0; i < normals.Length; ++i) {
			normals[i] = Vector3.up;
		}

		SetVertices();
		for (int y = 0; y < (size * (size + 1)); y += (size + 1)) {
			for (int x = 0; x < size; ++x) {
				CreateSquare(y + x);
			}
		}

		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.normals = normals;
		mesh.RecalculateBounds();

		randoms = new float[vertices.Length];
		randomTargets = new float[vertices.Length];
		for (int i = 0; i < randoms.Length; ++i) {
			randoms[i] = Random.Range(0, Mathf.PI * 2);
			randomTargets[i] = randoms[i];
		}
		gameObject.GetComponent<MeshCollider>().sharedMesh = mesh;
	}

	void SetVertices() {
		for (int y = 0; y < size + 1; ++y) {
			for (int x = 0; x < size + 1; ++x) {
                if (x == 0 || x == size + 1 || y == 0 || y == size + 1)
                {
                    vertices[x + (y * (size + 1))] = new Vector3(x, Random.Range(-2, -1), y);
                }
                else
                {
                    vertices[x + (y * (size + 1))] = new Vector3(x, Random.Range(0, dunesHeight), y);
                }
			}
		}
	}

	void CreateSquare(int i) {
		triangles[triangleIndex] = i;
		triangles[triangleIndex + 1] = i + (size + 1);
		triangles[triangleIndex + 2] = i + 1;

		triangles[triangleIndex + 3] = i + (size + 1) + 1;
		triangles[triangleIndex + 4] = i + 1;
		triangles[triangleIndex + 5] = i + (size + 1);
		triangleIndex += 6;
	}
}
