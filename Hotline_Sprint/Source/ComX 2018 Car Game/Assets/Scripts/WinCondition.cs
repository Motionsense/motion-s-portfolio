﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinCondition : MonoBehaviour {

	GlobalVariables globalVariables;
	Camera mainCamera;
	[SerializeField]
	Transform spotlight;
	[SerializeField]
	CameraFollow cameraFollow;
	public bool criminalWinning = false;
	public bool policeWinning = false;
	public Transform player1;
	[SerializeField]
	Image winPanel;
	[SerializeField]
	Text winText;
	const float panelFadeSpeed = 1f;
	RoadSpawner roadSpawner;
	int criminalIndex = 0;
	bool loadOnce = true;

	void Start() {
		mainCamera = Camera.main;
		roadSpawner = GameObject.Find("Roads").GetComponent<RoadSpawner>();
		if (GameObject.Find("GlobalVariables")) {
			globalVariables = GameObject.Find("GlobalVariables").GetComponent<GlobalVariables>();
			criminalIndex = globalVariables.playerRotation - 2;
			if(criminalIndex < 0) {
				criminalIndex = globalVariables.playersAmount - 1;
			}
		}
	}

	public void CriminalWins() {
		criminalWinning = true;
		cameraFollow.enabled = false;
		roadSpawner.enabled = false;
	}

	public void PoliceWin() {
		if (!criminalWinning) {
			policeWinning = true;
			roadSpawner.enabled = false;
		}
	}

	void Update() {
		if (criminalWinning) {
			CriminalWinCamera();
		}
		if (policeWinning) {
			winPanel.enabled = true;
			winText.enabled = true;
			winText.text = "Suspect Apprehended";
			Color newCol = winPanel.color;
			newCol.a += Time.deltaTime * panelFadeSpeed;
			winPanel.color = newCol;
			if(newCol.a >= 3 & loadOnce) {
				globalVariables.LoadScene(3);
				loadOnce = false;
			}
		}
	}

	float timer = 0;
	void CriminalWinCamera() {
		timer += Time.deltaTime;

		if (timer < 1 && player1 != null) { // Make the criminal stop and focus the camera on him for 1 second
			player1.GetComponent<CarController>().forceStop = true;
			Vector3 newPos = player1.position;
			newPos.z -= 5;
			newPos.y += 2;
			mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, newPos, 0.1f);
			Vector3 lookDirection = player1.position - mainCamera.transform.position;
			mainCamera.transform.rotation = Quaternion.Lerp(mainCamera.transform.rotation, Quaternion.LookRotation(lookDirection), 0.1f);
		} else { // Fade in the criminal wins panel after 1 second
			winPanel.enabled = true;
			winText.enabled = true;
			winText.text = "Arrest Evaded";
			Color newCol = winPanel.color;
			newCol.a += Time.deltaTime * panelFadeSpeed;
			winPanel.color = newCol;
			if (player1 != null) {
				Vector3 lookDirection = player1.position - mainCamera.transform.position;
				mainCamera.transform.rotation = Quaternion.Lerp(mainCamera.transform.rotation, Quaternion.LookRotation(lookDirection), 0.1f);
			}
		}
		if(timer > 3 && loadOnce) {
			globalVariables.playerScores[criminalIndex]++;
			globalVariables.LoadScene(3);
			loadOnce = false;
		}
	}

}
