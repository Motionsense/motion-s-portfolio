﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {

	PlayerController playerSetup;
	SceneStart sceneStart;
	WinCondition winCondtion;

	// INPUTS
	public string Triggers = "null";
	public string LeftStickX = "null";
	public string ButtonX = "null";
	public string TriggerL2 = "null";
	public string TriggerR2 = "null";
	public string ButtonB = "null";
	public bool isPS4 = false;

	// Speed Variables
	public float currentTorque = 0;
	public float maxTorque = 3000;
	public float maxSpeed = 120;
	const float breakPower = 1000;
	const float handbrakePower = 1200;
	float speedRatio = 0; // 1 = Not moving; 0 = Max speed;
	float wheelCircumference = 0;
	float currentSpeed = 0; // In units per hour
	float triggerInput = 0;

	// Wheel Colliders
	WheelFrictionCurve normalFriction;
	WheelFrictionCurve driftingFriction;
	[SerializeField]
	WheelCollider backLeftWC;
	[SerializeField]
	WheelCollider backRightWC;
	[SerializeField]
	WheelCollider frontLeftWC;
	[SerializeField]
	WheelCollider frontRightWC;
	[SerializeField]
	Transform[] wheelObjects = new Transform[4];
	[SerializeField]
	ParticleSystem[] smokeEmitters = new ParticleSystem[2];
	Color defaultSmokeColor;
	[SerializeField]
	GameObject sparkEmitter;
	[SerializeField]
	Light[] breakLights = new Light[2];

	// Steering Variables
	float steerInput = 0;
	float steeringModifier = 35; // How many degrees the wheels can turn
	public float currentSteerAngle = 0;
	float steeringRate = 0.1f;
    float driftFriction = 1f;
	Vector3 centerOfMass = new Vector3(0,-2, 0);

	// Boost
	float boostPercentage = 0;
	float boostRechargeRate = 100f;
	float boostStrength = 500;
	Rigidbody rb;
	float rechargeAreaSize = 150f; // Pixels
	GameObject cannister;

	[SerializeField]
	bool grounded = true;
	public bool forceStop = false;
	public bool destroyed = false;

	void Start() {
		playerSetup = GameObject.Find("GameMechanics").GetComponent<PlayerController>();
		sceneStart = GameObject.Find("GameMechanics").GetComponent<SceneStart>();
		winCondtion = GameObject.Find("GameMechanics").GetComponent<WinCondition>();
		normalFriction = backLeftWC.sidewaysFriction;
		driftingFriction = normalFriction;
		driftingFriction.stiffness = driftFriction;
		wheelCircumference = Mathf.PI * (backLeftWC.radius * 2);
		backLeftWC.ConfigureVehicleSubsteps(5f, 5, 5); // Get more precise results for reading speed / rpm
		gameObject.GetComponent<Rigidbody>().centerOfMass = centerOfMass;
		rb = gameObject.GetComponent<Rigidbody>();
		cannister = transform.Find("Cannister").gameObject;
		foreach (ParticleSystem emitter in smokeEmitters) {
			emitter.Stop();
		}
	}

	void Update() {
		// Get trigger input
		if (!isPS4) {
			triggerInput = Input.GetAxis(Triggers);
		} else {
			triggerInput = ((Input.GetAxis(TriggerR2) + 1) / 2) - ((Input.GetAxis(TriggerL2) + 1) / 2);
		}

		// Get speed
		currentSpeed = (wheelCircumference * backLeftWC.rpm * 60) / 1000;
		if (currentSpeed > 0) {
			speedRatio = currentSpeed / maxSpeed;
			speedRatio *= -1;
			speedRatio += 1;
		} else {
			speedRatio = 1;
		}
		if (triggerInput >= 0) { // Accelerating
			currentTorque = (triggerInput * maxTorque) * speedRatio;
			backLeftWC.brakeTorque = 0;
			backRightWC.brakeTorque = 0;
			frontLeftWC.brakeTorque = 0;
			frontRightWC.brakeTorque = 0;
			// Set speed
			backLeftWC.motorTorque = currentTorque;
			backRightWC.motorTorque = currentTorque;
			foreach (Light breakLight in breakLights) {
				breakLight.enabled = false;
			}
		} else { 
			// Braking
			if (speedRatio < 0.9f) {
				currentTorque = (triggerInput * -breakPower);
				backLeftWC.motorTorque = 0;
				backRightWC.motorTorque = 0;
				backLeftWC.brakeTorque = currentTorque;
				backRightWC.brakeTorque = currentTorque;
				frontLeftWC.brakeTorque = currentTorque;
				frontRightWC.brakeTorque = currentTorque;
				foreach (Light breakLight in breakLights) {
					breakLight.color = Color.red;
					breakLight.enabled = true;
				}
			} else { // Reverse
				currentTorque = triggerInput * (maxTorque / 4);
				backLeftWC.brakeTorque = 0;
				backRightWC.brakeTorque = 0;
				frontLeftWC.brakeTorque = 0;
				frontRightWC.brakeTorque = 0;
				// Set speed
				backLeftWC.motorTorque = currentTorque;
				backRightWC.motorTorque = currentTorque;
				foreach (Light breakLight in breakLights) {
					breakLight.color = Color.white;
					breakLight.enabled = true;
				}
			}
		}
		// Get turning
		steerInput = Input.GetAxis(LeftStickX) * steeringModifier;
		currentSteerAngle = Mathf.Lerp(currentSteerAngle, steerInput, steeringRate);
		// Set turning
		frontLeftWC.steerAngle = currentSteerAngle;
		frontRightWC.steerAngle = currentSteerAngle;

		// Drift and boost
		if (!sceneStart.starting && grounded) {
			// Drift
			if (Input.GetButtonDown(ButtonX)) {
				StartTireSmoke();
				backLeftWC.sidewaysFriction = driftingFriction;
				backRightWC.sidewaysFriction = driftingFriction;
				backLeftWC.brakeTorque = handbrakePower;
				backRightWC.brakeTorque = handbrakePower;
			}
			if (Input.GetButtonUp(ButtonX)) {
				EndTireSmoke();
				backLeftWC.sidewaysFriction = normalFriction;
				backRightWC.sidewaysFriction = normalFriction;
			}
			if (Input.GetButton(ButtonX)) {
				foreach (ParticleSystem emitter in smokeEmitters) {
					ParticleSystem.MainModule settings = emitter.main;
					Color newColor = emitter.main.startColor.color;
					newColor.a = (currentSpeed / maxSpeed) / 10;
					settings.startColor = new ParticleSystem.MinMaxGradient(newColor);
				}
			}
			// Boost
			Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position);
			if (screenPos.x < rechargeAreaSize || screenPos.x > Screen.width - rechargeAreaSize || screenPos.y < rechargeAreaSize || screenPos.y > Screen.height - rechargeAreaSize) {
				if (boostPercentage < 100) {
					boostPercentage += boostRechargeRate * Time.deltaTime;
				} else {
					cannister.SetActive(true);
				}
			}
			if (boostPercentage >= 100 && Input.GetButtonDown(ButtonB)) {
				rb.AddForce(transform.forward * boostStrength, ForceMode.Acceleration);
				cannister.SetActive(false);
				boostPercentage = 0;
			}
		} else {
			EndTireSmoke();
			backLeftWC.sidewaysFriction = normalFriction;
			backRightWC.sidewaysFriction = normalFriction;
		}

		if (forceStop) {
			backLeftWC.brakeTorque = handbrakePower * 2;
			backRightWC.brakeTorque = handbrakePower * 2;
			frontLeftWC.brakeTorque = handbrakePower * 2;
			frontRightWC.brakeTorque = handbrakePower * 2;
			frontLeftWC.steerAngle = 0;
			frontRightWC.steerAngle = 0;
		}

		if (sceneStart.starting) { // Set cars to accelerate when game starts
			backLeftWC.motorTorque = maxTorque * speedRatio;
			backRightWC.motorTorque = maxTorque * speedRatio;
			backLeftWC.brakeTorque = 0;
			backRightWC.brakeTorque = 0;
			frontLeftWC.brakeTorque = 0;
			frontRightWC.brakeTorque = 0;
			backLeftWC.sidewaysFriction = normalFriction;
			backRightWC.sidewaysFriction = normalFriction;
			frontLeftWC.steerAngle = 0;
			frontRightWC.steerAngle = 0;
		}
		if (!winCondtion.policeWinning && !winCondtion.criminalWinning && !destroyed) {
			CheckOnTrack();
		} else {
			grounded = false;
		}
	}

	void StartTireSmoke() {
		foreach (ParticleSystem emitter in smokeEmitters) {
			emitter.GetComponent<ParticleSystem>().Play();
		}
	}

	void EndTireSmoke() {
		foreach (ParticleSystem emitter in smokeEmitters) {
			emitter.GetComponent<ParticleSystem>().Stop();
		}
	}

	float dist = 2.5f;
	float widthOffset = 0.5f;
	float depthOffset = 1.5f;
	void CheckOnTrack() {
		RaycastHit[] hits = new RaycastHit[4];
		Vector3 frontLeft = transform.position + new Vector3(-widthOffset, 0, depthOffset);
		Vector3 frontRight = transform.position + new Vector3(widthOffset, 0, depthOffset);
		Vector3 backLeft = transform.position + new Vector3(-widthOffset, 0, -depthOffset);
		Vector3 backRight = transform.position + new Vector3(widthOffset, 0, -depthOffset);
		if (Physics.Raycast(frontLeft, Vector3.down, out hits[0]) && Physics.Raycast(frontRight, Vector3.down, out hits[1]) && Physics.Raycast(backLeft, Vector3.down, out hits[2]) && Physics.Raycast(backRight, Vector3.down, out hits[3])) {
			if(hits[0].distance < dist && hits[1].distance < dist && hits[2].distance < dist && hits[3].distance < dist) {
				grounded = true;
			} else {
				grounded = false;
			}
		} else {
			grounded = false;
			playerSetup.DestroyCar(transform);
			destroyed = true;
		}
	}

	float wheelSpinModifier = 0.2f;
	void FixedUpdate() {
		foreach(Transform wheel in wheelObjects) {
			wheel.Rotate(new Vector3(currentSpeed * wheelSpinModifier, 0, 0));
		}
	}

	private void OnCollisionEnter(Collision collision) {
		if(collision.transform.root.tag == "Collision") {
			foreach (ContactPoint contact in collision.contacts) {
				StartCoroutine(DeleteSparks(GameObject.Instantiate(sparkEmitter, contact.point, Quaternion.identity)));
			}
		}
	}

	IEnumerator DeleteSparks(GameObject sparks) {
		yield return new WaitForSeconds(0.2f);
		Destroy(sparks);
	}
}
