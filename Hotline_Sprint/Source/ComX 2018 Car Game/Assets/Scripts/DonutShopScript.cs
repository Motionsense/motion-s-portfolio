﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonutShopScript : MonoBehaviour {

    [SerializeField]
    Rigidbody donutObject;
    [SerializeField]
    Rigidbody platformObject;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Collision")
        {
            donutObject.constraints = RigidbodyConstraints.None;
            StartCoroutine(Delay());
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1);
        platformObject.constraints = RigidbodyConstraints.FreezeAll;
    }
}
