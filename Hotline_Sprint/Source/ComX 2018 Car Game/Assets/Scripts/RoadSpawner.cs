﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSpawner : MonoBehaviour {

	const int gridWidth = 10;
	const int gridHeight = 10;
	public int tileSize = 50;
	GameObject[,] roadTiles = new GameObject[gridWidth, gridHeight];
	public bool[,] isRoad = new bool[gridWidth, gridHeight];
	Vector2 destination = new Vector2(gridWidth - 1, gridHeight - 1);
	const int pathComplexity = 3;

	[SerializeField]
	GameObject floorObject;
	[SerializeField]
	GameObject endRoad;
	[SerializeField]
	GameObject straightRoad;
	[SerializeField]
	GameObject cornerRoad;
	[SerializeField]
	GameObject junctionRoad;
	[SerializeField]
	GameObject plusRoad;
    [SerializeField]
    GameObject destinationObject;
	[SerializeField]
	GameObject[] buildingObjects;
    [SerializeField]
    GameObject motelObject;
    [SerializeField]
    GameObject stationObject;

	void Start() {
		for (int x = 0; x < gridWidth; ++x) {
			for (int y = 0; y < gridHeight; ++y) {
				roadTiles[x, y] = FloorTile(x, y);
			}
		}
		GeneratePath(Vector2.zero, destination, pathComplexity);
		GeneratePath(new Vector2(0, gridHeight - 1), new Vector2(gridWidth - 1, 0), pathComplexity);
		GeneratePath(new Vector2(0, Mathf.Floor(gridHeight - 1)), new Vector2(gridWidth - 1, Mathf.Floor(gridHeight - 1)), pathComplexity);
		CreateTiles();
        GameObject.Instantiate(destinationObject, new Vector3((gridWidth - 1) * tileSize, 0, (gridHeight - 1) * tileSize), Quaternion.identity, transform);
		GameObject.Find("GameMechanics").GetComponent<AIController>().SetupController();
		roadTiles[gridWidth - 1, gridHeight - 1].SetActive(false);
	}

	GameObject FloorTile(int xPos, int yPos) {
		Vector3 tilePos = new Vector3(xPos * floorObject.transform.localScale.x * 10, 0, yPos * floorObject.transform.localScale.z * 10);
		GameObject newTile = GameObject.Instantiate(floorObject, tilePos, Quaternion.identity, transform);
		return newTile;
	}

	void GeneratePath(Vector2 start, Vector2 target, int nodes) {
		nodes++;
		int nodesLeft = nodes;
		Vector2 previousNode = start;
		while(nodesLeft > 0) {
			Vector2 node;
			if(nodesLeft == 1){
				node = target;
			} else {
				node = new Vector2(Random.Range(0, gridWidth - 1), Random.Range(0, gridHeight - 1));
			}
			DrawPath(previousNode, node);
			previousNode = node;
			nodesLeft--;
		}
	}

	void DrawPath(Vector2 start, Vector2 target) {
		if (start.y > target.y) {
			while (start.y != target.y) {
				isRoad[(int)start.x, (int)start.y] = true;
				start.y--;
			}
		} else {
			while (start.y != target.y) {
				isRoad[(int)start.x, (int)start.y] = true;
				start.y++;
			}
		}
		if (start.x > target.x) {
			while (start.x != target.x - 1) {
				isRoad[(int)start.x, (int)start.y] = true;
				start.x--;
			}
		} else {
			while (start.x != target.x + 1) {
				isRoad[(int)start.x, (int)start.y] = true;
				start.x++;
			}
		}
	}

	void CreateTiles() {
		// Loop through every tile
		for (int x = 0; x < gridWidth; ++x) {
			for (int y = 0; y < gridHeight; ++y) {

				// Check roads around given tile
				bool roadUp = false;
				bool roadDown = false;
				bool roadLeft = false;
				bool roadRight = false;
				if (x == 0 && y == 0) {
					roadDown = true;
				}
				if (x > 0) { // Check Left
					if (isRoad[x - 1, y]) {
						roadLeft = true;
					}
				}
				if (x < gridWidth - 1) { // Check Right
					if (isRoad[x + 1, y]) {
						roadRight = true;
					}
				}
				if (y > 0) { // Check Down
					if (isRoad[x, y - 1]) {
						roadDown = true;
					}
				}
				if (y < gridHeight - 1) { // Check Up
					if (isRoad[x, y + 1]) {
						roadUp = true;
					}
				}

				// Create a road prefab for the tile
				if (isRoad[x, y]) {
					if (roadLeft) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(endRoad, tilePos, Quaternion.Euler(0, 90, 0), transform);
					}
					if (roadRight) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(endRoad, tilePos, Quaternion.Euler(0, 270, 0), transform);
					}
					if (roadUp) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(endRoad, tilePos, Quaternion.Euler(0, 180, 0), transform);
					}
					if (roadDown) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(endRoad, tilePos, Quaternion.Euler(0, 0, 0), transform);
					}
					if (roadUp && roadDown) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x,y] = GameObject.Instantiate(straightRoad, tilePos, Quaternion.identity, transform);
					}
					if (roadLeft && roadRight) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(straightRoad, tilePos, Quaternion.Euler(0, 90, 0), transform);
					}
					if (roadLeft && roadUp) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(cornerRoad, tilePos, Quaternion.Euler(0, 180, 0), transform);
					}
					if (roadRight && roadUp) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(cornerRoad, tilePos, Quaternion.Euler(0, 270, 0), transform);
					}
					if (roadDown && roadLeft) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(cornerRoad, tilePos, Quaternion.Euler(0, 90, 0), transform);
					}
					if (roadDown && roadRight) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(cornerRoad, tilePos, Quaternion.Euler(0, 0, 0), transform);
					}
					if (roadUp && roadDown && roadLeft) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(junctionRoad, tilePos, Quaternion.Euler(0, 90, 0), transform);
					}
					if (roadUp && roadDown && roadRight) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(junctionRoad, tilePos, Quaternion.Euler(0, 270, 0), transform);
					}
					if (roadLeft && roadRight && roadUp) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(junctionRoad, tilePos, Quaternion.Euler(0, 180, 0), transform);
					}
					if (roadLeft && roadRight && roadDown) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(junctionRoad, tilePos, Quaternion.Euler(0, 0, 0), transform);
					}
					if (roadLeft && roadRight && roadDown && roadUp) {
						Destroy(roadTiles[x, y]);
						Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
						roadTiles[x, y] = GameObject.Instantiate(plusRoad, tilePos, Quaternion.identity, transform);
					}
				}

                // Create buildings for the non-road tile
                if (!isRoad[x, y])
                {
                    // If no roads are connect to this tile, spawn nothing
                    if(!roadUp && !roadDown && !roadLeft && !roadRight)
                    {
                        continue;
                    }
                    // Chance to spawn large structures
                    int randomLargeStructure = Random.Range(0, 20);
                    if (randomLargeStructure == 0)
                    {
                        GameObject empty = new GameObject();
                        empty.transform.parent = roadTiles[x, y].transform;
                        Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
                        int randomRot = Random.Range(0, 4);
                        switch (randomRot) {
                            case 0:
                                {
                                    GameObject.Instantiate(motelObject, tilePos, Quaternion.Euler(0, 0, 0), empty.transform);
                                    break;
                                }
                            case 1:
                                {
                                    GameObject.Instantiate(motelObject, tilePos, Quaternion.Euler(0, 90, 0), empty.transform);
                                    break;
                                }
                            case 2:
                                {
                                    GameObject.Instantiate(motelObject, tilePos, Quaternion.Euler(0, 180, 0), empty.transform);
                                    break;
                                }
                            case 3:
                                {
                                    GameObject.Instantiate(motelObject, tilePos, Quaternion.Euler(0, 270, 0), empty.transform);
                                    break;
                                }
                        }
                    }
                    else if(randomLargeStructure == 1)
                    {
                        GameObject empty = new GameObject();
                        empty.transform.parent = roadTiles[x, y].transform;
                        Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
                        int randomRot = Random.Range(0, 4);
                        switch (randomRot)
                        {
                            case 0:
                                {
                                    GameObject.Instantiate(stationObject, tilePos, Quaternion.Euler(0, 0, 0), empty.transform);
                                    break;
                                }
                            case 1:
                                {
                                    GameObject.Instantiate(stationObject, tilePos, Quaternion.Euler(0, 90, 0), empty.transform);
                                    break;
                                }
                            case 2:
                                {
                                    GameObject.Instantiate(stationObject, tilePos, Quaternion.Euler(0, 180, 0), empty.transform);
                                    break;
                                }
                            case 3:
                                {
                                    GameObject.Instantiate(stationObject, tilePos, Quaternion.Euler(0, 270, 0), empty.transform);
                                    break;
                                }
                        }
                    }
                    else
                    {
                        bool bottomLeftAssigned = false;
                        bool bottomRightAssigned = false;
                        bool topLeftAssigned = false;
                        bool topRightAssigned = false;

                        if (roadUp)
                        {
                            Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
                            tilePos.x -= 20;
                            tilePos.z += 20;
                            GameObject empty = new GameObject();
                            empty.transform.parent = roadTiles[x, y].transform;
                            if (!topLeftAssigned)
                            {
                                GameObject.Instantiate(buildingObjects[0], tilePos, Quaternion.Euler(0, 270, 0), empty.transform);
                                topLeftAssigned = true;
                            }
                            tilePos.x += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 270, 0), empty.transform);
                            tilePos.x += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 270, 0), empty.transform);
                            tilePos.x += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 270, 0), empty.transform);
                            if (!topRightAssigned)
                            {
                                tilePos.x += 10;
                                GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 270, 0), empty.transform);
                                topRightAssigned = true;
                            }
                        }
                        if (roadDown)
                        {
                            Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
                            tilePos.x -= 20;
                            tilePos.z -= 20;
                            GameObject empty = new GameObject();
                            empty.transform.parent = roadTiles[x, y].transform;
                            if (!bottomLeftAssigned)
                            {
                                GameObject.Instantiate(buildingObjects[0], tilePos, Quaternion.Euler(0, 90, 0), empty.transform);
                                bottomLeftAssigned = true;
                            }
                            tilePos.x += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 90, 0), empty.transform);
                            tilePos.x += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 90, 0), empty.transform);
                            tilePos.x += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 90, 0), empty.transform);
                            if (!bottomRightAssigned)
                            {
                                tilePos.x += 10;
                                GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 90, 0), empty.transform);
                                bottomRightAssigned = true;
                            }
                        }
                        if (roadLeft)
                        {
                            Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
                            tilePos.x -= 20;
                            tilePos.z -= 20;
                            GameObject empty = new GameObject();
                            empty.transform.parent = roadTiles[x, y].transform;
                            if (!bottomLeftAssigned)
                            {
                                GameObject.Instantiate(buildingObjects[0], tilePos, Quaternion.Euler(0, 180, 0), empty.transform);
                                bottomLeftAssigned = true;
                            }
                            tilePos.z += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 180, 0), empty.transform);
                            tilePos.z += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 180, 0), empty.transform);
                            tilePos.z += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 180, 0), empty.transform);
                            if (!topLeftAssigned)
                            {
                                tilePos.z += 10;
                                GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 180, 0), empty.transform);
                                topLeftAssigned = true;
                            }
                        }
                        if (roadRight)
                        {
                            Vector3 tilePos = new Vector3(x * floorObject.transform.localScale.x * 10, 0, y * floorObject.transform.localScale.z * 10);
                            tilePos.x += 20;
                            tilePos.z -= 20;
                            GameObject empty = new GameObject();
                            empty.transform.parent = roadTiles[x, y].transform;
                            if (!bottomRightAssigned)
                            {
                                GameObject.Instantiate(buildingObjects[0], tilePos, Quaternion.Euler(0, 0, 0), empty.transform);
                                bottomRightAssigned = true;
                            }
                            tilePos.z += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 0, 0), empty.transform);
                            tilePos.z += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 0, 0), empty.transform);
                            tilePos.z += 10;
                            GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 0, 0), empty.transform);
                            if (!topRightAssigned)
                            {
                                tilePos.z += 10;
                                GameObject.Instantiate(buildingObjects[Random.Range(0, buildingObjects.Length)], tilePos, Quaternion.Euler(0, 0, 0), empty.transform);
                                topRightAssigned = true;
                            }
                        }
                    }
                }
			}
		}
	}

}
