﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AICar : MonoBehaviour {

	public Node currentNode;
	Node previousNode;
	Rigidbody rb;
	float maxSpeed = 12;
	float accelleration = 50;
	Transform child;

	void Start() {
		rb = gameObject.GetComponent<Rigidbody>();
		previousNode = currentNode;
		child = transform.GetChild(0);
	}

	void FixedUpdate () {
		Vector3 targetDirection = (transform.position - currentNode.position).normalized;
		if (targetDirection != Vector3.zero) {
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(targetDirection, Vector3.up), 0.05f);
		}
		if(rb.velocity.magnitude < maxSpeed) {
			rb.AddForce(-transform.forward * accelleration, ForceMode.Acceleration);
		}
		if (Vector3.Distance(transform.position, currentNode.position) < 10) {
			Node nextNode = currentNode.neighbours[Random.Range(0, currentNode.neighbours.Count)];
			if (currentNode.neighbours.Count > 1) {
				while(nextNode == previousNode) {
					nextNode = currentNode.neighbours[Random.Range(0, currentNode.neighbours.Count)];
				}
			}
			previousNode = currentNode;
			currentNode = nextNode;
			maxSpeed = 12;
		}

		RaycastHit hit;
		Vector3 rayPos = child.position;
		rayPos.y = 1;
		if(Physics.Raycast(rayPos, -transform.forward, out hit, 10)) {
			if (hit.distance > 5) {
				maxSpeed = 2;
				accelleration = 50;
			} else {
				maxSpeed = 5;
				accelleration = -30;
			}
		} else {
			maxSpeed = 12;
			accelleration = 50;
		}
	}
}
