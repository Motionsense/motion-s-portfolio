﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

enum PlayerState { NotJoined, Car, Colour, Name, Ready };
class Player {
	public PlayerState state = PlayerState.NotJoined;
	public Color color = Color.red;
	public GameObject model;
	public int selectedCar = 0;
	public float inputTimer = 0;
	public int[] nameLetters = new int[3];
	public int selectedLetter = 0;
	public string name = "AAA";
}

public class ControllerSetup : MonoBehaviour {

	[SerializeField]
	GlobalVariables globalVariables;
	const float inputDelay = 0.2f;
	char[] letters = new char[] { 'A', 'B', 'C' , 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
	int playersReady = 0;

	Player player1 = new Player();
	[SerializeField]
	Text player1Text;
	[SerializeField]
	Text player1Name;
	[SerializeField]
	Text player1Selector;
	Vector3 player1Pos = new Vector3(29, -1, 28);

	Player player2 = new Player();
	[SerializeField]
	Text player2Text;
	[SerializeField]
	Text player2Name;
	[SerializeField]
	Text player2Selector;
	Vector3 player2Pos = new Vector3(34.75f, -1, 28);

	Player player3 = new Player();
	[SerializeField]
	Text player3Text;
	[SerializeField]
	Text player3Name;
	[SerializeField]
	Text player3Selector;
	Vector3 player3Pos = new Vector3(40.75f, -1, 28);

	Player player4 = new Player();
	[SerializeField]
	Text player4Text;
	[SerializeField]
	Text player4Name;
	[SerializeField]
	Text player4Selector;
	Vector3 player4Pos = new Vector3(46.5f, -1, 28);

	[SerializeField]
	GameObject[] playerStartButtons = new GameObject[4];
	[SerializeField]
	GameObject[] carPrefabs = new GameObject[4];

	void Start() {
		if(Camera.main.aspect == 16f / 9f) {
			Camera.main.GetComponent<Animator>().Play("MenuCamera16x9");
		}
		else {
			Camera.main.GetComponent<Animator>().Play("MenuCamera4x3");
		}
	}

	void Update () {
		Player1Inputs();
		Player2Inputs();
		Player3Inputs();
		Player4Inputs();
	}

	void Player1Inputs() {
		if (Input.GetJoystickNames().Length >= 1) {
			if (player1.state == PlayerState.NotJoined) {
				playerStartButtons[0].SetActive(true);
			} else {
				playerStartButtons[0].SetActive(false);
			}
			if (player1.inputTimer < inputDelay) {
				player1.inputTimer += Time.deltaTime;
				return;
			}
			// XBOX INPUTS
			if (Input.GetJoystickNames()[0] == "Controller (Xbox One For Windows)" || Input.GetJoystickNames()[0] == "Controller (XBOX One For Windows)" || Input.GetJoystickNames()[0] == "Controller (XBOX 360 For Windows)") {

				switch (player1.state) {
					case PlayerState.NotJoined: {
							if (Input.GetButton("ButtonA[1]")) { // Player 1 joining, initiailise variables
								globalVariables.playersAmount++;
								player1.color = Color.red;
								player1.model = GameObject.Instantiate(carPrefabs[player1.selectedCar], player1Pos, Quaternion.Euler(0, 90, 0));
								player1.model.transform.Find("model").GetComponent<Renderer>().material.color = player1.color;
								player1Text.text = "Select Car";
								player1.state = PlayerState.Car;
								player1.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Car: {
							if (Input.GetAxis("DpadX[1]") > 0) { // Next car
								Destroy(player1.model);
								player1.selectedCar++;
								if (player1.selectedCar >= carPrefabs.Length) {
									player1.selectedCar = 0;
								}
								player1.model = GameObject.Instantiate(carPrefabs[player1.selectedCar], player1Pos, Quaternion.Euler(0, 90, 0));
								player1.model.transform.Find("model").GetComponent<Renderer>().material.color = player1.color;
								player1.inputTimer = 0;
							}
							if (Input.GetAxis("DpadX[1]") < 0) { // Previous Car
								Destroy(player1.model);
								player1.selectedCar--;
								if (player1.selectedCar < 0) {
									player1.selectedCar = carPrefabs.Length - 1;
								}
								player1.model = GameObject.Instantiate(carPrefabs[player1.selectedCar], player1Pos, Quaternion.Euler(0, 90, 0));
								player1.model.transform.Find("model").GetComponent<Renderer>().material.color = player1.color;
								player1.inputTimer = 0;
							}
							if (Input.GetButton("ButtonA[1]")) { // Car selected, move to colour
								player1Text.text = "Select Colour";
								player1.state = PlayerState.Colour;
								player1.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonB[1]")) { // Player 1 leaving, destroy variables
								globalVariables.playersAmount--;
								Destroy(player1.model);
								player1Text.text = "";
								player1.state = PlayerState.NotJoined;
								player1.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Colour: {
							if (Input.GetAxis("DpadX[1]") > 0) { // Next colour
								player1.color = CycleColourUp(player1.color);
								player1.model.transform.Find("model").GetComponent<Renderer>().material.color = player1.color;
							}
							if (Input.GetAxis("DpadX[1]") < 0) { // Previous Colour
								player1.color = CycleColourDown(player1.color);
								player1.model.transform.Find("model").GetComponent<Renderer>().material.color = player1.color;
							}
							if (Input.GetButton("ButtonA[1]")) { // Colour selected, move to Name
								player1Text.text = "Choose Name";
								player1Selector.text = "^";
								player1Name.text = player1.name;
								player1.state = PlayerState.Name;
								player1.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonB[1]")) { // Colour Canceled, return to car
								player1Text.text = "Select Car";
								player1.state = PlayerState.Car;
								player1.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Name: {
							if (Input.GetAxis("DpadX[1]") > 0) { // Next letter
								player1.selectedLetter++;
								if (player1.selectedLetter >= player1.nameLetters.Length) {
									player1.selectedLetter = 0;
								}
								if (player1.selectedLetter == 0) {
									player1Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player1.selectedLetter == 1) {
									player1Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player1.selectedLetter == 2) {
									player1Selector.alignment = TextAnchor.MiddleRight;
								}
								player1.inputTimer = 0;
							}
							if (Input.GetAxis("DpadX[1]") < 0) { // Previous letter
								player1.selectedLetter--;
								if (player1.selectedLetter < 0) {
									player1.selectedLetter = player1.nameLetters.Length - 1;
								}
								if (player1.selectedLetter == 0) {
									player1Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player1.selectedLetter == 1) {
									player1Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player1.selectedLetter == 2) {
									player1Selector.alignment = TextAnchor.MiddleRight;
								}
								player1.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[1]") > 0) { // Letter up
								player1.nameLetters[player1.selectedLetter]++;
								if (player1.nameLetters[player1.selectedLetter] >= letters.Length) {
									player1.nameLetters[player1.selectedLetter] = 0;
								}
								player1.name = "" + letters[player1.nameLetters[0]] + letters[player1.nameLetters[1]] + letters[player1.nameLetters[2]];
								player1Name.text = player1.name;
								player1.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[1]") < 0) { // Letter down
								player1.nameLetters[player1.selectedLetter]--;
								if (player1.nameLetters[player1.selectedLetter] < 0) {
									player1.nameLetters[player1.selectedLetter] = letters.Length - 1;
								}
								player1.name = "" + letters[player1.nameLetters[0]] + letters[player1.nameLetters[1]] + letters[player1.nameLetters[2]];
								player1Name.text = player1.name;
								player1.inputTimer = 0;
							}
							if (Input.GetButton("ButtonA[1]")) { // Name selected, move to Ready
								player1Text.text = "READY!";
								player1Selector.text = "";
								playersReady++;
								player1.state = PlayerState.Ready;
								CheckReady();
								player1.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[1]")) { // Name Canceled, return to colour
								player1Text.text = "Select Colour";
								player1Selector.text = "";
								player1.state = PlayerState.Colour;
								player1.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Ready: {
							if (Input.GetButton("ButtonB[1]")) { // Ready Canceled, return to name
								player1Text.text = "Choose Name";
								player1Selector.text = "^";
								player1.state = PlayerState.Name;
								playersReady--;
								player1.inputTimer = -0.2f;
							}
							break;
						}
				}
			}
			// PS4 Controls
			if (Input.GetJoystickNames()[0] == "Wireless Controller") {

				switch (player1.state) {
					case PlayerState.NotJoined: {
							if (Input.GetButton("ButtonB[1]")) { // Player 1 joining, initiailise variables
								globalVariables.playersAmount++;
								player1.color = Color.red;
								player1.model = GameObject.Instantiate(carPrefabs[player1.selectedCar], player1Pos, Quaternion.Euler(0, 90, 0));
								player1.model.transform.Find("model").GetComponent<Renderer>().material.color = player1.color;
								player1Text.text = "Select Car";
								player1.state = PlayerState.Car;
								player1.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Car: {
							if (Input.GetAxis("DpadY[1]") > 0) { // Next car
								Destroy(player1.model);
								player1.selectedCar++;
								if (player1.selectedCar >= carPrefabs.Length) {
									player1.selectedCar = 0;
								}
								player1.model = GameObject.Instantiate(carPrefabs[player1.selectedCar], player1Pos, Quaternion.Euler(0, 90, 0));
								player1.model.transform.Find("model").GetComponent<Renderer>().material.color = player1.color;
								player1.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[1]") < 0) { // Previous Car
								Destroy(player1.model);
								player1.selectedCar--;
								if (player1.selectedCar < 0) {
									player1.selectedCar = carPrefabs.Length - 1;
								}
								player1.model = GameObject.Instantiate(carPrefabs[player1.selectedCar], player1Pos, Quaternion.Euler(0, 90, 0));
								player1.model.transform.Find("model").GetComponent<Renderer>().material.color = player1.color;
								player1.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[1]")) { // Car selected, move to colour
								player1Text.text = "Select Colour";
								player1.state = PlayerState.Colour;
								player1.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonX[1]")) { // Player 1 leaving, destroy variables
								globalVariables.playersAmount--;
								Destroy(player1.model);
								player1Text.text = "";
								player1.state = PlayerState.NotJoined;
								player1.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Colour: {
							if (Input.GetAxis("DpadY[1]") > 0) { // Next colour
								player1.color = CycleColourUp(player1.color);
								player1.model.transform.Find("model").GetComponent<Renderer>().material.color = player1.color;
							}
							if (Input.GetAxis("DpadY[1]") < 0) { // Previous Colour
								player1.color = CycleColourDown(player1.color);
								player1.model.transform.Find("model").GetComponent<Renderer>().material.color = player1.color;
							}
							if (Input.GetButton("ButtonB[1]")) { // Colour selected, move to Name
								player1Text.text = "Choose Name";
								player1Selector.text = "^";
								player1Name.text = player1.name;
								player1.state = PlayerState.Name;
								player1.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonX[1]")) { // Colour Canceled, return to car
								player1Text.text = "Select Car";
								player1.state = PlayerState.Car;
								player1.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Name: {
							if (Input.GetAxis("DpadY[1]") > 0) { // Next letter
								player1.selectedLetter++;
								if (player1.selectedLetter >= player1.nameLetters.Length) {
									player1.selectedLetter = 0;
								}
								if (player1.selectedLetter == 0) {
									player1Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player1.selectedLetter == 1) {
									player1Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player1.selectedLetter == 2) {
									player1Selector.alignment = TextAnchor.MiddleRight;
								}
								player1.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[1]") < 0) { // Previous letter
								player1.selectedLetter--;
								if (player1.selectedLetter < 0) {
									player1.selectedLetter = player1.nameLetters.Length - 1;
								}
								if (player1.selectedLetter == 0) {
									player1Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player1.selectedLetter == 1) {
									player1Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player1.selectedLetter == 2) {
									player1Selector.alignment = TextAnchor.MiddleRight;
								}
								player1.inputTimer = 0;
							}
							if (Input.GetAxis("DpadPS4[1]") > 0) { // Letter up
								player1.nameLetters[player1.selectedLetter]++;
								if (player1.nameLetters[player1.selectedLetter] >= letters.Length) {
									player1.nameLetters[player1.selectedLetter] = 0;
								}
								player1.name = "" + letters[player1.nameLetters[0]] + letters[player1.nameLetters[1]] + letters[player1.nameLetters[2]];
								player1Name.text = player1.name;
								player1.inputTimer = 0;
							}
							if (Input.GetAxis("DpadPS4[1]") < 0) { // Letter down
								player1.nameLetters[player1.selectedLetter]--;
								if (player1.nameLetters[player1.selectedLetter] < 0) {
									player1.nameLetters[player1.selectedLetter] = letters.Length - 1;
								}
								player1.name = "" + letters[player1.nameLetters[0]] + letters[player1.nameLetters[1]] + letters[player1.nameLetters[2]];
								player1Name.text = player1.name;
								player1.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[1]")) { // Name selected, move to Ready
								player1Text.text = "READY!";
								player1Selector.text = "";
								playersReady++;
								player1.state = PlayerState.Ready;
								CheckReady();
								player1.inputTimer = 0;
							}
							if (Input.GetButton("ButtonX[1]")) { // Name Canceled, return to colour
								player1Text.text = "Select Colour";
								player1Selector.text = "";
								player1.state = PlayerState.Colour;
								player1.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Ready: {
							if (Input.GetButton("ButtonX[1]")) { // Ready Canceled, return to name
								player1Text.text = "Choose Name";
								player1Selector.text = "^";
								player1.state = PlayerState.Name;
								playersReady--;
								player1.inputTimer = -0.2f;
							}
							break;
						}
				}
			}
		}
	}

	void Player2Inputs() {
		if (Input.GetJoystickNames().Length >= 2) {
			if (player2.state == PlayerState.NotJoined) {
				playerStartButtons[1].SetActive(true);
			} else {
				playerStartButtons[1].SetActive(false);
			}
			if (player2.inputTimer < inputDelay) {
				player2.inputTimer += Time.deltaTime;
				return;
			}
			// XBOX INPUTS
			if (Input.GetJoystickNames()[1] == "Controller (Xbox One For Windows)" || Input.GetJoystickNames()[1] == "Controller (XBOX One For Windows)" || Input.GetJoystickNames()[1] == "Controller (XBOX 360 For Windows)") {

				switch (player2.state) {
					case PlayerState.NotJoined: {
							if (Input.GetButton("ButtonA[2]")) { // Player 1 joining, initiailise variables
								globalVariables.playersAmount++;
								player2.color = Color.red;
								player2.model = GameObject.Instantiate(carPrefabs[player2.selectedCar], player2Pos, Quaternion.Euler(0, 90, 0));
								player2.model.transform.Find("model").GetComponent<Renderer>().material.color = player2.color;
								player2Text.text = "Select Car";
								player2.state = PlayerState.Car;
								player2.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Car: {
							if (Input.GetAxis("DpadX[2]") > 0) { // Next car
								Destroy(player2.model);
								player2.selectedCar++;
								if (player2.selectedCar >= carPrefabs.Length) {
									player2.selectedCar = 0;
								}
								player2.model = GameObject.Instantiate(carPrefabs[player2.selectedCar], player2Pos, Quaternion.Euler(0, 90, 0));
								player2.model.transform.Find("model").GetComponent<Renderer>().material.color = player2.color;
								player2.inputTimer = 0;
							}
							if (Input.GetAxis("DpadX[2]") < 0) { // Previous Car
								Destroy(player2.model);
								player2.selectedCar--;
								if (player2.selectedCar < 0) {
									player2.selectedCar = carPrefabs.Length - 1;
								}
								player2.model = GameObject.Instantiate(carPrefabs[player2.selectedCar], player2Pos, Quaternion.Euler(0, 90, 0));
								player2.model.transform.Find("model").GetComponent<Renderer>().material.color = player2.color;
								player2.inputTimer = 0;
							}
							if (Input.GetButton("ButtonA[2]")) { // Car selected, move to colour
								player2Text.text = "Select Colour";
								player2.state = PlayerState.Colour;
								player2.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonB[2]")) { // Player 1 leaving, destroy variables
								globalVariables.playersAmount--;
								Destroy(player2.model);
								player2Text.text = "";
								player2.state = PlayerState.NotJoined;
								player2.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Colour: {
							if (Input.GetAxis("DpadX[2]") > 0) { // Next colour
								player2.color = CycleColourUp(player2.color);
								player2.model.transform.Find("model").GetComponent<Renderer>().material.color = player2.color;
							}
							if (Input.GetAxis("DpadX[2]") < 0) { // Previous Colour
								player2.color = CycleColourDown(player2.color);
								player2.model.transform.Find("model").GetComponent<Renderer>().material.color = player2.color;
							}
							if (Input.GetButton("ButtonA[2]")) { // Colour selected, move to Name
								player2Text.text = "Choose Name";
								player2Selector.text = "^";
								player2Name.text = player2.name;
								player2.state = PlayerState.Name;
								player2.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonB[2]")) { // Colour Canceled, return to car
								player2Text.text = "Select Car";
								player2.state = PlayerState.Car;
								player2.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Name: {
							if (Input.GetAxis("DpadX[2]") > 0) { // Next letter
								player2.selectedLetter++;
								if (player2.selectedLetter >= player2.nameLetters.Length) {
									player2.selectedLetter = 0;
								}
								if (player2.selectedLetter == 0) {
									player2Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player2.selectedLetter == 1) {
									player2Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player2.selectedLetter == 2) {
									player2Selector.alignment = TextAnchor.MiddleRight;
								}
								player2.inputTimer = 0;
							}
							if (Input.GetAxis("DpadX[2]") < 0) { // Previous letter
								player2.selectedLetter--;
								if (player2.selectedLetter < 0) {
									player2.selectedLetter = player2.nameLetters.Length - 1;
								}
								if (player2.selectedLetter == 0) {
									player2Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player2.selectedLetter == 1) {
									player2Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player2.selectedLetter == 2) {
									player2Selector.alignment = TextAnchor.MiddleRight;
								}
								player2.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[2]") > 0) { // Letter up
								player2.nameLetters[player2.selectedLetter]++;
								if (player2.nameLetters[player2.selectedLetter] >= letters.Length) {
									player2.nameLetters[player2.selectedLetter] = 0;
								}
								player2.name = "" + letters[player2.nameLetters[0]] + letters[player2.nameLetters[1]] + letters[player2.nameLetters[2]];
								player2Name.text = player2.name;
								player2.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[2]") < 0) { // Letter down
								player2.nameLetters[player2.selectedLetter]--;
								if (player2.nameLetters[player2.selectedLetter] < 0) {
									player2.nameLetters[player2.selectedLetter] = letters.Length - 1;
								}
								player2.name = "" + letters[player2.nameLetters[0]] + letters[player2.nameLetters[1]] + letters[player2.nameLetters[2]];
								player2Name.text = player2.name;
								player2.inputTimer = 0;
							}
							if (Input.GetButton("ButtonA[2]")) { // Name selected, move to Ready
								player2Text.text = "READY!";
								player2Selector.text = "";
								playersReady++;
								player2.state = PlayerState.Ready;
								CheckReady();
								player2.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[2]")) { // Name Canceled, return to colour
								player2Text.text = "Select Colour";
								player2Selector.text = "";
								player2.state = PlayerState.Colour;
								player2.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Ready: {
							if (Input.GetButton("ButtonB[2]")) { // Ready Canceled, return to name
								player2Text.text = "Choose Name";
								player2Selector.text = "^";
								player2.state = PlayerState.Name;
								playersReady--;
								player2.inputTimer = -0.2f;
							}
							break;
						}
				}
			}
			// PS4 Controls
			if (Input.GetJoystickNames()[1] == "Wireless Controller") {

				switch (player2.state) {
					case PlayerState.NotJoined: {
							if (Input.GetButton("ButtonB[2]")) { // Player 1 joining, initiailise variables
								globalVariables.playersAmount++;
								player2.color = Color.red;
								player2.model = GameObject.Instantiate(carPrefabs[player2.selectedCar], player2Pos, Quaternion.Euler(0, 90, 0));
								player2.model.transform.Find("model").GetComponent<Renderer>().material.color = player2.color;
								player2Text.text = "Select Car";
								player2.state = PlayerState.Car;
								player2.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Car: {
							if (Input.GetAxis("DpadY[2]") > 0) { // Next car
								Destroy(player2.model);
								player2.selectedCar++;
								if (player2.selectedCar >= carPrefabs.Length) {
									player2.selectedCar = 0;
								}
								player2.model = GameObject.Instantiate(carPrefabs[player2.selectedCar], player2Pos, Quaternion.Euler(0, 90, 0));
								player2.model.transform.Find("model").GetComponent<Renderer>().material.color = player2.color;
								player2.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[2]") < 0) { // Previous Car
								Destroy(player2.model);
								player2.selectedCar--;
								if (player2.selectedCar < 0) {
									player2.selectedCar = carPrefabs.Length - 1;
								}
								player2.model = GameObject.Instantiate(carPrefabs[player2.selectedCar], player2Pos, Quaternion.Euler(0, 90, 0));
								player2.model.transform.Find("model").GetComponent<Renderer>().material.color = player2.color;
								player2.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[2]")) { // Car selected, move to colour
								player2Text.text = "Select Colour";
								player2.state = PlayerState.Colour;
								player2.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonX[2]")) { // Player 1 leaving, destroy variables
								globalVariables.playersAmount--;
								Destroy(player2.model);
								player2Text.text = "";
								player2.state = PlayerState.NotJoined;
								player2.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Colour: {
							if (Input.GetAxis("DpadY[2]") > 0) { // Next colour
								player2.color = CycleColourUp(player2.color);
								player2.model.transform.Find("model").GetComponent<Renderer>().material.color = player2.color;
							}
							if (Input.GetAxis("DpadY[2]") < 0) { // Previous Colour
								player2.color = CycleColourDown(player2.color);
								player2.model.transform.Find("model").GetComponent<Renderer>().material.color = player2.color;
							}
							if (Input.GetButton("ButtonB[2]")) { // Colour selected, move to Name
								player2Text.text = "Choose Name";
								player2Selector.text = "^";
								player2Name.text = player2.name;
								player2.state = PlayerState.Name;
								player2.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonX[2]")) { // Colour Canceled, return to car
								player2Text.text = "Select Car";
								player2.state = PlayerState.Car;
								player2.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Name: {
							if (Input.GetAxis("DpadY[2]") > 0) { // Next letter
								player2.selectedLetter++;
								if (player2.selectedLetter >= player2.nameLetters.Length) {
									player2.selectedLetter = 0;
								}
								if (player2.selectedLetter == 0) {
									player2Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player2.selectedLetter == 1) {
									player2Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player2.selectedLetter == 2) {
									player2Selector.alignment = TextAnchor.MiddleRight;
								}
								player2.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[2]") < 0) { // Previous letter
								player2.selectedLetter--;
								if (player2.selectedLetter < 0) {
									player2.selectedLetter = player2.nameLetters.Length - 1;
								}
								if (player2.selectedLetter == 0) {
									player2Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player2.selectedLetter == 1) {
									player2Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player2.selectedLetter == 2) {
									player2Selector.alignment = TextAnchor.MiddleRight;
								}
								player2.inputTimer = 0;
							}
							if (Input.GetAxis("DpadPS4[2]") > 0) { // Letter up
								player2.nameLetters[player2.selectedLetter]++;
								if (player2.nameLetters[player2.selectedLetter] >= letters.Length) {
									player2.nameLetters[player2.selectedLetter] = 0;
								}
								player2.name = "" + letters[player2.nameLetters[0]] + letters[player2.nameLetters[1]] + letters[player2.nameLetters[2]];
								player2Name.text = player2.name;
								player2.inputTimer = 0;
							}
							if (Input.GetAxis("DpadPS4[2]") < 0) { // Letter down
								player2.nameLetters[player2.selectedLetter]--;
								if (player2.nameLetters[player2.selectedLetter] < 0) {
									player2.nameLetters[player2.selectedLetter] = letters.Length - 1;
								}
								player2.name = "" + letters[player2.nameLetters[0]] + letters[player2.nameLetters[1]] + letters[player2.nameLetters[2]];
								player2Name.text = player2.name;
								player2.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[2]")) { // Name selected, move to Ready
								player2Text.text = "READY!";
								player2Selector.text = "";
								playersReady++;
								player2.state = PlayerState.Ready;
								CheckReady();
								player2.inputTimer = 0;
							}
							if (Input.GetButton("ButtonX[2]")) { // Name Canceled, return to colour
								player2Text.text = "Select Colour";
								player2Selector.text = "";
								player2.state = PlayerState.Colour;
								player2.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Ready: {
							if (Input.GetButton("ButtonX[2]")) { // Ready Canceled, return to name
								player2Text.text = "Choose Name";
								player2Selector.text = "^";
								player2.state = PlayerState.Name;
								playersReady--;
								player2.inputTimer = -0.2f;
							}
							break;
						}
				}
			}
		}
	}

	void Player3Inputs() {
		if (Input.GetJoystickNames().Length >= 3) {
			if (player3.state == PlayerState.NotJoined) {
				playerStartButtons[2].SetActive(true);
			} else {
				playerStartButtons[2].SetActive(false);
			}
			if (player3.inputTimer < inputDelay) {
				player3.inputTimer += Time.deltaTime;
				return;
			}
			// XBOX INPUTS
			if (Input.GetJoystickNames()[2] == "Controller (Xbox One For Windows)" || Input.GetJoystickNames()[2] == "Controller (XBOX One For Windows)" || Input.GetJoystickNames()[2] == "Controller (XBOX 360 For Windows)") {

				switch (player3.state) {
					case PlayerState.NotJoined: {
							if (Input.GetButton("ButtonA[3]")) { // Player 1 joining, initiailise variables
								globalVariables.playersAmount++;
								player3.color = Color.red;
								player3.model = GameObject.Instantiate(carPrefabs[player3.selectedCar], player3Pos, Quaternion.Euler(0, 90, 0));
								player3.model.transform.Find("model").GetComponent<Renderer>().material.color = player3.color;
								player3Text.text = "Select Car";
								player3.state = PlayerState.Car;
								player3.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Car: {
							if (Input.GetAxis("DpadX[3]") > 0) { // Next car
								Destroy(player3.model);
								player3.selectedCar++;
								if (player3.selectedCar >= carPrefabs.Length) {
									player3.selectedCar = 0;
								}
								player3.model = GameObject.Instantiate(carPrefabs[player3.selectedCar], player3Pos, Quaternion.Euler(0, 90, 0));
								player3.model.transform.Find("model").GetComponent<Renderer>().material.color = player3.color;
								player3.inputTimer = 0;
							}
							if (Input.GetAxis("DpadX[3]") < 0) { // Previous Car
								Destroy(player3.model);
								player3.selectedCar--;
								if (player3.selectedCar < 0) {
									player3.selectedCar = carPrefabs.Length - 1;
								}
								player3.model = GameObject.Instantiate(carPrefabs[player3.selectedCar], player3Pos, Quaternion.Euler(0, 90, 0));
								player3.model.transform.Find("model").GetComponent<Renderer>().material.color = player3.color;
								player3.inputTimer = 0;
							}
							if (Input.GetButton("ButtonA[3]")) { // Car selected, move to colour
								player3Text.text = "Select Colour";
								player3.state = PlayerState.Colour;
								player3.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonB[3]")) { // Player 1 leaving, destroy variables
								globalVariables.playersAmount--;
								Destroy(player3.model);
								player3Text.text = "";
								player3.state = PlayerState.NotJoined;
								player3.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Colour: {
							if (Input.GetAxis("DpadX[3]") > 0) { // Next colour
								player3.color = CycleColourUp(player3.color);
								player3.model.transform.Find("model").GetComponent<Renderer>().material.color = player3.color;
							}
							if (Input.GetAxis("DpadX[3]") < 0) { // Previous Colour
								player3.color = CycleColourDown(player3.color);
								player3.model.transform.Find("model").GetComponent<Renderer>().material.color = player3.color;
							}
							if (Input.GetButton("ButtonA[3]")) { // Colour selected, move to Name
								player3Text.text = "Choose Name";
								player3Selector.text = "^";
								player3Name.text = player3.name;
								player3.state = PlayerState.Name;
								player3.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonB[3]")) { // Colour Canceled, return to car
								player3Text.text = "Select Car";
								player3.state = PlayerState.Car;
								player3.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Name: {
							if (Input.GetAxis("DpadX[3]") > 0) { // Next letter
								player3.selectedLetter++;
								if (player3.selectedLetter >= player3.nameLetters.Length) {
									player3.selectedLetter = 0;
								}
								if (player3.selectedLetter == 0) {
									player3Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player3.selectedLetter == 1) {
									player3Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player3.selectedLetter == 2) {
									player3Selector.alignment = TextAnchor.MiddleRight;
								}
								player3.inputTimer = 0;
							}
							if (Input.GetAxis("DpadX[3]") < 0) { // Previous letter
								player3.selectedLetter--;
								if (player3.selectedLetter < 0) {
									player3.selectedLetter = player3.nameLetters.Length - 1;
								}
								if (player3.selectedLetter == 0) {
									player3Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player3.selectedLetter == 1) {
									player3Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player3.selectedLetter == 2) {
									player3Selector.alignment = TextAnchor.MiddleRight;
								}
								player3.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[3]") > 0) { // Letter up
								player3.nameLetters[player3.selectedLetter]++;
								if (player3.nameLetters[player3.selectedLetter] >= letters.Length) {
									player3.nameLetters[player3.selectedLetter] = 0;
								}
								player3.name = "" + letters[player3.nameLetters[0]] + letters[player3.nameLetters[1]] + letters[player3.nameLetters[2]];
								player3Name.text = player3.name;
								player3.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[3]") < 0) { // Letter down
								player3.nameLetters[player3.selectedLetter]--;
								if (player3.nameLetters[player3.selectedLetter] < 0) {
									player3.nameLetters[player3.selectedLetter] = letters.Length - 1;
								}
								player3.name = "" + letters[player3.nameLetters[0]] + letters[player3.nameLetters[1]] + letters[player3.nameLetters[2]];
								player3Name.text = player3.name;
								player3.inputTimer = 0;
							}
							if (Input.GetButton("ButtonA[3]")) { // Name selected, move to Ready
								player3Text.text = "READY!";
								player3Selector.text = "";
								playersReady++;
								player3.state = PlayerState.Ready;
								CheckReady();
								player3.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[3]")) { // Name Canceled, return to colour
								player3Text.text = "Select Colour";
								player3Selector.text = "";
								player3.state = PlayerState.Colour;
								player3.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Ready: {
							if (Input.GetButton("ButtonB[3]")) { // Ready Canceled, return to name
								player3Text.text = "Choose Name";
								player3Selector.text = "^";
								player3.state = PlayerState.Name;
								playersReady--;
								player3.inputTimer = -0.2f;
							}
							break;
						}
				}
			}
			// PS4 Controls
			if (Input.GetJoystickNames()[2] == "Wireless Controller") {

				switch (player3.state) {
					case PlayerState.NotJoined: {
							if (Input.GetButton("ButtonB[3]")) { // Player 1 joining, initiailise variables
								globalVariables.playersAmount++;
								player3.color = Color.red;
								player3.model = GameObject.Instantiate(carPrefabs[player3.selectedCar], player3Pos, Quaternion.Euler(0, 90, 0));
								player3.model.transform.Find("model").GetComponent<Renderer>().material.color = player3.color;
								player3Text.text = "Select Car";
								player3.state = PlayerState.Car;
								player3.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Car: {
							if (Input.GetAxis("DpadY[3]") > 0) { // Next car
								Destroy(player3.model);
								player3.selectedCar++;
								if (player3.selectedCar >= carPrefabs.Length) {
									player3.selectedCar = 0;
								}
								player3.model = GameObject.Instantiate(carPrefabs[player3.selectedCar], player3Pos, Quaternion.Euler(0, 90, 0));
								player3.model.transform.Find("model").GetComponent<Renderer>().material.color = player3.color;
								player3.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[3]") < 0) { // Previous Car
								Destroy(player3.model);
								player3.selectedCar--;
								if (player3.selectedCar < 0) {
									player3.selectedCar = carPrefabs.Length - 1;
								}
								player3.model = GameObject.Instantiate(carPrefabs[player3.selectedCar], player3Pos, Quaternion.Euler(0, 90, 0));
								player3.model.transform.Find("model").GetComponent<Renderer>().material.color = player3.color;
								player3.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[3]")) { // Car selected, move to colour
								player3Text.text = "Select Colour";
								player3.state = PlayerState.Colour;
								player3.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonX[3]")) { // Player 1 leaving, destroy variables
								globalVariables.playersAmount--;
								Destroy(player3.model);
								player3Text.text = "";
								player3.state = PlayerState.NotJoined;
								player3.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Colour: {
							if (Input.GetAxis("DpadY[3]") > 0) { // Next colour
								player3.color = CycleColourUp(player3.color);
								player3.model.transform.Find("model").GetComponent<Renderer>().material.color = player3.color;
							}
							if (Input.GetAxis("DpadY[3]") < 0) { // Previous Colour
								player3.color = CycleColourDown(player3.color);
								player3.model.transform.Find("model").GetComponent<Renderer>().material.color = player3.color;
							}
							if (Input.GetButton("ButtonB[3]")) { // Colour selected, move to Name
								player3Text.text = "Choose Name";
								player3Selector.text = "^";
								player3Name.text = player3.name;
								player3.state = PlayerState.Name;
								player3.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonX[3]")) { // Colour Canceled, return to car
								player3Text.text = "Select Car";
								player3.state = PlayerState.Car;
								player3.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Name: {
							if (Input.GetAxis("DpadY[3]") > 0) { // Next letter
								player3.selectedLetter++;
								if (player3.selectedLetter >= player3.nameLetters.Length) {
									player3.selectedLetter = 0;
								}
								if (player3.selectedLetter == 0) {
									player3Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player3.selectedLetter == 1) {
									player3Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player3.selectedLetter == 2) {
									player3Selector.alignment = TextAnchor.MiddleRight;
								}
								player3.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[3]") < 0) { // Previous letter
								player3.selectedLetter--;
								if (player3.selectedLetter < 0) {
									player3.selectedLetter = player3.nameLetters.Length - 1;
								}
								if (player3.selectedLetter == 0) {
									player3Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player3.selectedLetter == 1) {
									player3Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player3.selectedLetter == 2) {
									player3Selector.alignment = TextAnchor.MiddleRight;
								}
								player3.inputTimer = 0;
							}
							if (Input.GetAxis("DpadPS4[3]") > 0) { // Letter up
								player3.nameLetters[player3.selectedLetter]++;
								if (player3.nameLetters[player3.selectedLetter] >= letters.Length) {
									player3.nameLetters[player3.selectedLetter] = 0;
								}
								player3.name = "" + letters[player3.nameLetters[0]] + letters[player3.nameLetters[1]] + letters[player3.nameLetters[2]];
								player3Name.text = player3.name;
								player3.inputTimer = 0;
							}
							if (Input.GetAxis("DpadPS4[3]") < 0) { // Letter down
								player3.nameLetters[player3.selectedLetter]--;
								if (player3.nameLetters[player3.selectedLetter] < 0) {
									player3.nameLetters[player3.selectedLetter] = letters.Length - 1;
								}
								player3.name = "" + letters[player3.nameLetters[0]] + letters[player3.nameLetters[1]] + letters[player3.nameLetters[2]];
								player3Name.text = player3.name;
								player3.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[3]")) { // Name selected, move to Ready
								player3Text.text = "READY!";
								player3Selector.text = "";
								playersReady++;
								player3.state = PlayerState.Ready;
								CheckReady();
								player3.inputTimer = 0;
							}
							if (Input.GetButton("ButtonX[3]")) { // Name Canceled, return to colour
								player3Text.text = "Select Colour";
								player3Selector.text = "";
								player3.state = PlayerState.Colour;
								player3.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Ready: {
							if (Input.GetButton("ButtonX[3]")) { // Ready Canceled, return to name
								player3Text.text = "Choose Name";
								player3Selector.text = "^";
								player3.state = PlayerState.Name;
								playersReady--;
								player3.inputTimer = -0.2f;
							}
							break;
						}
				}
			}
		}
	}

	void Player4Inputs() {
		if (Input.GetJoystickNames().Length >= 4) {
			if (player4.state == PlayerState.NotJoined) {
				playerStartButtons[3].SetActive(true);
			} else {
				playerStartButtons[3].SetActive(false);
			}
			if (player4.inputTimer < inputDelay) {
				player4.inputTimer += Time.deltaTime;
				return;
			}
			// XBOX INPUTS
			if (Input.GetJoystickNames()[3] == "Controller (Xbox One For Windows)" || Input.GetJoystickNames()[3] == "Controller (XBOX One For Windows)" || Input.GetJoystickNames()[3] == "Controller (XBOX 360 For Windows)") {

				switch (player4.state) {
					case PlayerState.NotJoined: {
							if (Input.GetButton("ButtonA[4]")) { // Player 1 joining, initiailise variables
								globalVariables.playersAmount++;
								player4.color = Color.red;
								player4.model = GameObject.Instantiate(carPrefabs[player4.selectedCar], player4Pos, Quaternion.Euler(0, 90, 0));
								player4.model.transform.Find("model").GetComponent<Renderer>().material.color = player4.color;
								player4Text.text = "Select Car";
								player4.state = PlayerState.Car;
								player4.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Car: {
							if (Input.GetAxis("DpadX[4]") > 0) { // Next car
								Destroy(player4.model);
								player4.selectedCar++;
								if (player4.selectedCar >= carPrefabs.Length) {
									player4.selectedCar = 0;
								}
								player4.model = GameObject.Instantiate(carPrefabs[player4.selectedCar], player4Pos, Quaternion.Euler(0, 90, 0));
								player4.model.transform.Find("model").GetComponent<Renderer>().material.color = player4.color;
								player4.inputTimer = 0;
							}
							if (Input.GetAxis("DpadX[4]") < 0) { // Previous Car
								Destroy(player4.model);
								player4.selectedCar--;
								if (player4.selectedCar < 0) {
									player4.selectedCar = carPrefabs.Length - 1;
								}
								player4.model = GameObject.Instantiate(carPrefabs[player4.selectedCar], player4Pos, Quaternion.Euler(0, 90, 0));
								player4.model.transform.Find("model").GetComponent<Renderer>().material.color = player4.color;
								player4.inputTimer = 0;
							}
							if (Input.GetButton("ButtonA[4]")) { // Car selected, move to colour
								player4Text.text = "Select Colour";
								player4.state = PlayerState.Colour;
								player4.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonB[4]")) { // Player 1 leaving, destroy variables
								globalVariables.playersAmount--;
								Destroy(player4.model);
								player4Text.text = "";
								player4.state = PlayerState.NotJoined;
								player4.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Colour: {
							if (Input.GetAxis("DpadX[4]") > 0) { // Next colour
								player4.color = CycleColourUp(player4.color);
								player4.model.transform.Find("model").GetComponent<Renderer>().material.color = player4.color;
							}
							if (Input.GetAxis("DpadX[4]") < 0) { // Previous Colour
								player4.color = CycleColourDown(player4.color);
								player4.model.transform.Find("model").GetComponent<Renderer>().material.color = player4.color;
							}
							if (Input.GetButton("ButtonA[4]")) { // Colour selected, move to Name
								player4Text.text = "Choose Name";
								player4Selector.text = "^";
								player4Name.text = player4.name;
								player4.state = PlayerState.Name;
								player4.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonB[4]")) { // Colour Canceled, return to car
								player4Text.text = "Select Car";
								player4.state = PlayerState.Car;
								player4.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Name: {
							if (Input.GetAxis("DpadX[4]") > 0) { // Next letter
								player4.selectedLetter++;
								if (player4.selectedLetter >= player4.nameLetters.Length) {
									player4.selectedLetter = 0;
								}
								if (player4.selectedLetter == 0) {
									player4Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player4.selectedLetter == 1) {
									player4Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player4.selectedLetter == 2) {
									player4Selector.alignment = TextAnchor.MiddleRight;
								}
								player4.inputTimer = 0;
							}
							if (Input.GetAxis("DpadX[4]") < 0) { // Previous letter
								player4.selectedLetter--;
								if (player4.selectedLetter < 0) {
									player4.selectedLetter = player4.nameLetters.Length - 1;
								}
								if (player4.selectedLetter == 0) {
									player4Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player4.selectedLetter == 1) {
									player4Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player4.selectedLetter == 2) {
									player4Selector.alignment = TextAnchor.MiddleRight;
								}
								player4.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[4]") > 0) { // Letter up
								player4.nameLetters[player4.selectedLetter]++;
								if (player4.nameLetters[player4.selectedLetter] >= letters.Length) {
									player4.nameLetters[player4.selectedLetter] = 0;
								}
								player4.name = "" + letters[player4.nameLetters[0]] + letters[player4.nameLetters[1]] + letters[player4.nameLetters[2]];
								player4Name.text = player4.name;
								player4.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[4]") < 0) { // Letter down
								player4.nameLetters[player4.selectedLetter]--;
								if (player4.nameLetters[player4.selectedLetter] < 0) {
									player4.nameLetters[player4.selectedLetter] = letters.Length - 1;
								}
								player4.name = "" + letters[player4.nameLetters[0]] + letters[player4.nameLetters[1]] + letters[player4.nameLetters[2]];
								player4Name.text = player4.name;
								player4.inputTimer = 0;
							}
							if (Input.GetButton("ButtonA[4]")) { // Name selected, move to Ready
								player4Text.text = "READY!";
								player4Selector.text = "";
								playersReady++;
								player4.state = PlayerState.Ready;
								CheckReady();
								player4.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[4]")) { // Name Canceled, return to colour
								player4Text.text = "Select Colour";
								player4Selector.text = "";
								player4.state = PlayerState.Colour;
								player4.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Ready: {
							if (Input.GetButton("ButtonB[4]")) { // Ready Canceled, return to name
								player4Text.text = "Choose Name";
								player4Selector.text = "^";
								player4.state = PlayerState.Name;
								playersReady--;
								player4.inputTimer = -0.2f;
							}
							break;
						}
				}
			}
			// PS4 Controls
			if (Input.GetJoystickNames()[3] == "Wireless Controller") {

				switch (player4.state) {
					case PlayerState.NotJoined: {
							if (Input.GetButton("ButtonB[4]")) { // Player 1 joining, initiailise variables
								globalVariables.playersAmount++;
								player4.color = Color.red;
								player4.model = GameObject.Instantiate(carPrefabs[player4.selectedCar], player4Pos, Quaternion.Euler(0, 90, 0));
								player4.model.transform.Find("model").GetComponent<Renderer>().material.color = player4.color;
								player4Text.text = "Select Car";
								player4.state = PlayerState.Car;
								player4.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Car: {
							if (Input.GetAxis("DpadY[4]") > 0) { // Next car
								Destroy(player4.model);
								player4.selectedCar++;
								if (player4.selectedCar >= carPrefabs.Length) {
									player4.selectedCar = 0;
								}
								player4.model = GameObject.Instantiate(carPrefabs[player4.selectedCar], player4Pos, Quaternion.Euler(0, 90, 0));
								player4.model.transform.Find("model").GetComponent<Renderer>().material.color = player4.color;
								player4.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[4]") < 0) { // Previous Car
								Destroy(player4.model);
								player4.selectedCar--;
								if (player4.selectedCar < 0) {
									player4.selectedCar = carPrefabs.Length - 1;
								}
								player4.model = GameObject.Instantiate(carPrefabs[player4.selectedCar], player4Pos, Quaternion.Euler(0, 90, 0));
								player4.model.transform.Find("model").GetComponent<Renderer>().material.color = player4.color;
								player4.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[4]")) { // Car selected, move to colour
								player4Text.text = "Select Colour";
								player4.state = PlayerState.Colour;
								player4.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonX[4]")) { // Player 1 leaving, destroy variables
								globalVariables.playersAmount--;
								Destroy(player4.model);
								player4Text.text = "";
								player4.state = PlayerState.NotJoined;
								player4.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Colour: {
							if (Input.GetAxis("DpadY[4]") > 0) { // Next colour
								player4.color = CycleColourUp(player4.color);
								player4.model.transform.Find("model").GetComponent<Renderer>().material.color = player4.color;
							}
							if (Input.GetAxis("DpadY[4]") < 0) { // Previous Colour
								player4.color = CycleColourDown(player4.color);
								player4.model.transform.Find("model").GetComponent<Renderer>().material.color = player4.color;
							}
							if (Input.GetButton("ButtonB[4]")) { // Colour selected, move to Name
								player4Text.text = "Choose Name";
								player4Selector.text = "^";
								player4Name.text = player4.name;
								player4.state = PlayerState.Name;
								player4.inputTimer = -0.2f;
							}
							if (Input.GetButton("ButtonX[4]")) { // Colour Canceled, return to car
								player4Text.text = "Select Car";
								player4.state = PlayerState.Car;
								player4.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Name: {
							if (Input.GetAxis("DpadY[4]") > 0) { // Next letter
								player4.selectedLetter++;
								if (player4.selectedLetter >= player4.nameLetters.Length) {
									player4.selectedLetter = 0;
								}
								if (player4.selectedLetter == 0) {
									player4Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player4.selectedLetter == 1) {
									player4Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player4.selectedLetter == 2) {
									player4Selector.alignment = TextAnchor.MiddleRight;
								}
								player4.inputTimer = 0;
							}
							if (Input.GetAxis("DpadY[4]") < 0) { // Previous letter
								player4.selectedLetter--;
								if (player4.selectedLetter < 0) {
									player4.selectedLetter = player4.nameLetters.Length - 1;
								}
								if (player4.selectedLetter == 0) {
									player4Selector.alignment = TextAnchor.MiddleLeft;
								}
								if (player4.selectedLetter == 1) {
									player4Selector.alignment = TextAnchor.MiddleCenter;
								}
								if (player4.selectedLetter == 2) {
									player4Selector.alignment = TextAnchor.MiddleRight;
								}
								player4.inputTimer = 0;
							}
							if (Input.GetAxis("DpadPS4[4]") > 0) { // Letter up
								player4.nameLetters[player4.selectedLetter]++;
								if (player4.nameLetters[player4.selectedLetter] >= letters.Length) {
									player4.nameLetters[player4.selectedLetter] = 0;
								}
								player4.name = "" + letters[player4.nameLetters[0]] + letters[player4.nameLetters[1]] + letters[player4.nameLetters[2]];
								player4Name.text = player4.name;
								player4.inputTimer = 0;
							}
							if (Input.GetAxis("DpadPS4[4]") < 0) { // Letter down
								player4.nameLetters[player4.selectedLetter]--;
								if (player4.nameLetters[player4.selectedLetter] < 0) {
									player4.nameLetters[player4.selectedLetter] = letters.Length - 1;
								}
								player4.name = "" + letters[player4.nameLetters[0]] + letters[player4.nameLetters[1]] + letters[player4.nameLetters[2]];
								player4Name.text = player4.name;
								player4.inputTimer = 0;
							}
							if (Input.GetButton("ButtonB[4]")) { // Name selected, move to Ready
								player4Text.text = "READY!";
								player4Selector.text = "";
								playersReady++;
								player4.state = PlayerState.Ready;
								CheckReady();
								player4.inputTimer = 0;
							}
							if (Input.GetButton("ButtonX[4]")) { // Name Canceled, return to colour
								player4Text.text = "Select Colour";
								player4Selector.text = "";
								player4.state = PlayerState.Colour;
								player4.inputTimer = -0.2f;
							}
							break;
						}
					case PlayerState.Ready: {
							if (Input.GetButton("ButtonX[4]")) { // Ready Canceled, return to name
								player4Text.text = "Choose Name";
								player4Selector.text = "^";
								player4.state = PlayerState.Name;
								playersReady--;
								player4.inputTimer = -0.2f;
							}
							break;
						}
				}
			}
		}
	}

	void CheckReady() {
		if(globalVariables.playersAmount >= 2 && globalVariables.playersAmount == playersReady) {
			// START GAME
			globalVariables.player1Controller = player1.state == PlayerState.Ready ? true : false;
			globalVariables.player2Controller = player2.state == PlayerState.Ready ? true : false;
			globalVariables.player3Controller = player3.state == PlayerState.Ready ? true : false;
			globalVariables.player4Controller = player4.state == PlayerState.Ready ? true : false;
			globalVariables.colourArray[0] = player1.color;
			globalVariables.carSelected[0] = player1.selectedCar;
			globalVariables.playerNames[0] = player1.name;
			globalVariables.colourArray[1] = player2.color;
			globalVariables.carSelected[1] = player2.selectedCar;
			globalVariables.playerNames[1] = player2.name;
			globalVariables.colourArray[2] = player3.color;
			globalVariables.carSelected[2] = player3.selectedCar;
			globalVariables.playerNames[2] = player3.name;
			globalVariables.colourArray[3] = player4.color;
			globalVariables.carSelected[3] = player4.selectedCar;
			globalVariables.playerNames[3] = player4.name;
			globalVariables.LoadScene(2);
		}
	}

	Color CycleColourDown(Color col) {
		if (col.r >= 1 && col.g < 1) {
			col.g += Time.deltaTime;
		}
		if (col.r > 0 && col.g >= 1) {
			col.r -= Time.deltaTime;
		}
		if (col.g >= 1 && col.b < 1) {
			col.b += Time.deltaTime;
		}
		if (col.g > 0 && col.b >= 1) {
			col.g -= Time.deltaTime;
		}
		if (col.b >= 1 && col.r < 1) {
			col.r += Time.deltaTime;
		}
		if (col.b > 0 && col.r >= 1) {
			col.b -= Time.deltaTime;
		}
		return col;
	}

	Color CycleColourUp(Color col) {
		if(col.r >= 1 && col.g <= 0) {
			col.b += Time.deltaTime;
		}
		if(col.r > 0 && col.b >= 1) {
			col.r -= Time.deltaTime;
		}
		if(col.b >= 1 && col.r <= 0) {
			col.g += Time.deltaTime;
		}
		if(col.b > 0 && col.g >= 1) {
			col.b -= Time.deltaTime;
		}
		if(col.g >= 1 && col.b <= 0) {
			col.r += Time.deltaTime;
		}
		if(col.g > 0 && col.r >= 1) {
			col.g -= Time.deltaTime;
		}
		return col;
	}

}
