﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GraphicsQuality : MonoBehaviour {
    // <summary>
    //This is accesible in the Unity inspector,
    //but not in code outside this class.
    // <summary>
    [SerializeField]
    Dropdown setGraphics;
    [SerializeField]
    Dropdown setAntiAliasing;
    [SerializeField]
    Dropdown setShadows;

    // <summary>
    //Index values to be used and track 
    //when the user makes changes in options
    //and the changes are being saves
    // <summary>
    private int activeScreenResIndex;

    // <summary>
    // These arethe options for the graphics of the game
    // <summary>
    enum GraphicsLevel {
        Select_an_option,
        Low,
        Medium,
        High
    }

    // <summary>
    //When the game starts, the dropdown list will be populated
    //with the enum values
    // <summary>
    void Start() {
        PopulateList();
    }

    // <summary>
    //
    // <summary>
    void Update() {
     switch (setGraphics.value) {
            case 1:
                QualitySettings.SetQualityLevel(0);
                Debug.Log("Low Graphics selected!");
                break;
            case 2:
                QualitySettings.SetQualityLevel(1);
                Debug.Log("Medium Graphics selected!");
                break;

            case 3:
                QualitySettings.SetQualityLevel(2);
                Debug.Log("High Graphics selected!");
                break;
        }
        PlayerPrefs.SetInt("GraphicsQuality", activeScreenResIndex);
        PlayerPrefs.Save();
    }

    // <summary>
    //
    // <summary>
    void PopulateList() {
        string[] enumNames = Enum.GetNames(typeof(GraphicsLevel));
        List<string> names = new List<string>(enumNames);
        setGraphics.AddOptions(names);
    }

    // <summary>
    //
    // <summary>
    public void onAA() {
        QualitySettings.antiAliasing = (int)Mathf.Pow(2.0f,setAntiAliasing.value);

        PlayerPrefs.SetInt("AA", activeScreenResIndex);
        PlayerPrefs.Save();
    }

}
