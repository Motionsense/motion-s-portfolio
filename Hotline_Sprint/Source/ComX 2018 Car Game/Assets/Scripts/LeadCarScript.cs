﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeadCarScript : MonoBehaviour {

	HealthbarScript healthBar;
	//SceneStart sceneStart; - SLOW DISABLED
	CarController carController;
	PlayerController playerController;
	WinCondition winCondition;
	public bool isLeader = false;
	float health = 100;
	float hitDamageModifier = 1f;
	//float slowDamageModifier = 1f; - SLOW DISABLED
	//float speedDamageLimit = 10; // How slow the car can go before taking damage - SLOW DISABLED
	//bool canTakeSlowDamage = true;  - SLOW DISABLED
	[SerializeField]
	List<GameObject> ignoreCollisions = new List<GameObject>();
	float ignoreCollisionTime = 1f;
	[SerializeField]
	GameObject destinationIcon;
	GameObject destinationObject;
    const int speedOffset = 5; // How much faster the lead car is than the police
	//Vector3 lastPosition; - SLOW DISABLED

	void Start() {
		if (!isLeader) {
			return;
		}
		winCondition = GameObject.Find("GameMechanics").GetComponent<WinCondition>();
		//sceneStart = GameObject.Find("GameMechanics").GetComponent<SceneStart>(); - SLOW DISABLED
		carController = gameObject.GetComponent<CarController>();
		carController.maxSpeed += speedOffset;
		playerController = GameObject.Find("GameMechanics").GetComponent<PlayerController>();
		healthBar = GameObject.Find("Canvas").transform.Find("HealthBar").GetComponent<HealthbarScript>();
		destinationObject = GameObject.Find("Roads").GetComponentInChildren<DestinationScript>().gameObject;
		//lastPosition = transform.position; - SLOW DISABLED
	}

	void Update() {
		if (destinationIcon != null && destinationObject != null) {
			destinationIcon.transform.LookAt(destinationObject.transform.position);
		}
	}


	//void FixedUpdate() { - SLOW DISABLED
	//	if (!isLeader) {
	//		return;
	//	}

	//	float speed = Vector3.Distance(lastPosition, transform.position) * 10000;
	//	speed = Mathf.Round(speed);
	//	speed = speed / 100;
	//	lastPosition = transform.position;

	//	if (speed < speedDamageLimit && !sceneStart.starting && canTakeSlowDamage) {
	//		TakeDamage(slowDamageModifier);
	//	}

	//	if (playerController.carCrashing) {
	//		canTakeSlowDamage = false;
	//	} else {
	//		canTakeSlowDamage = true;
	//	}
	//}

	void OnCollisionEnter(Collision collision) {
		if (!isLeader) {
			return;
		}
		if (ignoreCollisions.Contains(collision.gameObject) || collision.transform.tag == "Road") {
			return;
		}
		TakeDamage(collision.relativeVelocity.magnitude * hitDamageModifier);
		ignoreCollisions.Add(collision.gameObject);
		StartCoroutine(RemoveIgnoreCollsion(collision.gameObject));
	}

	void TakeDamage(float damage) {
		if (carController.destroyed || winCondition.criminalWinning) {
			return;
		}
		health -= damage;
		healthBar.UpdateHealthbar(health);
		if (health <= 0 && !carController.destroyed) {
			playerController.DestroyCar(transform);
			health = 0;
			carController.destroyed = true;
		}
	}

	IEnumerator RemoveIgnoreCollsion(GameObject obj) {
		yield return new WaitForSeconds(ignoreCollisionTime);
		ignoreCollisions.Remove(obj);
	}

}
