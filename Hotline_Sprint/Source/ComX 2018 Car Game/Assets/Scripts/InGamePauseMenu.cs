﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGamePauseMenu : MonoBehaviour {

    // <summary>
    //checks if the game is paused so it can 
    //open the options
    // <summary>
    public static bool GameIsPaused = false;

    // <summary>
    //UI objects to be assigned to these
    //gameobjects
    // <summary>
    public GameObject pauseMenuUI;
    public GameObject optionsInGame;
    public GameObject mainOptionsInGame;

    // <summary>
    //If escape key is pressed is checking wether
    //the game is paused and based on it the game will pause
    //or resume
    // <summary>
    void Update () {
		
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
               Resume();
            } else {
               Pause();
            }
        }

	}
    // <summary>
    //If the game resumes then UI Menu will not 
    //be active anymore
    // <summary>
    public void Resume() {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1.0f;
        GameIsPaused = false;
    }

    // <summary>
    ///If the game pauses then UI Menu will 
    //be active and the game time will freeze
    // <summary>
    void Pause() {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0.0f;
        GameIsPaused = true;
    }

    // <summary>
    //If a button is pressed then the ingame options
    //wil be active
    // <summary>
    public void LoadOptions() {
        optionsInGame.SetActive(true);
        mainOptionsInGame.SetActive(false);
    }

    // <summary>
    //If a button is pressed then the ingame menu
    //will be active
    // <summary>
    public void LoadMenu() {
        optionsInGame.SetActive(false);
        mainOptionsInGame.SetActive(true);
    }

    // <summary>
    //If the user presses the exit button then the game
    //will close
    // <summary>
    public void QuitGame() {
        Application.Quit();
    }

    // <summary>
    //If the user presses the menu button then the game
    //will go back to the main menu
    // <summary>
    public void BackToMainMenu(int changethat) {
         SceneManager.LoadScene(changethat);
        Time.timeScale = 1.0f;
    }
}
