﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneStart : MonoBehaviour {

	PlayerController playerController;
	[SerializeField]
	Image coverPanel;
	bool fadePanel = true;
	[SerializeField]
	CameraFollow mainCamera;
	public bool starting = true;
	[SerializeField]
	Transform spotlight;
	Transform leadCar;	

	void Start () {
		StartCoroutine(IntroTimer());
		playerController = gameObject.GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		if(leadCar == null && playerController.player1 != null) {
			leadCar = playerController.player1.transform;
		}
		// Fade out from black screen
		if (fadePanel) {
			Color newCol = coverPanel.color;
			newCol.a -= Time.deltaTime;
			if(newCol.a <= 0) {
				newCol.a = 0;
				coverPanel.enabled = false;
				fadePanel = false;
			}
			coverPanel.color = newCol;
		}
	}

	IEnumerator IntroTimer() {
		yield return new WaitForSeconds(5f);
		mainCamera.enabled = true; // Set camera to move before enabling controls
		yield return new WaitForSeconds(1.5f);
		playerController.checkPolicePositions = true;
		starting = false;
	}
}
