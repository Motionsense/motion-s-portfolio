﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraFollow : MonoBehaviour {

	public float cameraHeight = 70;
	public float cameraDepth = -60;
	const float cameraMoveInterp = 0.02f;
	const float cameraLookInterp = 0.04f;

	public Transform target;
	Vector3 targetPos;

	void Update () {
		if(target == null) {
			return;
		}
		// Set camera position to lerp towards position relative to target car
		targetPos = target.position;
		targetPos.y += cameraHeight;
		targetPos.z += cameraDepth;
		transform.position = Vector3.Lerp(transform.position, targetPos, cameraMoveInterp);

		// Set camera rotation to lerp towards at target
		Vector3 lookDirection = target.position - transform.position;
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookDirection), cameraLookInterp);
	}
}
