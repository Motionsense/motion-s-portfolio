﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates new item objects
/// </summary>
[CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory/Equipment")]
public class Equipment : Item {

    public EquipmentSlot equipSlot;

    /// <summary>
    /// The modifiers to the stats of the armor and weapons
    /// </summary>
    public int armorModifier;
    public int damageModifier;

    /// <summary>
    /// If the item is being equiped its stats wil started to be used
    /// and the item will dissapear from the inventory
    /// </summary>
    public override void Use() {
        base.Use();
        EquipmentManager.instance.Equip(this);
        RemoveFromInventory();
    }
}

/// <summary>
/// Enum of the equipment types
/// </summary>
public enum EquipmentSlot { Head, Chest, Legs, Weapon }
