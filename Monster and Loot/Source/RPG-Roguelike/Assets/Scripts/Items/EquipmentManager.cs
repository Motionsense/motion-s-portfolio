﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Refference/s: https://github.com/Brackeys/RPG-Tutorial/blob/master/RPG%20Project/Assets/Scripts/EquipmentManager.cs

public class EquipmentManager : MonoBehaviour {
   
    #region Singleton
    /// <summary>
    /// 
    /// </summary>
    /// <returns> </returns>
    /// <param name=""></param>
    public static EquipmentManager instance;
    public Equipment[] defaultEquipment;

    void Awake() {
        instance = this;
    }

    #endregion

    /// <summary>
    /// Items that equiped on the player at the moment 
    /// </summary>
    Equipment[] currentEquipment;

    /// <summary>
    /// Callback for when an item is equipped/unequipped
    /// </summary>
    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
    public OnEquipmentChanged onEquipmentChanged;

    /// <summary>
    /// Reference to our inventory
    /// </summary>
    Inventory inventory;

    /// <summary>
    /// Initialize currentEquipment based on number of equipment slots
    /// </summary>
    void Start() {
        inventory = Inventory.instance;

        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currentEquipment = new Equipment[numSlots];
    }

    /// <summary>
    /// Unequips all the items if a keycode is pressed
    /// </summary>
    void Update() {
        if (Input.GetKeyDown(KeyCode.U)) {
            UnequipAll();
        }
    }

    /// <summary>
    /// Finds out what slot the item fits in
    /// </summary>
    public void Equip (Equipment newItem) {
        int slotIndex = (int)newItem.equipSlot;

        Equipment oldItem = Unequip(slotIndex);

        if (onEquipmentChanged != null) {
            onEquipmentChanged.Invoke(newItem, oldItem);
        }   
        currentEquipment[slotIndex] = newItem;
    }

    /// <summary>
    /// Unequips any item that is being equiped by adding it back to the inventory
    /// </summary>
    Equipment Unequip (int slotIndex) {
        Equipment oldItem = null;
        if (currentEquipment[slotIndex] != null) {
            oldItem = currentEquipment[slotIndex];
            inventory.Add(oldItem);

            currentEquipment[slotIndex] = null;
            if (onEquipmentChanged != null) {
                onEquipmentChanged.Invoke(null, oldItem);
            }
        }
        return oldItem;
    }

    /// <summary>
    /// Unequips all items that are equiped on the player
    /// </summary>
    void UnequipAll() {
        for (int i = 0; i < currentEquipment.Length; i++) {
            Unequip(i);
        }
    }
}
