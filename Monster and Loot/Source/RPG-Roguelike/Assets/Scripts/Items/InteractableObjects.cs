﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Reference/s: https://github.com/Brackeys/RPG-Tutorial/blob/master/RPG%20Project/Assets/Scripts/Interactable.cs
//              https://unity3d.com/learn/tutorials/projects/adventure-game-tutorial/interactables

public class InteractableObjects : MonoBehaviour {

    /// <summary>
    /// Reference to the radius of the interactable item
    /// </summary>
    public float radius = 3.0f;

    /// <summary>
    /// Reference to the object's position
    /// </summary>
    public Transform interactionTransform;

    /// <summary>
    /// Reference of the boolean if the player is focuing on the object
    /// </summary>
    bool isFocus = false;

    /// <summary>
    /// Reference to the player's position
    /// </summary>
    Transform player;

    /// <summary>
    /// Reference of the boolean if the item has been interacted
    /// </summary>
    bool hasInteracted = false;

    public virtual void Interact() {
        Debug.Log("Interacting with " + transform.name);
    }

    /// <summary>
    /// If we are currently being focused
    /// and we haven't already interacted with the object then we interact with the selected item
    /// </summary>
    void Update() {
        if (isFocus && !hasInteracted) {
            float distance = Vector3.Distance(player.position, interactionTransform.position);
            if (distance <= radius) {
                Interact();
                hasInteracted = true;
            }
        }
    }

    /// <summary>
    /// Called when the object starts being focused
    /// </summary>
    /// <param name="playerTransform"></param>
    public void OnFocused(Transform playerTransform) {
        isFocus = true;
        player = playerTransform;
        hasInteracted = false;
    }

    /// <summary>
    /// Called when the object is no longer focused
    /// </summary>
    public void OnDefocused() {
        isFocus = false;
        player = null;
        hasInteracted = false;
    }

    /// <summary>
    /// Gizmo shapes that represents the radius of the interactable item in Editor Mode
    /// </summary>
    void OnDrawGizmosSelected()
    {
        if(interactionTransform == null){
            interactionTransform = transform;
        }
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }
}
