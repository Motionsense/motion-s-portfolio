﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : InteractableObjects {

    public Item item;

    /// <summary>
    /// Interacts with the object by aproaching it and picking up
    /// </summary>
    public override void Interact() {
        base.Interact();
        PickUp();
    }
	
    /// <summary>
    /// When the player interacts with item it will pick up
    /// and add it to inventory
    /// </summary>
    void PickUp() {
        Debug.Log( item + " has been picked");
        bool wasItemPickedUp =  Inventory.instance.Add(item);

        if (wasItemPickedUp) {
            Destroy(gameObject);
        }
    }
}
