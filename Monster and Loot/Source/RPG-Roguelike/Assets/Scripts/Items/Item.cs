﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Refference/s: https://unity3d.com/learn/tutorials/topics/user-interface-ui/creating-items-and-testing

[CreateAssetMenu(fileName = "New Item", menuName ="Inventory/Item")]
public class Item : ScriptableObject {
    /// <summary>
    /// Reference to the default name of the newly created item and the icon sprite of it
    /// </summary>>
    new public string name = "New Item";
    public Sprite icon = null;
    public bool isDefaultItem = false;

    /// <summary>
    /// Created to be overrided
    /// </summary>
    public virtual void Use() {
        Debug.Log("Using " + name);
    }

    /// <summary>
    /// When the player will click the delete button 
    /// in the inventory the item will be removed
    /// </summary>
    public void RemoveFromInventory() {
        Inventory.instance.Remove(this);
    }

}
