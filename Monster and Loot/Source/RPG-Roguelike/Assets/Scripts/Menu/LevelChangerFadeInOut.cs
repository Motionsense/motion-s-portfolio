﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelChangerFadeInOut : MonoBehaviour {

    /// <summary>
    /// Reference to the text's animator and UI's gameobject
    /// </summary>
    [SerializeField]
    Animator FadeInOut;
    [SerializeField]
    GameObject LevelChangerCanvas;

    /// <summary>
    /// Variable with no value assigned used as 
    /// an index variable for loading levels
    /// </summary>
    private int levelToLoad;

    ///<summary>
    /// Fading out animation 
    ///</summary>
    /// <param name="levelIndex"></param>
    public void FadeToLevel (int levelIndex) {
        LevelChangerCanvas.gameObject.SetActive(true);

        levelToLoad = levelIndex;
        FadeInOut.SetTrigger("isFadingOut");
    }

    /// <summary>
    /// If the fade animation is completed
    /// the next scene will be loaded
    /// </summary>
    public void OnFadeComplete() {
        SceneManager.LoadScene(levelToLoad);
    }
}
