﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

// Reference - Script from Games Production Module Second Assignment

public class GraphicsQuality : MonoBehaviour {
    
    ///<summary>
    /// This is accesible in the Unity inspector,
    /// but not in code outside this class.
    ///<summary>
    [SerializeField]
    Dropdown setGraphics;
    [SerializeField]
    Dropdown setAntiAliasing;

    ///<summary>
    /// Index values to be used and track 
    /// when the user makes changes in options
    /// and the changes are being saves
    ///<summary>

    ///<summary>
    /// Options for the graphics of the game
    ///<summary>
    enum GraphicsLevel {
        Select_an_option,
        Low,
        Medium,
        High
    }

    ///<summary>
    /// When the game starts, the dropdown list will be populated
    /// with the enum values
    ///<summary>
    void Start() {
        PopulateList();
    }

    ///<summary>
    /// Based on the value in the dropdown
    /// the graphics of the game are being changed
    /// and the settings of the game will be saved
    /// real time
    ///<summary>
    void Update() {
     switch (setGraphics.value) {
            case 1:
                QualitySettings.SetQualityLevel(0);
                Debug.Log("Low Graphics selected!");
                break;
            case 2:
                QualitySettings.SetQualityLevel(1);
                Debug.Log("Medium Graphics selected!");
                break;

            case 3:
                QualitySettings.SetQualityLevel(2);
                Debug.Log("High Graphics selected!");
                break;
        }
        PlayerPrefs.Save();
    }

    ///<summary>
    //An array will access the values of the enum 
    //structure in order for the dropdown to access the enum values
    ///<summary>
    void PopulateList() {
        string[] enumNames = Enum.GetNames(typeof(GraphicsLevel));
        List<string> names = new List<string>(enumNames);
        setGraphics.AddOptions(names);
    }

    ///<summary>
    //The anti aliasing settings will be displayed as values
    //on the dropdown object
    ///<summary>
    public void onAA() {
        QualitySettings.antiAliasing = (int)Mathf.Pow(2.0f,setAntiAliasing.value);

        PlayerPrefs.Save();
    }

}
