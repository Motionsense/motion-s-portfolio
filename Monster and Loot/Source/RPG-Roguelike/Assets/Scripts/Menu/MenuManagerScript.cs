﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Reference - Script from Games Production Module Second Assignment

public class MenuManagerScript : MonoBehaviour {

    ///<summary>
    /// If any button that is assigned to this
    /// function then the application will close
    ///<summary>
    public void QuitGame() {
        Application.Quit();

        Debug.Log("The game has been closed");
    }

    ///<summary>
    /// Changes the game scene by loading a new one
    ///<summary>
    public void changeToScene(int changethat) {
        SceneManager.LoadScene(changethat);
    }

}
