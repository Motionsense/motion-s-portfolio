﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Reference - Script from Games Production Module Second Assignment

public class CameraMenu : MonoBehaviour {

    [SerializeField]
    Animator FadeInOut;

    ///<summary>
    //Camera speed when the scene loads
    //<summary>
    public float cameraSpeed = 0.05f;
    
    ///<summary>
    //Mount that represents a waypoit for the camera
    //to move to its location
    ///<summary>
    public Transform currentMount;
    
    ///<summary>
    //Accessing the camera class
    ///<summary>
    public Camera cam;

    ///<summary>
    //Sets the camera field of view
    ///<summary>
    public int cameraInitialView = 60;

    ///<summary>
    //After the game loads based on the button that is pressed
    //the camera will move to given mounts aka waypoint in the game scene's space
    //by changing its rotation,position on all 3 axis
    ///<summary>
    void LateUpdate () {
        transform.position = Vector3.Lerp(transform.position, currentMount.position, Time.deltaTime * cameraSpeed);

        Vector3 currentAngle = new Vector3(
            Mathf.LerpAngle(transform.rotation.eulerAngles.x, currentMount.transform.rotation.eulerAngles.x, Time.deltaTime * cameraSpeed),
            Mathf.LerpAngle(transform.rotation.eulerAngles.y, currentMount.transform.rotation.eulerAngles.y, Time.deltaTime * cameraSpeed),
            Mathf.LerpAngle(transform.rotation.eulerAngles.z, currentMount.transform.rotation.eulerAngles.z, Time.deltaTime * cameraSpeed));

        transform.eulerAngles = currentAngle;
	}

    ///<summary>
    //Creates new mounts that are considered as viewpoints
    ///<summary>
    public void setMount(Transform newMount){
        currentMount = newMount;
    }
}
