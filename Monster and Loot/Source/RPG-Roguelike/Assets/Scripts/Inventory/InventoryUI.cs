﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Reference/s: https://github.com/Brackeys/RPG-Tutorial/tree/master/RPG%20Project/Assets/Scripts/Inventory
//              https://www.youtube.com/watch?v=w6_fetj9PIw
public class InventoryUI : MonoBehaviour {

    /// <summary>
    /// Reference to the parent object of the items 
    /// Reference to the inventory user interface gameobject
    /// </summary>
    [SerializeField]
    private Transform itemsParent;
    [SerializeField]
    private GameObject inventoryUI;

    /// <summary>
    /// Reference to the boolean if the game is paused
    /// </summary>
    [SerializeField]
    static bool GameIsPaused = false;

    /// <summary>
    /// Reference to the inventory opening sound
    /// </summary>
    [SerializeField]
    AudioSource inventorySoundSource;
    [SerializeField]
    AudioClip inventorySound;

    /// <summary>
    /// The current inventory
    /// </summary>
    private Inventory inventory;

    /// <summary>
    /// List of all the items
    /// </summary>
    private InventorySlot[] slots;

	void Start () {
        inventory = Inventory.instance;
        inventory.onItemChangedCallback += UpdateUI;

        slots = itemsParent.GetComponentsInChildren<InventorySlot>();

        inventorySoundSource.clip = inventorySound;
    }
	
	/// <summary>
    /// When the player pressed the keycode the game will pause 
    /// </summary>
	void Update () {
        if (Input.GetKeyDown(KeyCode.E)) {
            if (GameIsPaused) {
                Resume();
            } else {
                Pause();
            }
        }
	}

    /// <summary>
    /// If an item is being deleted or added the inventory list will be updated
    /// </summary>
    void UpdateUI() {
        for (int i = 0; i < slots.Length; i++) {
            if(i < inventory.items.Count) {
                slots[i].AddItem(inventory.items[i]);
            } else {
                slots[i].ClearSlot();
            }
        }
    }

    ///<summary>
    /// If the game resumes then UI Menu will not 
    /// be active anymore
    ///<summary>
    public void Resume() {
        inventoryUI.SetActive(false);
        Time.timeScale = 1.0f;
        GameIsPaused = false;
    }

    ///<summary>
    ///If the game pauses then UI Menu will 
    //be active and the game time will freeze
    ///<summary>
    void Pause() {
        inventoryUI.SetActive(true);
        inventorySoundSource.Play();
        Time.timeScale = 0.0f;
        GameIsPaused = true;
    }
}
