﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Reference/s: https://github.com/Brackeys/RPG-Tutorial/tree/master/RPG%20Project/Assets/Scripts/Inventory
//              https://www.youtube.com/watch?v=w6_fetj9PIw

public class Inventory : MonoBehaviour {

    #region Singleton
    public static Inventory instance;

    void Awake()
    {
        if(instance != null)
        {
            Debug.LogWarning("more than one instance of inventory found");
            return;
        }
        instance = this;
    }

    #endregion

    /// <summary>
    /// Callback which is triggered when
	/// an item gets added/removed.
    /// </summary>
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    /// <summary>
    /// The maximum value of the inventory space
    /// </summary>
    public int space = 15;

    /// <summary>
    /// Creates a new list of items for the inventory
    /// </summary>
    public List<Item> items = new List<Item>();

    /// <summary>
    /// When the item is picked up, is being added automatically
    /// in the inventory. If the inventory has reached the maximum space
    /// of it then the item would not be able to be picked
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public bool Add (Item item) {
        if (!item.isDefaultItem) {
            if (items.Count >= space) {
                Debug.Log("not enough room");
                return false;
            }
            items.Add(item);

            if (onItemChangedCallback != null) {
                onItemChangedCallback.Invoke();
            }
        }
        return true;
    }

    /// <summary>
    /// Removes any item from the inventory by deleting it
    /// </summary>
    /// <param name="item"></param>
    public void Remove(Item item) {
        items.Remove(item);

        if (onItemChangedCallback != null) {
            onItemChangedCallback.Invoke();
        }
    }

}
