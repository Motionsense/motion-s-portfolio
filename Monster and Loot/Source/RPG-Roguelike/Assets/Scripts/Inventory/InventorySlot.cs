﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Reference/s: https://github.com/Brackeys/RPG-Tutorial/tree/master/RPG%20Project/Assets/Scripts/Inventory
//              https://www.youtube.com/watch?v=w6_fetj9PIw

public class InventorySlot : MonoBehaviour {

    /// <summary>
    /// References to the icon image of the item 
    /// and the remove button for each item
    /// </summary>
    [SerializeField]
    Image icon;
    [SerializeField]
    Button removeButton;

    Item item;

    /// <summary>
    /// When a new item is being added into inventory
    /// the icon sprite is being added toghether with the item
    /// </summary>
    /// <param name="newItem"></param>
    public void AddItem(Item newItem) {
        item = newItem;
        icon.sprite = item.icon;
        icon.enabled = true;
        removeButton.interactable = true;
    }

    /// <summary>
    /// When the remove button is pressed and the item is being deleted
    /// the icon sprite of that item is deleted too
    /// </summary>
    public void ClearSlot() {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
    }

    /// <summary>
    /// Removes item from the inventory
    /// </summary>
    public void OnRemoveButton() {
        Inventory.instance.Remove(item);
    }

    /// <summary>
    /// If the player will equip the item from the inventory
    /// the item will started to be used
    /// </summary>
    public void UseItem() {
        if (item != null) {
            item.Use();
        }
    }
}
