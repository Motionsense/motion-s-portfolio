﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat {

    /// <summary>
    /// Reference to the base value of the player's stats
    /// </summary>
    [SerializeField]
    private int baseValue;

    /// <summary>
    /// List of modifiers that change the baseValue
    /// </summary>
    private List<int> modifiers = new List<int>();

    /// <summary>
    /// Get the final value after applying modifiers
    /// </summary>
    /// <returns></returns>
    public int GetValue() {
        int finalValue = baseValue;
        modifiers.ForEach(x => finalValue += x);
        return finalValue;
    }
    /// <summary>
    /// Adds the modifier of the item stat if the equipment has been equiped
    /// </summary>
    /// <param name="modifier"></param>
    public void AddModifier(int modifier) {
        if(modifier != 0) {
            modifiers.Add(modifier);
        }
    }

    /// <summary>
    /// Removes the modifier of the item stat  if the equipment has been unequiped
    /// </summary>
    /// <param name="modifier"></param>
    public void RemoveModifier(int modifier) {
        if (modifier != 0) {
            modifiers.Remove(modifier);
        }
    }
}
