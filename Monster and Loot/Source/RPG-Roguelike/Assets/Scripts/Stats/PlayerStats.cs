﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Reference/s: https://www.youtube.com/watch?v=e8GmfoaOB4Y
//              https://github.com/Brackeys/RPG-Tutorial/tree/master/RPG%20Project/Assets/Scripts/Stats

public class PlayerStats : MonoBehaviour
{
    /// <summary>
    /// Reference to the player's health bar/slider
    /// </summary>
    [SerializeField]
    Slider healthBar;

    /// <summary>
    /// Reference to the player's animator
    /// </summary>
    [SerializeField]
    Animator playerAnimation;
    
    /// <summary>
    /// Reference to the player's damage text animator
    /// </summary>
    [SerializeField]
    Text damageText;

    /// <summary>
    /// Reference to the game over/player's death music
    /// </summary>
    [SerializeField]
    AudioSource deathMusicSource;
    [SerializeField]
    AudioClip deathMusicSound;

    /// <summary>
    /// Reference to the deathpanel UI gameobject
    /// </summary>
    [SerializeField]
    GameObject deathPanel;

    /// <summary>
    /// Reference to the deatPanelMenu animation
    /// </summary>
    [SerializeField]
    Animator deathPanelAnimation;

    [SerializeField]
    private Text armorText;

    public Stat armor;
    public Stat damage;
    public int health = 100;

    private Animator damageAnim;
    private Equipment armorModif;

    /// <summary>
    /// Accesing the components of animators
    /// Based on the item if it is equiped a modifier of its value
    /// will be added to the player's stats
    /// </summary>
    void Start() {
        damageAnim = damageText.GetComponent<Animator>();
        playerAnimation = GetComponent<Animator>();
        deathPanelAnimation = deathPanel.GetComponent<Animator>();

        deathMusicSource.clip = deathMusicSound;

        EquipmentManager.instance.onEquipmentChanged += OnEquipmentChanged;
    }

    /// <summary>
    /// Updating the health bar slider every frame according
    /// to the player's health
    /// </summary>
    void Update() {
        healthBar.value = health;
    }

    /// <summary>
    /// Calculating damage that the player
    /// would receive from the AI. For every hit the damage value
    /// will be shown on a text UI.
    /// Based on the armor the player has the damage amount the player would receive 
    /// will be reduced by substracting from the dmgAmount value which is 0 the armor value
    /// The dmg amount's value will not got negative
    /// </summary>
    /// <param name="dmgAmount"></param>
    public void CalculatingDamage(int dmgAmount) {
       playerAnimation.Play("TakingDamage");
       damageAnim.Play("Dmg_SHOW");

       dmgAmount -= armor.GetValue();
       dmgAmount = Mathf.Clamp(dmgAmount, 0, int.MaxValue);

       damageText.text = "-" + dmgAmount.ToString();
       health -= dmgAmount;
       
       Debug.Log("dmg " + dmgAmount);
        if(health <= 0) {
            healthBar.value = 0;
            PlayerDead();
        }
    }
    /// <summary>
    /// If the player's health points reaches zero then 
    /// the animations will be played
    /// </summary>
    void PlayerDead() {
        playerAnimation.Play("PlayerDeath");
        deathPanelAnimation.SetBool("isPlayerDead", true);
        deathMusicSource.Play();
    }
    /// <summary>
    /// When the equipment from the inventory is being equiped on the player
    /// based on the item/equipment's stats a modifier will be added to the 
    /// player's stats such as defence and damage
    /// </summary>
    /// <param name="newItem"></param>
    /// <param name="oldItem"></param>
    void OnEquipmentChanged(Equipment newItem, Equipment oldItem) {
        if (newItem != null) {
            armor.AddModifier(newItem.armorModifier);
            damage.AddModifier(newItem.damageModifier);
        }

        if (oldItem != null) {
            armor.RemoveModifier(oldItem.armorModifier);
            damage.RemoveModifier(oldItem.damageModifier);
        }
    }
}
