﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSound : MonoBehaviour {

    /// <summary>
    /// Reference to the footsteps sound
    /// </summary>
    [SerializeField]
    AudioSource footstepSource;
    [SerializeField]
    AudioClip[] footstepsSound;

	// Use this for initialization
	void Start () {
        footstepSource = footstepSource.GetComponent<AudioSource>();
    }
	
     /// <summary>
     /// In order to play the footstep sound, first a random selection
     /// function needs to be used
     /// </summary>
    private void Footstep() {
        AudioClip footstepSound = GetRandomFootstep();
        footstepSource.PlayOneShot(footstepSound);
    }

    /// <summary>
    /// Random selection of the sounds
    /// </summary>
    /// <returns>returns any value between zero and the length of the array</returns>
    private AudioClip GetRandomFootstep() {
        return footstepsSound[Random.Range(0, footstepsSound.Length)];
    }
}
