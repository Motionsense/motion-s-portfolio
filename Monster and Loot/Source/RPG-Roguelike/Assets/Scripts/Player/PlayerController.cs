﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    /// <summary>
    /// Reference for the focus on any interactable object
    /// </summary>
    [SerializeField]
    private InteractableObjects focus;

    /// <summary>
    /// Reference for the player's health bar
    /// </summary>
    [SerializeField]
    private Slider playerHealth;

    /// <summary>
    /// Reference for the player's animation.
    /// </summary>
    [SerializeField]
    private Animator playerAnimation;

    /// <summary>
    /// Reference for the clicking to move on any NavMesh floor
    /// </summary>
    ClickToMove clickToMove;

    /// <summary>
    /// Reference for the boolean's regading the distance between player's position
    /// and raycast hit.
    /// </summary>
    private bool mRunning = false;

    void Start() {
        AudioListener.volume = 0.1f;
        playerAnimation = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
        clickToMove = GetComponent<ClickToMove>();
    }

    /// <summary>
    /// If the pointer is on any gameobject then the player
    /// will not move in the game's world space.
    /// If the left mouse button is pressed then a raycast hit is being created and 
    /// the player will start moving towards the raycast hit's position
    /// </summary>
    void Update() {
        if (EventSystem.current.IsPointerOverGameObject()) {
            return;
        } else {
            if (playerHealth.value <= 0) {
                return;
            } else {
                if (Input.GetKeyDown(KeyCode.Space)) {
                    playerAnimation.Play("PlayerMeleeAttack");
                }

                if (Input.GetMouseButtonDown(0)) {
                    RaycastHit hit;

                    // If the ray hits
                    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)) {
                        clickToMove.MoveToPoint(hit.point);   // Move to where we hit
                        RemoveFocus();
                    }
                }

                if (Input.GetMouseButton(1)) {
                    RaycastHit hit;

                    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)) {
                        InteractableObjects interactableObject = hit.collider.GetComponent<InteractableObjects>();
                        if (interactableObject != null) {
                            SetFocus(interactableObject);
                        }
                    }
                }
                if (clickToMove.playerAgent.remainingDistance <= clickToMove.playerAgent.stoppingDistance) {
                    mRunning = false;
                } else {
                    mRunning = true;
                }
                playerAnimation.SetBool("isRunning", mRunning);
            }
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
        SceneManager.LoadScene(3);
        }  
    }
    /// <summary>
    /// Focusing a gameobject by selecting it. If the newfocus object is not teh same
    /// as the new selected one then the new one becomes the old focus and the player start
    /// following it's position
    /// </summary>
    /// <param name="newFocus"></param>
    void SetFocus(InteractableObjects newFocus) {
        // If our focus has changed
        if (newFocus != focus) {
            // Defocus the old one
            if (focus != null)
                focus.OnDefocused();
                
            // Set our new focus
            focus = newFocus;   
            // Follow the new focus
            clickToMove.FollowTarget(newFocus);   
        }
        newFocus.OnFocused(transform);
    }

    /// <summary>
    /// Removing the focus made on an object
    /// </summary>
    void RemoveFocus() {
        if (focus != null)
            focus.OnDefocused();

        focus = null;
        clickToMove.StopFollowingTarget();
    }
}
