﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomIdleState : StateMachineBehaviour {

    /// <summary>
    /// OnStateMachineEnter is called when entering a statemachine via its Entry Node 
    /// in order to play random animations
    /// </summary>
    /// <param name="animator"></param>
    /// <param name="stateMachinePathHash"></param>
    override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash) {
        animator.SetInteger("IdleAnimID", Random.Range(0, 4));
    }
}
