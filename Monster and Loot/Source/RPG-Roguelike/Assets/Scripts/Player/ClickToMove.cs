﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

/// <summary>
/// Reference - A part of the code from this script that has been used in game is from 
/// Game Scripting Lecture Week 14 - Generation Map tutorial
/// </summary>
public class ClickToMove : MonoBehaviour {

    public NavMeshAgent playerAgent;
    private Transform target;

    // Use this for initialization
    void Start () {
        playerAgent = GetComponent<NavMeshAgent>();
    }

    void Update() {
        // If we have a target
        if (target != null) {
            // Move towards it and look at it
            playerAgent.SetDestination(target.position);
            FaceTarget();
        }
        playerAgent.angularSpeed = 4000;
        playerAgent.speed = 4;
        playerAgent.acceleration = 12;
    }

    /// <summary>
    /// By selecting any point in the 3D space
    /// the player will start moving towards it
    /// </summary>
    /// <param name="point"></param>
    public void MoveToPoint(Vector3 point) {
        playerAgent.SetDestination(point);
    }

    /// <summary>
    /// The player will start following any object and
    /// will stop when is reaching its radius
    /// </summary>
    /// <param name="newTarget"></param>
    public void FollowTarget(InteractableObjects newTarget) {
        playerAgent.stoppingDistance = newTarget.radius * .8f;
        playerAgent.updateRotation = false;
        target = newTarget.interactionTransform;
    }

    /// <summary>
    /// The player will stop following the target
    /// by keeping the distance at the same value;
    /// </summary>
    public void StopFollowingTarget() {
        playerAgent.stoppingDistance = 0f;
        playerAgent.updateRotation = true;
        target = null;
    }

    // Make sure to look at the target
    /// <summary>
    /// In order for the player to move to the desired position 
    /// it needs to change its rotation
    /// </summary>
    void FaceTarget() {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }
}
