﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerDamage : MonoBehaviour {

    /// <summary>
    /// If any objects collides with any object that has one of these
    /// tags then the enemy will receive damage
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "SkeletonWar") {
            other.gameObject.GetComponent<SkelWarAIStats>().AICalculatingDamage(15);
        }

        if (other.gameObject.tag == "SkeletonWiz") {
            other.gameObject.GetComponent<SkelWizAIStats>().AICalculatingDamage(15);
        }

        if (other.gameObject.tag == "SkeletonBoss") {
            other.gameObject.GetComponent<AISkeletonBossStats>().AICalculatingDamage(15);
        }
    }
}
