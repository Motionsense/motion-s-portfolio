﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Teleporter : MonoBehaviour {

    /// <summary>
    /// Reference to the level's name
    /// </summary>
    [SerializeField]
    private string levelToLoad;

    /// <summary>
    /// When the player will collide with the the object's
    /// collider then a new level will be loaded
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other) {
        SceneManager.LoadScene(levelToLoad);
    }
}
