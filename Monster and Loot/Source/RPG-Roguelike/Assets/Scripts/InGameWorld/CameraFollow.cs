﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraFollow : MonoBehaviour {

    /// <summary>
    /// Reference to the player's position
    /// </summary>
    [SerializeField]
	public Transform PlayerTarget;

    /// <summary>
    /// Reference to camera's speed
    /// </summary>
    [SerializeField]
    float smoothSpeed = 0.125f;

    /// <summary>
    /// Reference to the camera's position and rotation
    /// </summary>
    [SerializeField]
    Vector3 offset;

    void Start() {
        PlayerTarget = GameObject.Find("Player").transform;
    }

    /// <summary>
    /// The camera will start to follow the player
    /// by keeping its position the same in relation
    /// to the player's position
    /// </summary>
    void Update() {
        Vector3 desiredPosition = PlayerTarget.position + offset;
        transform.position = Vector3.Slerp(transform.position, desiredPosition, smoothSpeed);
        transform.LookAt(PlayerTarget);
    }

}
