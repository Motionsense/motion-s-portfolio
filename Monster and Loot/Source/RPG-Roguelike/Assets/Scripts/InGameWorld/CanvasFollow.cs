﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFollow : MonoBehaviour {

    /// <summary>
    /// Reference to the object's position
    /// </summary>
    public Transform target;

	/// <summary>
    /// The object will acess a gameobject's position with the MainCamera
    /// </summary>
	void Start () {
        target = GameObject.FindGameObjectWithTag("MainCamera").transform;
	}
	
	/// <summary>
    /// Every frame the camera will follow the player
    /// </summary>
	void Update () {
        transform.LookAt(target.position);
	}
}
