﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine.AI;

// Reference/s: Game Engine Scripting Module - Lecture 17: Random Walk Algoritm added and improved
//              http://pcgbook.com/
//              http://www.nathanmwilliams.com/files/AnInvestigationIntoDungeonGeneration.pdf


public class Room {
	public int x = 0;
	public int y = 0; // here a Y value is being used and not Z is because of the rotaion of the prefabs all prefabs will be rotates -90 degrees where the Y axis becomes Z axis and vice-versa
	public int w = 0;
	public int h = 0;
	public Room connectedTo = null;
	public int branch = 0;
	public string relative_positioning = "x";
	public bool dead_end = false;
	public int room_id = 0;
}

public class SpawnList {
	public int x;
	public int y;
	public bool byWall;
	public bool byCorridor;
	public int asDoor = 0;
	public Room room = null;
	public bool spawnedObject;
}

[System.Serializable]
public class SpawnOption {
	public int minSpawnCount;
	public int maxSpawnCount;
	public bool spawnByWall;
	public GameObject gameObject;
	public int spawnRoom = 0;
}

[System.Serializable]
public class CustomRoom {
	[Tooltip("make sure room id isnt bigger than your room count")]
	public int roomId = 1;
	public GameObject floorPrefab;
	public GameObject wallPrefab;
	public GameObject doorPrefab;
	public GameObject cornerPrefab;
}

public class MapTile {
	public int type = 0; //Default = 0 , Room Floor = 1, Wall = 2, Corridor Floor 3, Room Corners = 4, 5, 6 , 7
	public Room room = null;
}

public class RandomGeneration : MonoBehaviour {
	public GameObject playerPrefab;
	public GameObject exitPrefab;
	public List<SpawnList> spawnedObjectLocations = new List<SpawnList>();
	public GameObject floorPrefab;
	public GameObject wallPrefab;
	public GameObject corridorFloorPrefab;

	public GameObject cornerPrefab;
	public bool cornerRotation = false;
    public int maximumRoomCount = 10;
    public int roomMargin = 3;
	public int minRoomSize = 5;
	public int maxRoomSize = 10;
	public float tileScaling = 1f;
	public List<SpawnOption> spawnOptions = new List<SpawnOption>();
	public List<CustomRoom> customRooms = new List<CustomRoom> ();
    public List<SpawnOption> armorOptions = new List<SpawnOption>();
    public List<SpawnOption> enemiesOptions = new List<SpawnOption>();

    private NavMeshSurface NavMesh;


    class Dungeon {
		public static int map_size;
		public static int map_size_x;
		public static int map_size_y;


		public static MapTile[,] map;

		public static List<Room> rooms = new List<Room>();
		
		public static Room goalRoom;
		public static Room startRoom;
		
		public int min_size;
		public int max_size;
		
		public int maximumRoomCount;
		public int roomMargin;
		public int roomMarginTemp;

		//tile types for ease
		public static List<int> roomsandfloors = new List<int> { 1, 3 };
		public static List<int> corners = new List<int> {4,5,6,7};
		public static List<int> walls = new List<int> {8,9,10,11};
		private static List<string> directions = new List<string> {"x","y","-y","-x"};
		
		public void Generate() {
			int room_count = this.maximumRoomCount;
			int min_size = this.min_size;
			int max_size = this.max_size;
            if(roomMargin < 2) {
                map_size = room_count * max_size * 2;
            } else {
                map_size = room_count * (max_size + (roomMargin * 2));
            }
            map = new MapTile[map_size, map_size];

            ///<summary>
            ///By calculating the map size based on the room margin set by the player a new MapTile with the X and Y coordinates is created
            ///</summary>
			for (int x = 0; x < map_size; x++) {
				for (int y = 0; y < map_size; y++) {
					map [x, y] = new MapTile ();
					map [x, y].type = 0;
				}
			}
			rooms = new List<Room> ();
			
			int collision_count = 0;
			string direction = "set";
            string oldDirection = "set";
			Room lastRoom;

			for (int i = 0; i < room_count; i++) {
				Room room = new Room (); 
				if (rooms.Count == 0) {
					//first room
					room.x = (int)Mathf.Floor (map_size / 2f);
					room.y = (int)Mathf.Floor (map_size / 2f);
					room.w = Random.Range (min_size, max_size);
					room.h = Random.Range (min_size, max_size);
					room.branch = 0;
					lastRoom = room;
				} else {
					int branch = 0;
					if (collision_count == 0) {
                        branch = Random.Range(5, 20);
					}
					room.branch = branch;

					lastRoom = rooms [rooms.Count - 1];
					int lri = 1;

					while (lastRoom.dead_end) {
						lastRoom = rooms [rooms.Count - lri++];
					}


                    if (direction == "set") {
                        string newRandomDirection = directions[Random.Range(0, directions.Count)];
                        direction = newRandomDirection;
                        while (direction == oldDirection)
                        {
                            newRandomDirection = directions[Random.Range(0, directions.Count)];
                            direction = newRandomDirection;
                        }
                    }
                    this.roomMarginTemp = Random.Range(0, this.roomMargin - 1);

                    if (direction == "y") {
						room.x = lastRoom.x + lastRoom.w + Random.Range (3, 5) + this.roomMarginTemp;
						room.y = lastRoom.y;
					} else if (direction == "-y") {
						room.x = lastRoom.x - lastRoom.w - Random.Range (3, 5) - this.roomMarginTemp;
						room.y = lastRoom.y;
					} else if (direction == "x") {
						room.y = lastRoom.y + lastRoom.h + Random.Range (3, 5) + this.roomMarginTemp;
						room.x = lastRoom.x;
					} else if (direction == "-x") {
						room.y = lastRoom.y - lastRoom.h - Random.Range (3, 5) - this.roomMarginTemp;
						room.x = lastRoom.x;
					}

					room.w = Random.Range (min_size, max_size);
					room.h = Random.Range (min_size, max_size);
					room.connectedTo = lastRoom;
				}

				bool doesCollide = this.DoesCollide (room, 0);				
				if (doesCollide) {
					i--;
					collision_count += 1;
					if (collision_count > 3) {
						lastRoom.branch = 1;
						lastRoom.dead_end = true;
						collision_count = 0;
					} else {
                        oldDirection = direction;
						direction = "set";
					}
				} else {
					room.room_id = i;
					rooms.Add (room);
                    oldDirection = direction;
					direction = "set";
				}
			}

            /// <summary>
            /// Based on the value of how many rooms need to be spawned
            /// the floor for each room will be spawned based on the height(Y) and width(X) values of each room
            /// </summary>
            for (int i = 0; i < rooms.Count; i++) {
				Room room = rooms [i];
				for (int x = room.x; x < room.x + room.w; x++) {
					for (int y = room.y; y < room.y + room.h; y++) {
						map [x, y].type = 1;
						map [x, y].room = room;
					}
				}
			}

            ///<summary>
            ///The corridor is being created by going through a loop creating corridor that would link two rooms at the same time
            ///First two points would be assigned as a local variables which are placed in two rooms that are adjacent or close to each other
            ///by accessing the room's X and Y which acts as Z values
            ///If the difference between X values  of the point A and B is greater than the difference between point the Y values of the 
            ///</summary>
            for (int i = 1; i < rooms.Count; i++) {
				Room roomA = rooms [i];
				Room roomB = rooms [i].connectedTo;

				if (roomB != null) {
					Room pointA = new Room ();
                    Room pointB = new Room ();

					pointA.x = roomA.x + (int)Mathf.Floor (roomA.w / 2);
					pointB.x = roomB.x + (int)Mathf.Floor (roomB.w / 2);

					pointA.y = roomA.y + (int)Mathf.Floor (roomA.h / 2);
					pointB.y = roomB.y + (int)Mathf.Floor (roomB.h / 2);

					if (Mathf.Abs (pointA.x - pointB.x) > Mathf.Abs (pointA.y - pointB.y)) {
						if (roomA.h > roomB.h) {
							pointA.y = pointB.y;

						} else {
							pointB.y = pointA.y;

						}
					} else {
						if (roomA.w > roomB.w) {
							pointA.x = pointB.x;
						} else {
							pointB.x = pointA.x;
						}					
					}

					while ((pointB.x != pointA.x) || (pointB.y != pointA.y)) {
						if (pointB.x != pointA.x) {
							if (pointB.x > pointA.x) {
								pointB.x--;
							} else {					
								pointB.x++;
							}
						} else if (pointB.y != pointA.y) {
							if (pointB.y > pointA.y) {
								pointB.y--;
							} else {
								pointB.y++;
							}
						}

						if (map [pointB.x, pointB.y].room == null) {
							map [pointB.x, pointB.y].type = 3;
						}
					} 
				}
			}

			//x crop; because map created in the middle of array and we are pushing it to bottom left edge.
			int row = 1;
			int min_crop_x = map_size;
			for (int x = 0; x < map_size -1; x++) {
				bool x_empty = true;
				for (int y = 0; y < map_size -1; y++) {
					if (map[x,y].type != 0){
						x_empty = false;
						if(x < min_crop_x){
							min_crop_x = x;
						}
						break;
					}
				}
				if (!x_empty){
					for (int y=0;y < map_size -1;y++){
						map[row,y] = map[x,y];
						map[x,y] = new MapTile();
					}
					row += 1;
				}
			}
			
			//y crop
			row = 1;
			int min_crop_y = map_size;
			for (int y = 0; y < map_size -1; y++) {
				bool y_empty = true;
				for (int x = 0; x < map_size -1; x++) {
					if (map[x,y].type != 0){
						y_empty = false;
						if(y < min_crop_y){
							min_crop_y = y;
						}
						break;
					}
				}
				if (!y_empty){
					for (int x=0;x < map_size -1;x++){
						map[x,row] = map[x,y];
						map[x,y] = new MapTile();
					}
					row += 1;
				}
			}
			foreach (Room room in rooms) {
				room.x -= min_crop_x;
				room.y -= min_crop_y;
			}

			//test map size
			int final_map_size_y = 0;
			for (int y = 0; y < map_size -1; y++) {
				for (int x = 0; x < map_size -1; x++) {
					if (map[x,y].type != 0){
						final_map_size_y += 1;
						break;
					}
				}
			}

			int final_map_size_x = 0;
			for (int x = 0; x < map_size -1; x++) {
				for (int y = 0; y < map_size -1; y++) {
					if (map[x,y].type != 0){
						final_map_size_x += 1;
						break;
					}
				}
			}

			final_map_size_x += 4;
			final_map_size_y += 4;

			MapTile[,] new_map = new MapTile[final_map_size_x,final_map_size_y];
			for (int x= 0; x < final_map_size_x; x++) {
				for(int y=0;y < final_map_size_y; y++){
					new_map[x,y] = map[x,y];
				}
			}
			map = new_map;
			map_size_x = final_map_size_x;
			map_size_y = final_map_size_y;


            for (int x = 0; x < map_size_x -1; x++) {
				for (int y = 0; y < map_size_y -1; y++) {
					if (map [x, y].type == 0) {
						if (map [x + 1, y].type == 1 || map [x + 1, y].type == 3) { //west
							map [x, y].type = 11;
							map [x, y].room = map [x + 1, y].room;
						}
						if (x > 0) {
							if (map [x - 1, y].type == 1 || map [x - 1, y].type == 3) { //east
								map [x, y].type = 9;
								map [x, y].room = map [x - 1, y].room;

							}
						}
						
						if (map [x, y + 1].type == 1 || map [x, y + 1].type == 3) { //south
							map [x, y].type = 10;
							map [x, y].room = map [x, y + 1].room;

						}
						
						if (y > 0) {
							if (map [x, y - 1].type == 1 || map [x, y - 1].type == 3) { //north
								map [x, y].type = 8;
								map [x, y].room = map [x, y - 1].room;

							}
						}
					}
				}
			}

            /// <summary>
            /// Creates corners for each room
            /// If the walls and the floor are beings spawned on each coordinate N-S-E-W then the corners of the rooms will be adjacent to the walls
            /// </summary>
            for (int x = 0; x < map_size_x -1; x++) {
				for (int y = 0; y < map_size_y -1; y++) {
					if (walls.Contains (map [x, y + 1].type) && walls.Contains (map [x + 1, y].type) && roomsandfloors.Contains (map [x + 1, y + 1].type)) { //north
						map [x, y].type = 4;
						map [x, y].room = map [x + 1, y + 1].room;
					}
					if (y > 0) {
						if (walls.Contains (map [x + 1, y].type) && walls.Contains (map [x, y - 1].type) && roomsandfloors.Contains (map [x + 1, y - 1].type)) { //north
							map [x, y].type = 5;
							map [x, y].room = map [x + 1, y - 1].room;
						}
					}
					if (x > 0) {
						if (walls.Contains (map [x - 1, y].type) && walls.Contains (map [x, y + 1].type) && roomsandfloors.Contains (map [x - 1, y + 1].type)) { //north
							map [x, y].type = 7;
							map [x, y].room = map [x - 1, y + 1].room;
						}
					}
					if (x > 0 && y > 0) {
						if (walls.Contains (map [x - 1, y].type) && walls.Contains (map [x, y - 1].type) && roomsandfloors.Contains (map [x - 1, y - 1].type)) { //north
							map [x, y].type = 6;
							map [x, y].room = map [x - 1, y - 1].room;
						}
					}
					if (map [x, y].type == 3) { 
						if (map [x + 1, y].type == 1) {
							map [x, y + 1].type = 11;
							map [x, y - 1].type = 11;
						} else if (Dungeon.map [x - 1, y].type == 1) {
							map [x, y + 1].type = 9;
							map [x, y - 1].type = 9;
						}
					}
				}
			}

            /// <summary>
            /// Spawns the end room
            /// </summary>
            goalRoom = rooms[rooms.Count -1 ];
			if (goalRoom != null) {
				goalRoom.x = goalRoom.x + (goalRoom.w / 2);
				goalRoom.y = goalRoom.y + (goalRoom.h / 2);
			}
			
            /// <summary>
            /// Assigns the spawn room to the index room 0 and 
            /// assigns the positiono of it
            /// </summary>
			startRoom = rooms[0];
			startRoom.x = startRoom.x + (startRoom.w / 2);
			startRoom.y = startRoom.y + (startRoom.h / 2);
		}

        /// <summary>
        /// Checks if the rooms will collide and if they do
        /// a distance will be taken to the point any room will
        /// not collide with other rooms anynore
        /// </summary>
        /// <param name="room"></param>
        /// <param name="ignore"></param>
        /// <returns></returns>
		private bool DoesCollide (Room room, int ignore) {
			int random_blankliness = 0;

			for (int i = 0; i < rooms.Count; i++) {
				Room check = rooms[i];
				if (!((room.x + room.w + random_blankliness < check.x) ||
                     (room.x > check.x + check.w + random_blankliness) || 
                     (room.y + room.h + random_blankliness < check.y) || 
                     (room.y > check.y + check.h + random_blankliness)))
                    return true;
			}
			return false;
		}
 
        /// <summary>
        /// Creates and calculates the distance between two points from two different room in order for the corridor to be created
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
		private float lineDistance( Room point1, Room point2 ) {
			int xs = 0;
            int ys = 0;
			
			xs = point2.x - point1.x;
			xs = xs * xs;
			
			ys = point2.y - point1.y;
			ys = ys * ys;
			
			return Mathf.Sqrt( xs + ys );
		}
	}

    public void Generate() {
        Dungeon dungeon = new Dungeon();

        ///<summary>
        /// Dungeon's minimum, maximum room values will be assigned to a local variable in order for the Dungeon class values to be used
        ///</summary>
        dungeon.min_size = minRoomSize;
        dungeon.max_size = maxRoomSize;
        dungeon.maximumRoomCount = maximumRoomCount;
        dungeon.roomMargin = roomMargin;

        dungeon.Generate();

        ///<summary>
        ///By accessing the values of the dungeon map such as X and Z axis values a custom room would be spawned
        ///</summary>
        for (int y = 0; y < Dungeon.map_size_y; y++) {
            for (int x = 0; x < Dungeon.map_size_x; x++) {
                int tile = Dungeon.map[x, y].type;

                ///<summary>
                ///The wall and the floor tiles would start spawinin by accessing the position values multiplaying with the tile scaling of the dungeon
                ///</summary>
                GameObject created_tile;
                Vector3 tile_location;
                Vector3 wall_location;
                tile_location = new Vector3(x * tileScaling, 0, y * tileScaling);
                wall_location = new Vector3(x * tileScaling, 3.0f, y * tileScaling);

                ///<summary>
                ///After the floors have been created and assigned for each room in the Dungeon class based on the size of the room the floors will start to spawn
                ///</summary>
                created_tile = null;
                if (tile == 1) {
                    GameObject floorPrefabToUse = floorPrefab;
                    Room room = Dungeon.map[x, y].room;
                    if (room != null) {
                        foreach (CustomRoom customroom in customRooms) {
                            if (customroom.roomId == room.room_id) {
                                floorPrefabToUse = customroom.floorPrefab;
                                break;
                            }
                        }
                    }
                    created_tile = GameObject.Instantiate(floorPrefabToUse, tile_location, Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
                }

                ///<summary>
                ///By accessing the values of the dungeon map such as X and Z axis values a custom room would be spawned
                ///</summary>
                if (Dungeon.walls.Contains(tile)) {
                    GameObject wallPrefabToUse = wallPrefab;
                    Room room = Dungeon.map[x, y].room;
                    if (room != null) {
                        foreach (CustomRoom customroom in customRooms) {
                            if (customroom.roomId == room.room_id) {
                                wallPrefabToUse = customroom.wallPrefab;
                                break;
                            }
                        }
                    }
                    created_tile = GameObject.Instantiate(wallPrefabToUse, wall_location, Quaternion.identity) as GameObject;
                    created_tile.transform.Rotate(Vector3.up * (-90 * (tile - 2)));
                }

                ///<summary>
                ///After the corridors have been created starts to instantiates the corridorfloor prefab objects
                ///</summary>
                if (tile == 3) {
                    created_tile = GameObject.Instantiate(corridorFloorPrefab, tile_location, Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
                }

                ///<summary>
                ///By accessing the values of the dungeon map such as X and Z axis values the corners of each room will be spawned accoring to the room's position
                ///</summary>
                if (Dungeon.corners.Contains(tile)) {
                    GameObject cornerPrefabToUse = cornerPrefab;
                    Room room = Dungeon.map[x, y].room;
                    if (room != null) {
                        foreach (CustomRoom customroom in customRooms) {
                            if (customroom.roomId == room.room_id) {
                                cornerPrefabToUse = customroom.cornerPrefab;
                                break;
                            }
                        }
                    }

                    if (cornerPrefab) {
                        created_tile = GameObject.Instantiate(cornerPrefabToUse, wall_location, Quaternion.identity) as GameObject;
                        if (cornerRotation) {
                            created_tile.transform.Rotate(Vector3.up * (-90 * (tile - 4)));
                        }
                    }
                    else {
                        created_tile = GameObject.Instantiate(wallPrefab, tile_location, Quaternion.identity) as GameObject;
                    }
                }

                if (created_tile) {
                    created_tile.transform.parent = transform;
                }
            }
        }

        ///<summary>
        ///When the game starts the player object and the boss/teleporter object's positions will be changed based on the room's coordinates
        ///</summary>
        playerPrefab.transform.position =  new Vector3(Dungeon.startRoom.x * tileScaling, 0, Dungeon.startRoom.y * tileScaling);
        exitPrefab.transform.position = new Vector3(Dungeon.goalRoom.x * tileScaling, 0, Dungeon.goalRoom.y * tileScaling);
   	
		//Spawn Objects;
		List<SpawnList> spawnedObjectLocations = new List<SpawnList> ();

		//OTHERS
		for (int x = 0; x < Dungeon.map_size_x; x++) {
			for (int y = 0; y < Dungeon.map_size_y; y++) {
				if (Dungeon.map [x, y].type == 1 &&
				    	((Dungeon.startRoom != Dungeon.map [x, y].room && Dungeon.goalRoom != Dungeon.map [x, y].room) || maximumRoomCount <= 3)) {
					SpawnList location = new SpawnList ();
					location.x = x;
					location.y = y;
					if (Dungeon.walls.Contains(Dungeon.map [x + 1, y].type) || Dungeon.walls.Contains(Dungeon.map [x - 1, y].type) || Dungeon.walls.Contains(Dungeon.map [x, y + 1].type) || Dungeon.walls.Contains(Dungeon.map [x, y - 1].type)) {
						location.byWall = true;
					}
					if (Dungeon.map [x + 1, y].type == 3 || Dungeon.map [x - 1, y].type == 3 || Dungeon.map [x, y + 1].type == 3 || Dungeon.map [x, y - 1].type == 3) {
						location.byCorridor = true;
					}
					if (Dungeon.map [x + 1, y + 1].type == 3 || Dungeon.map [x - 1, y - 1].type == 3 || Dungeon.map [x - 1, y + 1].type == 3 || Dungeon.map [x + 1, y - 1].type == 3) {
						location.byCorridor = true;
					}
					location.room = Dungeon.map[x,y].room;
					spawnedObjectLocations.Add (location);
				}
				else if (Dungeon.map [x, y].type == 3) {
					SpawnList location = new SpawnList ();
					location.x = x;
					location.y = y;	

					if (Dungeon.map [x + 1, y].type == 1 ) {
						location.byCorridor = true;
						location.asDoor = 4;
						location.room = Dungeon.map[x + 1,y].room;

						spawnedObjectLocations.Add (location);
					}
					else if(Dungeon.map [x - 1, y].type == 1) {
						location.byCorridor = true;
						location.asDoor = 2;
						location.room = Dungeon.map[x - 1,y].room;

						spawnedObjectLocations.Add (location);	
					}
					else if (Dungeon.map [x, y + 1].type == 1 ) {
						location.byCorridor = true;
						location.asDoor = 3;
						location.room = Dungeon.map[x,y + 1].room;

						spawnedObjectLocations.Add (location);
					}
					else if (Dungeon.map [x, y - 1].type == 1) {
						location.byCorridor = true;
						location.asDoor = 1;
						location.room = Dungeon.map[x,y - 1].room;

						spawnedObjectLocations.Add (location);					
					}
				}
			}
		}
		
		for (int i = 0; i < spawnedObjectLocations.Count; i++) {
			SpawnList temp = spawnedObjectLocations [i];
			int randomIndex = Random.Range (i, spawnedObjectLocations.Count);
			spawnedObjectLocations [i] = spawnedObjectLocations [randomIndex];
			spawnedObjectLocations [randomIndex] = temp;
		}
		
		int objectCountToSpawn = 0;

        /// <summary>
        /// Spawning objects across the dungeon
        /// First the an integer objecttospawn will be assigned to the minimum and maximum value 
        /// While the objectstospawn value is bigger than 0 then a loop will start by spawning the objects across the room and checking 
        /// if the player wants to spawn the object in the corridor or adjacent the walls of the specific room
        /// After checking the requirements the objects will be placed randomly across the room by using the room's size values such as X and Z
        /// </summary>
        foreach (SpawnOption objectToSpawn in spawnOptions) {
			objectCountToSpawn = Random.Range(objectToSpawn.minSpawnCount,objectToSpawn.maxSpawnCount);
			while (objectCountToSpawn > 0) {
                bool created = false;
                Debug.Log(objectCountToSpawn);

                for (int i = 0;i < spawnedObjectLocations.Count;i++) {
					bool createHere= false;
				
					if (!spawnedObjectLocations[i].spawnedObject && !spawnedObjectLocations[i].byCorridor){
						if(objectToSpawn.spawnRoom > maximumRoomCount){
							objectToSpawn.spawnRoom = 0;
						}
						if(objectToSpawn.spawnRoom == 0){
							if (objectToSpawn.spawnByWall){
								if (spawnedObjectLocations[i].byWall){
									createHere = true;
								}
							} else {
								createHere = true;
							}
						} else {
							if (spawnedObjectLocations[i].room.room_id == objectToSpawn.spawnRoom){
								if (objectToSpawn.spawnByWall){
									if (spawnedObjectLocations[i].byWall){
										createHere = true;
									}
								} else {
									createHere = true;
								}
							}
						}
					}

					if (createHere) {
						SpawnList spawnLocation = spawnedObjectLocations[i];
						GameObject newObject;
                        newObject = GameObject.Instantiate(objectToSpawn.gameObject,new Vector3(spawnLocation.x * tileScaling , 0.3f,spawnLocation.y * tileScaling),Quaternion.Euler(-90.0f,0.0f,0.0f)) as GameObject;

						newObject.transform.parent = transform;
						spawnedObjectLocations[i].spawnedObject = newObject; 
						objectCountToSpawn--;
                        created = true;
						break;
					}
				}
                if (!created) {
                    objectCountToSpawn--;
                }
            }
		}

        /// <summary>
        /// Spawning armor objects across the dungeon
        /// First the an integer objecttospawn will be assigned to the minimum and maximum value 
        /// While the objectstospawn value is bigger than 0 then a loop will start by spawning the objects across the room and checking 
        /// if the player wants to spawn the object in the corridor or adjacent the walls of the specific room
        /// After checking the requirements the objects will be placed randomly across the room by using the room's size values such as X and Z
        /// </summary>
        foreach (SpawnOption armorToSpawn in armorOptions) {
            objectCountToSpawn = Random.Range(armorToSpawn.minSpawnCount, armorToSpawn.maxSpawnCount);
            while (objectCountToSpawn > 0) {
                bool created = false;
                Debug.Log(objectCountToSpawn);

                for (int i = 0; i < spawnedObjectLocations.Count; i++) {
                    bool createHere = false;

                    if (!spawnedObjectLocations[i].spawnedObject && !spawnedObjectLocations[i].byCorridor) {
                        if (armorToSpawn.spawnRoom > maximumRoomCount) {
                            armorToSpawn.spawnRoom = 0;
                        }
                        if (armorToSpawn.spawnRoom == 0) {
                            if (armorToSpawn.spawnByWall) {
                                if (spawnedObjectLocations[i].byWall) {
                                    createHere = true;
                                }
                            } else {
                                createHere = true;
                            }
                        } else {
                            if (spawnedObjectLocations[i].room.room_id == armorToSpawn.spawnRoom) {
                                if (armorToSpawn.spawnByWall) {
                                    if (spawnedObjectLocations[i].byWall) {
                                        createHere = true;
                                    }
                                } else{
                                    createHere = true;
                                }
                            }
                        }
                    }

                    if (createHere) {
                        SpawnList spawnLocation = spawnedObjectLocations[i];
                        GameObject newObject;
                        newObject = GameObject.Instantiate(armorToSpawn.gameObject, new Vector3(spawnLocation.x * tileScaling, 0, spawnLocation.y * tileScaling), Quaternion.identity) as GameObject;

                        newObject.transform.parent = transform;
                        spawnedObjectLocations[i].spawnedObject = newObject;
                        objectCountToSpawn--;
                        created = true;
                        break;
                    }
                }
                if (!created) {
                    objectCountToSpawn--;
                }
            }
        }

        /// <summary>
        /// Spawning enemies across the dungeon
        /// First the an integer objecttospawn will be assigned to the minimum and maximum value 
        /// While the objectstospawn value is bigger than 0 then a loop will start by spawning the objects across the room and checking 
        /// if the player wants to spawn the object in the corridor or adjacent the walls of the specific room
        /// After checking the requirements the objects will be placed randomly across the room by using the room's size values such as X and Z
        /// </summary>
        foreach (SpawnOption enemiesToSpawn in enemiesOptions) {
            objectCountToSpawn = Random.Range(enemiesToSpawn.minSpawnCount, enemiesToSpawn.maxSpawnCount);
            while (objectCountToSpawn > 0) {
                bool created = false;
                Debug.Log(objectCountToSpawn);

                for (int i = 0; i < spawnedObjectLocations.Count; i++) {
                    bool createHere = false;

                    if (!spawnedObjectLocations[i].spawnedObject && !spawnedObjectLocations[i].byCorridor) {
                        if (enemiesToSpawn.spawnRoom > maximumRoomCount) {
                            enemiesToSpawn.spawnRoom = 0;
                        }
                        if (enemiesToSpawn.spawnRoom == 0) {
                            if (enemiesToSpawn.spawnByWall) {
                                if (spawnedObjectLocations[i].byWall) {
                                    createHere = true;
                                }
                            } else {
                                createHere = true;
                            }
                        } else {
                            if (spawnedObjectLocations[i].room.room_id == enemiesToSpawn.spawnRoom) {
                                if (enemiesToSpawn.spawnByWall) {
                                    if (spawnedObjectLocations[i].byWall) {
                                        createHere = true;
                                    }
                                } else {
                                    createHere = true;
                                }
                            }
                        }
                    }
                    if (createHere) {
                        SpawnList spawnLocation = spawnedObjectLocations[i];
                        GameObject newObject;
                        newObject = GameObject.Instantiate(enemiesToSpawn.gameObject, new Vector3(spawnLocation.x * tileScaling, 0, spawnLocation.y * tileScaling), Quaternion.identity) as GameObject;

                        newObject.transform.parent = transform;
                        spawnedObjectLocations[i].spawnedObject = newObject;
                        objectCountToSpawn--;
                        created = true;
                        break;
                    }
                }
                if (!created) {
                    objectCountToSpawn--;
                }
            }
        }
    }

    public void ClearOldDungeon(bool immediate = false) {
        int childs = transform.childCount;
        for (var i = childs - 1; i >= 0; i--) {
            if (immediate) {
                DestroyImmediate(transform.GetChild(i).gameObject);
            } else {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }

    // Use this for initialization
    void Start () {
            ClearOldDungeon();
            Generate();
            GenerateNavMesh();
	}

    /// <summary>
    /// Generates and bakes a navmessurface
    /// </summary>
    void GenerateNavMesh() {
        NavMesh = gameObject.AddComponent<NavMeshSurface>();
        NavMesh.BuildNavMesh();
        Debug.Log("NavMesh has been baked");
    }
}
