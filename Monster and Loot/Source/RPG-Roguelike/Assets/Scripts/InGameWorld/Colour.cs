﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Colour : MonoBehaviour
{
    ///<summary>
    //Color class that will access the color 
    //of any 3D object in the scene
    ///<summary>
    [SerializeField]
    Color ObjectColor;
    private Color currentColor;

    ///<summary>
    //When the player selects a color, a material
    //with that color will be automatically created
    ///<summary>
    private Material materialColored;

    ///<summary>
    //If the color of the current object is not the one
    //the user has selected then assigns the new color to the
    //3D object
    ///<summary>
    void Update() {
        if (ObjectColor != currentColor) {
            materialColored = new Material(Shader.Find("Diffuse"));
            materialColored.color = currentColor = ObjectColor;

            this.GetComponent<Renderer>().material = materialColored;
        }
    }
}