﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AISkeletonBossStats : MonoBehaviour {

    /// <summary>
    /// Reference to the AI's animator with animations
    /// and the damage text UI
    /// </summary>
    [SerializeField]
    Animator skeletonWarAnim;
    [SerializeField]
    Text damageText;

    /// <summary>
    /// AI's health points
    /// </summary>
    public int healthPoints = 30;

    /// <summary>
    /// AI's damage text animation which shows the damage taken
    /// </summary>
    private Animator damageAnim;

    /// <summary>
    /// Reference to the player's win music
    /// </summary>
    [SerializeField]
    AudioSource winMusicSource;
    [SerializeField]
    AudioClip winMusicSound;

    /// <summary>
    /// Reference to the winpanel UI gameobject
    /// </summary>
    [SerializeField]
    GameObject winPanel;

    /// <summary>
    /// Reference to the winPanelMenu animation
    /// </summary>
    [SerializeField]
    Animator winPanelAnimation;

    void Start() {
        damageAnim = damageText.GetComponent<Animator>();
        skeletonWarAnim = GetComponent<Animator>();
        winPanelAnimation = winPanel.GetComponent<Animator>();

        winMusicSource.clip = winMusicSound;
    }
    /// <summary>
    /// Calculating the damage the AI will receive
    /// by substracting for every collision between the 
    /// player's and AI's damage amount from AI's health points
    /// </summary>
    public void AICalculatingDamage(int amount) {
        skeletonWarAnim.Play("Damage");
        damageAnim.Play("Dmg_SHOW");
        damageText.text = "-" + amount.ToString();
        healthPoints -= amount;
        Debug.Log("ai has taken " + amount);

        if (healthPoints <= 0) {
            BossDead();
            GameWon();
        }
    }

    /// <summary>
    /// If AI's health points reach zero, death animation
    /// will be played and from the point AI's health points
    /// have reached zero, after ten seconds the gameobject AI 
    /// will be deleted
    /// </summary>
    void BossDead() {
        skeletonWarAnim.Play("Take 001");
        Destroy(this.gameObject, 10f);
    }

    /// <summary>
    /// If the boss dies then the animation 
    /// of the boss dying is being used and the win music starts to play
    /// </summary>
    void GameWon() {
        winPanelAnimation.SetBool("isBossDead", true);
        winMusicSource.Play();
    }
}
