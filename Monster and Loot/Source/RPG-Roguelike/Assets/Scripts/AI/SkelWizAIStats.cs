﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkelWizAIStats : MonoBehaviour {

    /// <summary>
    /// Reference to the AI's animator with animations
    /// and the damage text UI
    /// </summary>
    [SerializeField]
    Animator skeletenWizAnim;
    [SerializeField]
    Text damageText;

    /// <summary>
    /// AI's health points
    /// </summary>
    public int healthPoints = 30;

    /// <summary>
    /// AI's damage text animation which shows the damage taken
    /// </summary>
    private Animator damageAnim;

    void Start() {
        damageAnim = damageText.GetComponent<Animator>();
        skeletenWizAnim = GetComponent<Animator>();
    }

    /// <summary>
    /// Calculating the damage the AI will receive
    /// by substracting for every collision between the 
    /// player's and AI's damage amount from AI's health points
    /// </summary>
    public void AICalculatingDamage(int amount) {
        skeletenWizAnim.Play("damage_001");
        damageAnim.Play("Dmg_SHOW");
        damageText.text = "-" + amount.ToString();
        healthPoints -= amount;
        Debug.Log("ai has taken " + amount);

        if (healthPoints <= 0) {
            AIDead();
        }
    }
    /// <summary>
    /// If AI's health points reach zero, death animation
    /// will be played and from the point AI's health points
    /// have reached zero, after ten seconds the gameobject AI 
    /// will be deleted
    /// </summary>
    void AIDead() {
        skeletenWizAnim.Play("dead");
        Destroy(this.gameObject, 10f);
    }
}
