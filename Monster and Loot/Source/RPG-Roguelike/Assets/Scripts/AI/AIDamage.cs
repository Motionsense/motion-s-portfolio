﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDamage : MonoBehaviour {
    /// <summary>
    /// The minimum and maximum damage values
    /// the AI would be able to deal
    /// </summary>
    public int minDamage = 1;
    public int maxDamage = 2;
    /// <summary>
    /// If the AI's weapon will collide with the collider with
    /// tag "Player" then by deal a damage between two random values
    /// <paramref name="other"/>
    /// </summary>
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            other.gameObject.GetComponent<PlayerStats>().CalculatingDamage(Random.Range(minDamage,maxDamage));
        }
    }
}
