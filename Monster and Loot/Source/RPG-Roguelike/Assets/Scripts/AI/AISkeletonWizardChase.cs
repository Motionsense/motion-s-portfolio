﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AISkeletonWizardChase : MonoBehaviour {

    /// <summary>
    /// 1. Player's position is being acessed by 
    /// using the X,Y and Z coordinates.
    /// 2. Using the animator with the animations of the AI
    /// </summary>
    public Transform player;
    public Animator enemyChase;

    /// <summary>
    /// Refference to the player's health bar/slider
    /// </summary>
    [SerializeField]
    Slider playerHealth;

    // Use this for initialization
    void Start() {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerHealth = GameObject.FindGameObjectWithTag("PlayerHealth").GetComponent<Slider>();

        enemyChase = GetComponent<Animator>();
    }

    /// <summary>
    /// If player's health points reach zero then AI will
    /// stop attacking and following the player.
    /// If the player's health points are greater than zero and the player
    /// is close to the AI by 10 tiles then AI will start chasing the player
    /// by calculating the distance between player's and AI's.
    /// </summary>
    void Update() {
        if (playerHealth.value <= 0) {
            enemyChase.SetBool("isWZIdleNormal", true);
            enemyChase.SetBool("isWZWalking", false);
            enemyChase.SetBool("isWZDmging", false);
        } else {
            Vector3 direction = player.position - this.transform.position;
            float angle = Vector3.Angle(direction, this.transform.forward);

            if (Vector3.Distance(player.position, this.transform.position) < 10 && angle < 120) {
                direction.y = 0.0f;

                this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                    Quaternion.LookRotation(direction), 0.1f);

                enemyChase.SetBool("isWZIdleNormal", false);
                if (direction.magnitude > 1.5f) {
                    this.transform.Translate(0, 0, 0.05f);

                    enemyChase.SetBool("isWZWalking", true);
                    enemyChase.SetBool("isWZDmging", false);
                } else {
                    enemyChase.SetBool("isWZWalking", false);
                    enemyChase.SetBool("isWZDmging", true);
                }
            } else {
                enemyChase.SetBool("isWZIdleNormal", true);
                enemyChase.SetBool("isWZWalking", false);
                enemyChase.SetBool("isWZDmging", false);
            }
        }
    }
}
