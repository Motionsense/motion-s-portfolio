//\===========================================================================================================================================
//\ Filename: BrickCollection.h
//\ Author  : Theodor Danciu
//\ Date    : 24/07/2018
//\ Edited	: 27/07/2018
//\ Brief   : Class declaration for BrickCollection
//\	Notes	: I've tried to write the code in a way that is self - documenting.Please note that I'm not commenting UG framework that has been
//\			  provided  for obvious reasons. I will try to comment only the code that contains logical statements such as functions or declaring
//\			  variables that are part of functions.		
//\			  Please note , while documenting the code I will reference the parts I've used from other projects.
//\===========================================================================================================================================

///
// Refference - The code that has been used here has been povided from one of the Example C++ projects unde the name EnemyOO
///

#ifndef _BRICKCOLLECTION_H__
#define _BRICKCOLLECTION_H__

#include "UGFW.h"

class BrickCollection
{
public:
	BrickCollection();
	~BrickCollection();

	//Getters/Setters
	int getPosX() const;
	int getPosY() const;
	int getColumns() const;
	int getRows() const;
	int getBrickWidth() const;
	int getBrickHeight() const;
	int getSprite() const;
	int getValue() const;

	void setPosX(float a_newPosX);
	void setPosY(float a_newPosY);
	void setRow(float a_setRow);
	void setCol(float a_setCol);
	void setValue(float a_setValue);
	void setSprite(float a_setSprite);
	void setPosition(int a_x, int a_y);

private:
	//Housekeeping Variables
	int m_iPosX;
	int m_iPosY;
	int m_iColumns = 4;
	int m_iRows = 4;
	int m_iBrickWidth = 50;
	int m_iBrickHeight = 150;
	int m_iSpriteID;
	int m_iValue;
};

#endif //_BRICKCOLLECTION_H__


