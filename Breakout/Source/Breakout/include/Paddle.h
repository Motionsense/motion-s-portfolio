//\===========================================================================================================================================
//\ Filename: Paddle.h
//\ Author  : Theodor Danciu
//\ Date    : 25/07/2018
//\ Edited	: 27/07/2018
//\ Brief   : Class declaration for Paddle
//\	Notes	: I've tried to write the code in a way that is self - documenting.Please note that I'm not commenting UG framework that has been
//\			  provided  for obvious reasons. I will try to comment only the code that contains logical statements such as functions or declaring
//\			  variables that are part of functions.		
//\			  Please note , while documenting the code I will reference the parts I've used from other projects.
//\===========================================================================================================================================

///
// Refference - The code that I have used here has been provided in the Week's 3 tutorial CT4019
///

#ifndef __PADDLE_H__
#define __PADDLE_H__

class Paddle {
public:
	Paddle();
	~Paddle();

	const float GetX(){ return m_iPaddlePosX; };
	const float GetY(){ return m_iPaddlePosY; };
	const int GetSpriteID() const { return m_iPaddleSpriteID; };
	const int GetPaddleHeight() const { return m_iPaddleHeight; };
	const int GetPaddleWidth() const { return m_iPaddleWidth; };

	void SetX(int a_x);
	void SetY(int a_y);
	void setPosition(int a_x, int a_y);
	void SetSprite(int a_iSprite);

	void PaddleInitialise();
	void PaddleDraw();
	void PaddleMovement();
	bool PaddleCollisionWalls(int a_iScreenX, int a_iOther);
private:
	//Housekeeping Variables
	int m_iPaddleSpriteID = 0;
	float m_iPaddlePosX = 0;
	float m_iPaddlePosY = 0;
	const float m_iPaddleVelocity = 8.0f;
	const int m_iPaddleHeight = 10;
	const int m_iPaddleWidth = 100;
};

#endif // !__PADDLE_H__

