//\===========================================================================================================================================
//\ Filename: Game.h
//\ Author  : Theodor Danciu
//\ Date    : 25/07/2018
//\ Edited	: 27/07/2018
//\ Brief   : Class declaration for Game
//\	Notes	: I've tried to write the code in a way that is self - documenting.Please note that I'm not commenting UG framework that has been
//\			  provided  for obvious reasons. I will try to comment only the code that contains logical statements such as functions or declaring
//\			  variables that are part of functions.		
//\			  Please note , while documenting the code I will reference the parts I've used from other projects.
//\===========================================================================================================================================

///
// Refference - The code that I have used here has been provided in the Week's 8 tutorial of the CT4019
///

#ifndef __GAME_H__
#define __GAME_H__

enum GameStates
{
	INSTRUCTIONS,
	GAME,
	GAMEWIN,
	GAMEOVER
};

class Game
{
public:
	Game();
	~Game();
	bool Initialise();
	bool Update();
	void Draw();
	void Deinitialise();
	void InstructionOnScreen();
private:
	//Housekeeping Variables
	int m_iLives = 3;
	int m_iScreenH = 0;
	int m_iScreenW = 0;
	int m_iYouWinSpriteID = 0;
	int m_iYouLoseSpriteID = 0;
	int m_ifontID = 0;
};

#endif // __GAME_H__