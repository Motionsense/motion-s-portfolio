//\===========================================================================================================================================
//\ Filename: Ball.h
//\ Author  : Theodor Danciu
//\ Date    : 26/07/2018
//\ Edited	: 27/07/2018
//\ Brief   : Class declaration for Ball
//\	Notes	: I've tried to write the code in a way that is self - documenting.Please note that I'm not commenting UG framework that has been
//\			  provided  for obvious reasons. I will try to comment only the code that contains logical statements such as functions or declaring
//\			  variables that are part of functions.		
//\			  Please note , while documenting the code I will reference the parts I've used from other projects.
//\===========================================================================================================================================

///
// Refference - None
///

#ifndef __BALL_H__
#define __BALL_H__

#include "UGFW.h"

class Ball
{
public:
	Ball();
	~Ball();

	float GetX() { return m_fBallPosX; }
	float GetY() { return m_fBallPosY; }
	void SetX(float a_x);
	void SetY(float a_y);

	void BalInitialise();
	void BallDraw();
	void BallUpdate();
	void BallMovement(float a_fdelta);
	bool BallOnScreenX(int a_iScreenWidth, int a_iOtherX, float a_fBallRadius);
	bool BallOnScreenY(int a_iScreenHeight, int a_iOtherY, float a_fBallRadius);
	void BouncingOnX();
	void BouncingOnY();
	void DoBallCollisionWalls();
	bool BallCollisionPaddle();
	void BallBricksCollisions();
	bool BallCollisionBricks(int x, int width, int y, int height);
	bool BallLosingLife();
	void BallResetPosition();

private:
	//Housekeeping Variables
	float m_fBallPosX;
	float m_fBallPosY;
	float m_fBallRadius = 16;
	float m_fBallSpeedY = 250.0f;;
	float m_fBallSpeedX = 250.0f;
	int m_iBallID;
};

#endif // __BALL_H__
