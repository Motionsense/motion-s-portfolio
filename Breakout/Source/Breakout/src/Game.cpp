//\===========================================================================================================================================
//\ Filename: Game.cpp
//\ Author  : Theodor Danciu
//\ Date    : 25/07/2018
//\ Edited	: 01/08/2018
//\ Brief   : Class declaration for Game
//\	Notes	: I've tried to write the code in a way that is self - documenting.Please note that I'm not commenting UG framework that has been
//\			  provided  for obvious reasons. I will try to comment only the code that contains logical statements such as functions or declaring
//\			  variables that are part of functions.		
//\			  Please note , while documenting the code I will reference the parts I've used from other projects.
//\===========================================================================================================================================

///
// Refference - The code that I have used here has been provided in the Week's 8 tutorial of the CT4019 and in the Week's 3 tutorial
// The collision calculations have been done by using AABB collision technique and Week's 19 Lecture Collisions and 
// the code from MovementOO from the C++ Game Example Projects
///

#include <vector>

#include "Game.h"
#include "Ball.h"
#include "Paddle.h"
#include "BrickCollection.h"

#include "UGFW.h"
#include "UG_Defines.h"

std::vector<BrickCollection> g_pBrickArray;

Ball* ball = new Ball;
Paddle* paddle = new Paddle;
BrickCollection* bricks = new BrickCollection;
GameStates currentstate = INSTRUCTIONS;

#pragma region Contrustor and Decontructor
Game::Game()
{
}

//Free up the memory accessed and used
Game::~Game()
{
	delete ball;
	delete paddle;
	delete bricks;
}
#pragma endregion

#pragma region Game States
//Initialising the window's values and its background color
bool Game::Initialise()
{
	//Flag success on window creation
	bool bSuccess = UG::Create(550, 700, false, "Breakout", 100, 100);
	if (bSuccess)
	{	
		UG::SetBackgroundColor(UG::SColour(0x0, 0x0, 0x50, 0xFF));

		paddle->PaddleInitialise();
		ball->BalInitialise();
		std::vector<BrickCollection> BrickSetup();
		g_pBrickArray = BrickSetup();
	}
	return bSuccess;
}

//Updating the game every frame
bool Game::Update()
{
	switch (currentstate)
	{
	case INSTRUCTIONS:
		UG::ClearScreen();
		UG::SetBackgroundColor(UG::SColour(0x0, 0x0, 0x0, 0xFF));
		InstructionOnScreen();

		//Check if player wants to quit.
		if (UG::IsKeyDown(UG::KEY_ESCAPE))
		{
			UG::Close();
		}
		break;
	case GAME:
		//Clear everything from the previous frame
		UG::ClearScreen();

		UG::SetBackgroundColor(UG::SColour(0x0, 0x0, 0x50, 0xFF));

		ball->BallUpdate();
		ball->DoBallCollisionWalls();
		ball->BallBricksCollisions();
		paddle->PaddleMovement();
		
		//if the ball collides withe the paddle the the ball will bounce off the Y axis by reverting the speed on Y axis
		if (ball->BallCollisionPaddle())
		{
			ball->BouncingOnY();
		}

		//Check if player want to quit
		if (UG::IsKeyDown(UG::KEY_ESCAPE))
		{
			UG::Close();
		}

		//If the bricks array is empty then we are changing the game state
		if (g_pBrickArray.empty())
		{
			currentstate = GAMEWIN;
		}

		//Check if the ball's position is less than 0 then we decrement the lives value by 1 and we reset the position of the ball
		//if the lives values reaches zero then the player has lost the game
		if (ball->BallLosingLife())
		{
			m_iLives--;
			ball->BallResetPosition();
			if (m_iLives == 0)
			{
				currentstate = GAMEOVER;
			}
		}
		break;
	case GAMEOVER:

		UG::ClearScreen();
		UG::GetScreenSize(m_iScreenW, m_iScreenH);
		m_iYouLoseSpriteID = UG::CreateSprite("./sprites/YouLose.png", 200, 50, true);
		UG::SetSpritePosition(m_iYouLoseSpriteID, m_iScreenW / 2.0f, m_iScreenH / 2.0f);

		//Check if player want to quit.
		if (UG::IsKeyDown(UG::KEY_ESCAPE))
		{
			UG::Close();
		}

		break;

	case GAMEWIN:
		UG::ClearScreen();
		UG::GetScreenSize(m_iScreenW, m_iScreenH);
		m_iYouWinSpriteID = UG::CreateSprite("./sprites/YouWin.png", 200, 50, true);
		UG::SetSpritePosition(m_iYouWinSpriteID, m_iScreenW / 2.0f, m_iScreenH / 2.0f);

		//Check if player wants to quit.
		if (UG::IsKeyDown(UG::KEY_ESCAPE))
		{
			UG::Close();
		}

		break;

	default:
		break;
	}

	bool bStillRunning = UG::Process();

	return bStillRunning;
}

//Drawing the sprites
void Game::Draw()
{
	switch (currentstate)
	{
	case GAME:
	ball->BallDraw();
	paddle->PaddleDraw();
	for each(BrickCollection oCurrentBrick in g_pBrickArray)
	{
		UG::DrawSprite(oCurrentBrick.getSprite());
	}
		break;
	case GAMEOVER:
		UG::ClearScreen();
		UG::DrawSprite(m_iYouLoseSpriteID);
		break;
	case GAMEWIN:
		UG::ClearScreen();
		UG::DrawSprite(m_iYouWinSpriteID);
		break;
	default:
		break;
	}
	
}

//Disposing all memory used in UG framework
void Game::Deinitialise()
{
	UG::Dispose();
}
#pragma endregion

#pragma region Ball Collision
void Ball::DoBallCollisionWalls()
{
	int iScreenWidth = 0, iScrennHeight = 0;
	UG::GetScreenSize(iScreenWidth, iScrennHeight);

	//If the ball pos x is not on the on screen then revert the speed on x axis
	if (!BallOnScreenX(iScreenWidth, m_fBallPosX, m_fBallRadius))
	{
		BouncingOnX();
	}

	//If the ball pos y is not on the on screen then revert the speed on y axis
	if (!BallOnScreenY(iScrennHeight, m_fBallPosY, m_fBallRadius))
	{
		BouncingOnY();
	}
}

//Checking if the ball's position on X axis is going off boundaries
bool Ball::BallOnScreenX(int a_iScreenWidth, int a_iOtherX, float a_fBallRadius)
{
	//If the position x of the ball plus the radius of the ball is bigger than the screen width or smaller than 0 then the ball is off boundaries
	bool returnValue = true;
	if (a_iOtherX + (a_fBallRadius * 0.5f) >= a_iScreenWidth ||
		a_iOtherX - (a_fBallRadius * 0.5f) <= 0)
	{
		returnValue = false;
	}
	return returnValue;
}

//Checking if the ball's position on Y axis is going off boundaries
bool Ball::BallOnScreenY(int a_iScreenHeight, int a_iOtherY, float a_fBallRadius)
{
	bool returnValue = true;
	if (a_iOtherY + (a_fBallRadius * 0.5f) >= a_iScreenHeight ||
		a_iOtherY - (a_fBallRadius * 0.5f) <= 0)
	{
		returnValue = false;
	}
	return returnValue;
}

//AABB - AABB collision technique 
bool Ball::BallCollisionPaddle()
{
	//Declaring temporary variables
	int iTempPaddleID = paddle->GetSpriteID();
	int iTempPaddleWidth = paddle->GetPaddleWidth();
	int iTempPaddleHeight = paddle->GetPaddleHeight();
	float fTempNewPaddlePosX, fTempNewPaddlePosY;

	//Getting the position coordinates of the paddle and of the ball
	UG::GetSpritePosition(iTempPaddleID, fTempNewPaddlePosX, fTempNewPaddlePosY);
	UG::GetSpritePosition(m_iBallID, m_fBallPosX, m_fBallPosY);

	//The coolision is true if the position X of the ball + ball's radius is bigger or equal with 
	//the position X of the paddle minus the half of the paddle's width and
	//the position x of the paddle plus its half width is bigger or equal the position x of the ball
	bool collisionX = m_fBallPosX + m_fBallRadius >= fTempNewPaddlePosX - iTempPaddleWidth / 2.0f &&
		fTempNewPaddlePosX + iTempPaddleWidth * 0.5f >= m_fBallPosX;

	//The coolision is true if the position Y of the ball + ball's radius is bigger or equal with the position Y of the paddle
	//and the position Y of the paddle plus its height is bigger or equal the position Y of the ball
	bool collisionY = m_fBallPosY + m_fBallRadius >= fTempNewPaddlePosY &&
		fTempNewPaddlePosY + iTempPaddleHeight >= m_fBallPosY;

	//Returning the checkings of the collision on X and Y
	return collisionX && collisionY;
}
#pragma endregion

#pragma region Paddle Collision
//The movement of the paddle across the screen
void Paddle::PaddleMovement()
{
	int iTempScreenWidth, m_iTempScreenHeight;
	UG::GetScreenSize(iTempScreenWidth, m_iTempScreenHeight);

	//In order to be able to move the paddle a temporary variable is assigned 
	//which will equal the original position of the X added or substracted with the velocity of the paddle 
	float iTempPadNewPosX = 0;
	iTempPadNewPosX = (m_iPaddlePosX -= m_iPaddleVelocity);
	if (PaddleCollisionWalls(iTempScreenWidth, iTempPadNewPosX))
	{
		if (UG::IsKeyDown(UG::KEY_A))
		{
			m_iPaddlePosX -= m_iPaddleVelocity;

			UG::SetSpritePosition(m_iPaddleSpriteID, iTempPadNewPosX, m_iPaddlePosY);
		}
	}

	iTempPadNewPosX = (m_iPaddlePosX += m_iPaddleVelocity);

	if (PaddleCollisionWalls(iTempScreenWidth, iTempPadNewPosX))
	{
		if (UG::IsKeyDown(UG::KEY_D))
		{
			m_iPaddlePosX += m_iPaddleVelocity;

			UG::SetSpritePosition(m_iPaddleSpriteID, iTempPadNewPosX, m_iPaddlePosY);
		}
	}
}

//Checking if the paddle collides with any of the walls on X axis
bool Paddle::PaddleCollisionWalls(int a_iScreenX, int a_iOther)
{
	if (a_iOther + (m_iPaddleWidth * 0.50f) >= a_iScreenX ||
		a_iOther - (m_iPaddleWidth * 0.50f) <= 0.0f)
	{
		return false;
	}
	else
	{
		return true;
	}
}
#pragma endregion

#pragma region Instruction Screen
//Game Instructions Screen
void Game::InstructionOnScreen()
{
	int InstrucID = UG::CreateSprite("./sprites/Instructions.png", 550, 700);
	UG::SetSpritePosition(InstrucID, 280, 350);
	UG::DrawSprite(InstrucID);

	if (UG::IsKeyDown(UG::KEY_N))
	{
		currentstate = GAME;
	}
}
#pragma endregion

#pragma region Bricks Collision/States
std::vector<BrickCollection> BrickSetup()
{
	//Getting the columns and rows of the bricks
	int iColms = bricks->getColumns();
	int iRows = bricks->getRows();

	// Creates a temporary brick array for all the bricks
	std::vector<BrickCollection> m_oBrickVector;

	// This gives each Brick a value so that it can be removed easily
	int iBrickValue = 0;

	// Loops through for the amount of bricks in the game
	for (int i = 0; i < iColms; i++)
	{
		for (int j = 0; j < iRows; j++)
		{
			// Creates a brick, this is the current brick that is being edited
			BrickCollection oCurrentBrick;

			oCurrentBrick.setCol(i);
			oCurrentBrick.setRow(j);

			if (oCurrentBrick.getRows() >= 0)
			{
				oCurrentBrick.setSprite(UG::CreateSprite("./sprites/DefaultBrick.png", 60, 20, true));
			}

			int iScreenW = 0, iScreenH = 0;
			int iWidthPositionIncs = 0, iHeightPositionIncs = 0;
			UG::GetScreenSize(iScreenW, iScreenH);

			//Spread enemys across 80% of width by calculating position increments
			iWidthPositionIncs = (int)((iScreenW * 0.8f) / iColms);
			//And across half of height by same method
			iHeightPositionIncs = (int)((iScreenH * 0.5f) / iRows);

			//Start enemy grid 10% of the way across the screen
			int iStartingWidth = (int)(iScreenW * 0.1f);
			//Start enemy grid 50% of the way up the screen
			int iStartingHeight = (int)(iScreenH * 0.5f);

			// This sets the center X and Y positions of the Brick
			oCurrentBrick.setPosition(
				(iStartingWidth + iWidthPositionIncs * (i + 0.5f)),
				(iStartingHeight + iHeightPositionIncs * (j - 0.5f)));
			
			
			// Sets the Brick value and then increments the brick value
			oCurrentBrick.setValue(iBrickValue);
			iBrickValue++;
			// After everything is done with the brick, push it back in the array and start working on the next brick
			m_oBrickVector.push_back(oCurrentBrick);

		}
	}
	// Returns the vector of bricks after its completely full
	return m_oBrickVector;
}

std::vector<BrickCollection> ResetBrickValues()
{
	//Temporary value that would be used to assign a new value to the bricks
	int iTempValue = 0;

	// Creates a new object brick
	std::vector<BrickCollection> a_brickVector;
	
	//Checking every brick in the array 
	for each (BrickCollection oCurrentBrick in g_pBrickArray)
	{
		// Adds the brick to the new array after 
		// Giving it a value 
		oCurrentBrick.setValue(iTempValue);
		iTempValue++;
		a_brickVector.push_back(oCurrentBrick);
	}

	// Returns the new brick object
	return a_brickVector;
}

//Checks the collision between the ball and every brick in the array
void Ball::BallBricksCollisions()
{
	int iTempBrickID = -1;
	for each(BrickCollection oCurrentBrick in g_pBrickArray)
	{

		//If there was a collision between the ball and a brick
		//we are deleting the brick it has collided with the ball
		//and we are reversing the ball's speed on y so it will bounce
		if (ball->BallCollisionBricks(oCurrentBrick.getPosX(), oCurrentBrick.getBrickWidth(), oCurrentBrick.getPosY(), oCurrentBrick.getBrickHeight()))
		{
			iTempBrickID = oCurrentBrick.getValue();
			ball->BouncingOnY();
		}
	}

	//If the temporary brick value is bigger than the zero then 
	//the ball has collided with a brick and the brick will be deleted from the bricks array
	if (iTempBrickID >= 0)
	{
		g_pBrickArray.erase(g_pBrickArray.begin() + iTempBrickID);
		g_pBrickArray = ResetBrickValues();
	}
}

//Checking wether the ball's position is going to collide with the position of a brick
bool Ball::BallCollisionBricks(int x, int width, int y, int height)
{
	bool collisionX = m_fBallPosX + m_fBallRadius >= x - width / 2.0f &&
		x + width >= m_fBallPosX;
	bool collisionY = m_fBallPosY + m_fBallRadius >= y &&
		y + height / 6.0f >= m_fBallPosY;

	return collisionX && collisionY;
}
#pragma endregion

