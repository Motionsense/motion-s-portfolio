//\===========================================================================================================================================
//\ Filename: BrickCollection.cpp
//\ Author  : Theodor Danciu
//\ Date    : 25/07/2018
//\ Edited	: 28/07/2018
//\ Brief   : Class declaration for BrickCollection
//\	Notes	: I've tried to write the code in a way that is self - documenting.Please note that I'm not commenting UG framework that has been
//\			  provided  for obvious reasons. I will try to comment only the code that contains logical statements such as functions or declaring
//\			  variables that are part of functions.		
//\			  Please note , while documenting the code I will reference the parts I've used from other projects.
//\===========================================================================================================================================

///
// Refference - The code that has been used here has been povided from one of the Example C++ projects unde the name EnemyOO
///

#include "BrickCollection.h"

BrickCollection::BrickCollection()
{

}



BrickCollection::~BrickCollection()
{
	
}

int BrickCollection::getPosX() const
{
	return m_iPosX;
}
int BrickCollection::getPosY() const
{
	return m_iPosY;
}

int BrickCollection::getColumns() const
{
	return m_iColumns;
}
int BrickCollection::getRows() const
{
	return m_iRows;
}

int BrickCollection::getBrickWidth() const
{
	return m_iBrickWidth;
}
int BrickCollection::getBrickHeight() const
{
	return m_iBrickHeight;
}

int BrickCollection::getSprite() const
{
	return m_iSpriteID;
}

int BrickCollection::getValue() const
{
	return m_iValue;
}

void BrickCollection::setPosX(float a_newPosX)
{
	m_iPosX = a_newPosX;
}
void BrickCollection::setPosY(float a_newPosY)
{
	m_iPosY = a_newPosY;
}
void BrickCollection::setRow(float a_setRow)
{
	m_iRows = a_setRow;
}
void BrickCollection::setCol(float a_setCol)
{
	m_iColumns = a_setCol;
}
void BrickCollection::setValue(float a_setValue)
{
	m_iValue = a_setValue;
}
void BrickCollection::setSprite(float a_setSprite)
{
	m_iSpriteID = a_setSprite;
}

void BrickCollection::setPosition(int a_x, int a_y)
{
	m_iPosX = a_x;
	m_iPosY = a_y;

	UG::SetSpritePosition(m_iSpriteID, (float)a_x, (float)a_y);
}