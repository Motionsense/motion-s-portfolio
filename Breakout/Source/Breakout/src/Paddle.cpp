//\===========================================================================================================================================
//\ Filename: Paddle.cpp
//\ Author  : Theodor Danciu
//\ Date    : 25/07/2018
//\ Edited	: 27/07/2018
//\ Brief   : Class declaration for Paddle
//\	Notes	: I've tried to write the code in a way that is self - documenting.Please note that I'm not commenting UG framework that has been
//\			  provided  for obvious reasons. I will try to comment only the code that contains logical statements such as functions or declaring
//\			  variables that are part of functions.		
//\			  Please note , while documenting the code I will reference the parts I've used from other projects.
//\===========================================================================================================================================

///
// Refference - None
///

#include "Paddle.h"

#include "UGFW.h"
#include "UG_Defines.h"

#pragma region Contrustor and Decontructor
//Default contstructor and deconstrutor
Paddle::Paddle()
{
}

Paddle::~Paddle()
{
}
#pragma endregion

#pragma region Setters
//Setting the X position of the paddle
void Paddle::SetX(int a_x)
{
	m_iPaddlePosX = a_x;
}

//Setting the Y position of the paddle
void Paddle::SetY(int a_y)
{
	m_iPaddlePosY = a_y;
}

//Setting the position of the paddle on X and Y axis
void Paddle::setPosition(int a_x, int a_y)
{
	SetX(a_x);
	SetY(a_y);

	UG::SetSpritePosition(m_iPaddleSpriteID, (float)a_x, (float)a_y);
}

//Setting the sprite of the paddle
void Paddle::SetSprite(int a_iSprite)
{
	m_iPaddleSpriteID = a_iSprite;
}
#pragma endregion

#pragma region States of the Paddle
//We are initialising the paddle by setting its sprite, size and position
void Paddle::PaddleInitialise()
{
	SetSprite(UG::CreateSprite("./sprites/Paddle.png", m_iPaddleWidth , m_iPaddleHeight , true));

	int m_iScreenX, m_iScreenY;
	UG::GetScreenSize(m_iScreenX, m_iScreenY);

	setPosition(m_iScreenX / 2.0f, m_iScreenY / 9.0f);
}

//Drawing the paddle
void Paddle::PaddleDraw()
{
	UG::DrawSprite(m_iPaddleSpriteID);
}
#pragma endregion