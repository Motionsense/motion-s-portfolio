//\===========================================================================================================================================
//\ Filename: main.cpp
//\ Author  : Theodor Danciu
//\ Date    : 25/07/2018
//\ Edited	: 25/07/2018
//\ Brief   : The Game's States logic 
//\	Notes	: I've tried to write the code in a way that is self - documenting.Please note that I'm not commenting UG framework that has been
//\			  provided  for obvious reasons. I will try to comment only the code that contains logical statements such as functions or declaring
//\			  variables that are part of functions.		
//\			  Please note , while documenting the code I will reference the parts I've used from other projects.
//\===========================================================================================================================================

///
// Refference - The code that I have used here has been provided in the Week's 8 tutorial of the CT4019
///

#include "UGFW.h"
#include "UG_Defines.h"

#include "Game.h"

void main()
{
	Game* game = new Game();

	bool bSuccess = game->Initialise();
	if (bSuccess)
	{
		bool bStillRunning = true; 
		while (bStillRunning)
		{
			bStillRunning = game->Update();
			game->Draw();
		}
	}
	game->Deinitialise();
}


