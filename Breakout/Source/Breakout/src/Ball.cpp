//\===========================================================================================================================================
//\ Filename: Ball.cpp
//\ Author  : Theodor Danciu
//\ Date    : 25/07/2018
//\ Edited	: 27/07/2018
//\ Brief   : Class declaration for Ball
//\	Notes	: I've tried to write the code in a way that is self - documenting.Please note that I'm not commenting UG framework that has been
//\			  provided  for obvious reasons. I will try to comment only the code that contains logical statements such as functions or declaring
//\			  variables that are part of functions.		
//\			  Please note , while documenting the code I will reference the parts I've used from other projects.
//\===========================================================================================================================================

///
// Refference - None
///

#include <cstdlib>

#include "Ball.h"
#include "UGFW.h"

#pragma region Contrustor and Deconstructor

//Default constructor and deconstructor
Ball::Ball()
{
	
}

Ball::~Ball()
{

}
#pragma endregion


#pragma region Setters
//Setting the X pos of the ball
void Ball::SetX(float a_x)
{
	m_fBallPosX = a_x;
}

//Setting the Y pos of the ball
void Ball::SetY(float a_y)
{
	m_fBallPosY = a_y;
}
#pragma endregion


#pragma region States of the ball
//Initialising the ball's varibales by initialising the size and position of it
void Ball::BalInitialise()
{
	m_iBallID = UG::CreateSprite("./sprites/Ball.png", m_fBallRadius , m_fBallRadius , true);

	int iScreenWidth, iScreenHeight;
	UG::GetScreenSize(iScreenWidth, iScreenHeight);

	UG::SetSpritePosition(m_iBallID, iScreenWidth * 0.5f, iScreenHeight * 0.3f);
}

//Drawing the ball on the screen
void Ball::BallDraw()
{
	UG::DrawSprite(m_iBallID);
}

//Updating the ball every frame
void Ball::BallUpdate()
{
	BallMovement(UG::GetDeltaTime());
}

//Checking if the ball reaches the bottom part of the screen
bool Ball::BallLosingLife()
{
	bool returnValue = false;
	if (m_fBallPosY - (m_fBallRadius * 0.5f) <= 1)
	{
		returnValue = true;
	}
	return returnValue;
}

//We are resseting the ball's position into the initial one
void Ball::BallResetPosition()
{
	int iScreenWidth, iScreenHeight;
	UG::GetScreenSize(iScreenWidth, iScreenHeight);
	UG::SetSpritePosition(m_iBallID, iScreenWidth * 0.5f, iScreenHeight * 0.3f);
}
#pragma endregion


#pragma region Ball Interaction

//Ball movement across the screen
void Ball::BallMovement(float a_fdelta) 
{
	//Getting the sprite position on x and y axis
	UG::GetSpritePosition(m_iBallID, m_fBallPosX, m_fBallPosY);

	//Temporary variables used for the movement of the ball
	float m_fBallDirX = 0, m_fBallDirY = 0;
	//Starting angle of the ball
	float fAngle = 20.0f;

	//Starting direction on X and Y axis which equals the cos and sin of the starting angle
	m_fBallDirX = (float)cosf(fAngle);
	m_fBallDirY = (float)sin(fAngle);

	//The movement of the ball on X and Y axis
	m_fBallPosX += m_fBallDirX * m_fBallSpeedX * a_fdelta;
	m_fBallPosY += m_fBallDirY * m_fBallSpeedY * a_fdelta;

	//Setting the sprite position on x and y axis
	UG::SetSpritePosition(m_iBallID, m_fBallPosX, m_fBallPosY);
}

//If the ball reaches the boundaries of the screen width we return a negative speed
void Ball::BouncingOnX()
{
	m_fBallSpeedX *= -1.0f;
}

//If the ball reaches the boundaries of the screen height we return a negative speed
void Ball::BouncingOnY()
{
	m_fBallSpeedY *= -1.0f;
}
#pragma endregion